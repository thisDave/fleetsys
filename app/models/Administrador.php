<?php

require_once 'Model.php';

class Administrador extends Model
{
	public function ingresarProveedor($datos, $pais)
	{
		$resultado = parent::showRes("CALL SP_insertProvider('".$datos[0]."', '".$datos[2]."', '".$datos[3]."', ".$datos[1].", ".$pais.")");

		$data = $resultado->fetch_assoc();

		if ($data['resultado'] == 'success')
			return true;
		else
			return false;
	}

	public function actualizarProveedor($nombre, $contacto, $telefono, $idProveedor)
	{
		$resultado = parent::showRes("CALL SP_updateProvider('".$nombre."', '".$contacto."', '".$telefono."', ".$idProveedor.")");

		if ($resultado)
			return true;
		else
			return false;
	}

	public function validarDatosAuto($placa, $motor, $chasis, $id = null)
	{
		$query = (is_null($id)) ? "CALL SP_valDatCarCreate('{$placa}', '{$motor}', '{$chasis}')" : "CALL SP_valDatCarUpdate('{$placa}', '{$motor}', '{$chasis}', {$id})";

		$resultado = parent::showRes($query);

		$dato = $resultado->fetch_assoc();

		return ($dato['resultado'] == 'success') ? true : false;
	}

	public function actualizarAuto($datos)
	{
		$query = "CALL SP_actualizarAuto('{$datos['placa']}', '{$datos['color']}', {$datos['marca']}, '{$datos['modelo']}', {$datos['anno']}, '{$datos['motor']}', '{$datos['chasis']}', '{$datos['vin']}', {$datos['proveedor']}, {$datos['region']}, {$datos['km']}, {$datos['kmetraje']}, {$datos['tipo']}, {$datos['id']})";

		$resultado = parent::showRes($query);

		if ($resultado)
			return true;
		else
			return false;
	}

	public function delAuto($idAuto)
	{
		$resultado = parent::showRes("CALL SP_eliminarAuto(".$idAuto.")");

		if ($resultado)
		{
			$data = $resultado->fetch_assoc();

			if ($data['resultado'] == 'success')
				return true;
			else
				return false;
		}
		else
		{
			return false;
		}

	}

	public function insertCategory($categoria)
	{
		$resultado = parent::showRes("CALL SP_insertCategory('".$categoria."')");

		$data = $resultado->fetch_assoc();

		if ($data['resultado'] == 'success')
			return true;
		else
			return false;
	}

	public function updtCategory($nombre, $idCategoria)
    {
        $resultado = parent::showRes("CALL SP_updateCategory('".$nombre."', ".$idCategoria.")");

        $data = $resultado->fetch_assoc();

        if ($data['resultado'] == 'success')
            return true;
        else
            return false;
    }

    public function deleteCategory($idCategoria)
    {
    	$resultado = parent::showRes("CALL SP_delCategory(".$idCategoria.")");

    	if ($resultado)
    	{
	    	$data = $resultado->fetch_assoc();

	    	if ($data['resultado'] == 'success')
	            return true;
	        else
	            return false;
    	}
    	else
    	{
    		return false;
    	}

    }

	public function insertProduct($nombre, $categoria)
	{
		$resultado = parent::showRes("CALL SP_insertProduct(".$categoria.", '".$nombre."')");

		$val = $resultado->fetch_assoc();

		$val = $val['val'];

		if ($val == 1)
			return true;
		else
			return false;
	}

	public function updateProd($nombre, $idCategoria, $idProducto)
    {
    	$resultado = parent::showRes("CALL SP_updateProd('".$nombre."', ".$idCategoria.", ".$idProducto.")");

    	$data = $resultado->fetch_assoc();

		if ($data['resultado'] == 'success')
			return true;
		else
			return false;
    }

    public function delProd($idProducto)
    {
    	$resultado = parent::showRes("CALL SP_delProd(".$idProducto.")");

    	$data = $resultado->fetch_assoc();

		if ($data['resultado'] == 'success')
			return true;
		else
			return false;
    }

    public function insertVoucher($voucher)
    {
    	$query = "CALL SP_insertarVale('".$_SESSION['startGas']['tipo']."', ".$voucher['nIngreso'].", ".$voucher['valor'].", '".$voucher['fecha']."', ".$_SESSION['startGas']['proveedor'].", ".$voucher['idUsuario'].", ".$voucher['auto'].", ".$voucher['kmSalida'].", ".$voucher['kmEntrada'].")";

    	$resultado = parent::showRes($query);

    	if ($resultado)
    	{
    		$data = $resultado->fetch_assoc();

    		if ($data['resultado'] == 'success')
    			return true;
    		else
    			return false;
    	}
    	else
    	{
    		return false;
    	}
    }

    public function insertarPreorden($datos)
    {
		$tipo = $datos['tipoPreorden'];
		$idV = $datos['idVehiculo'];
		$km = $datos['kilometraje'];
		$coment = $datos['comentarios'];
		$id = $_SESSION['log']['id'];

		$query = "CALL SP_insertarPreorden('{$tipo}', {$idV}, {$km}, '{$coment}', {$id})";

        $dato = parent::showRes($query)->fetch_assoc();

        return $dato['idPreorden'];
    }

    public function insertarDetallePreorden($id, $tipo, $accion)
    {
    	$tabla = ($tipo == 'externa') ? 'tbl_detallePrexternas' : 'tbl_detallePrinternas';

        unset($_SESSION['query']);

        if (parent::showRes("INSERT INTO {$tabla} (idPreorden, accion, idEstado) VALUES ({$id}, '{$accion}', 3)"))
            return true;
        else
            return false;
    }

    public function cleanPreorden($id)
    {
    	parent::showRes("DELETE FROM tbl_detallePrinternas WHERE idPreorden = {$id}");
    	parent::showRes("DELETE FROM tbl_detallePrexternas WHERE idPreorden = {$id}");
    	parent::showRes("DELETE FROM tbl_preordenes WHERE idPreorden = {$id}");
    }

    public function listaPreordenes()
    {
    	$datos = [];

    	$res = parent::showRes("CALL SP_listaPreordenes()");

    	if ($res->num_rows > 0)
    	{
	    	while ($campo = $res->fetch_assoc())
	    	{
	    	    $datos['idPreorden'][] = $campo['idPreorden'];
			    $datos['numeroPlaca'][] = $campo['numeroPlaca'];
			    $datos['modelo'][] = $campo['modelo'];
			    $datos['color'][] = $campo['color'];
			    $datos['tipoPreorden'][] = $campo['tipoPreorden'];
			    $datos['fechaEntrada'][] = $campo['fechaEntrada'];
			    $datos['fechaSalida'][] = $campo['fechaSalida'];
			    $datos['usuario'][] = $campo['usuario'];
			    $datos['estado'][] = $campo['estado'];
	    	}

	    	return $datos;
    	}
    	else
    	{
    		return false;
    	}
    }

    public function infoPreorden($id)
    {
    	$datos = [];

    	$query = "
    	SELECT
			p.*,
	        v.numeroPlaca,
	        e.estado
		FROM
			tbl_preordenes p
		INNER JOIN
			tbl_Vehiculos v ON p.idVehiculo = v.idVehiculo
		INNER JOIN
			tbl_Estados e ON p.idEstado = e.idEstado
		WHERE
			idPreorden = {$id};
    	";

    	$res = parent::showRes($query);

    	while ($campo = $res->fetch_assoc())
    	{
    	    $datos['idPreorden'] = $campo['idPreorden'];
		    $datos['tipoPreorden'] = $campo['tipoPreorden'];
		    $datos['idVehiculo'] = $campo['idVehiculo'];
		    $datos['kilometraje'] = $campo['kilometraje'];
			$datos['fechaEntrada'] = $campo['fechaEntrada'];
		    $datos['fechaSalida'] = $campo['fechaSalida'];
		    $datos['comentarios'] = $campo['comentarios'];
			$datos['idUsuario'] = $campo['idUsuario'];
		    $datos['idEstado'] = $campo['idEstado'];
		    $datos['numeroPlaca'] = $campo['numeroPlaca'];
		    $datos['estado'] = $campo['estado'];
    	}

    	return $datos;
    }

    public function detallePreorden($tipo, $id)
    {
    	$datos = [];

    	$tabla = ($tipo == 'interna') ? 'tbl_detallePrinternas' : 'tbl_detallePrexternas';

    	$query = "select * from {$tabla} where idPreorden = {$id}";

		$res = parent::showRes($query);

		while ($campo = $res->fetch_assoc())
		{
		    $datos['idPreorden'][] = $campo['idPreorden'];
		    $datos['accion'][] = $campo['accion'];
		    $datos['idEstado'][] = $campo['idEstado'];
		}

		return $datos;
    }

    public function totalNotificaciones()
    {
    	$resultado = parent::showRes("SELECT COUNT(*) AS 'total' FROM tbl_notificaciones");
		$campo = $resultado->fetch_assoc();
		return $campo['total'];
    }

    public function getNotifies()
    {
    	$resultado = parent::showRes("SELECT * FROM tbl_notificaciones");
    	if ($resultado->num_rows > 0) {
    		$datos = [];
    		while ($data = $resultado->fetch_assoc())
    		{
    		    $vehiculo = parent::infoAuto($data['idVehiculo']);
    		    $datos['idAlerta'][] = $data['idAlerta'];
    		    $datos['mensaje'][] = $data['mensaje'];
    		    $datos['detalles'][] = $data['detalles'];
    		    $datos['placa'][] = $vehiculo['placa'];
    		    $datos['kmActual'][] = $vehiculo['kilometraje'];
    		    $datos['mtto'][] = $vehiculo['mtto'];
    		}
    		return $datos;
    	}
    	else {
    		return false;
    	}
    }

    public function updateAlert($rango, $alerta)
    {
    	$resultado = parent::showRes("UPDATE tbl_alertas SET valor = {$rango}, alerta = {$alerta} WHERE idAlerta = 1");

    	if ($resultado)
    		return true;
    	else
    		return false;
    }

    public function obtenerAlerta()
    {
    	$resultado = parent::showRes("SELECT * FROM tbl_alertas");

    	if ($resultado->num_rows > 0) {
    		$info = [];
    		while ($data = $resultado->fetch_assoc()) {
    		    $info['idAlerta'] = $data['idAlerta'];
    		    $info['info'] = $data['info'];
    		    $info['valor'] = $data['valor'];
    		    $info['alerta'] = $data['alerta'];
    		}
    		return $info;
    	}else {
    		return false;
    	}
    }

    public function newMtto($idPreorden, $idUsuario, $comentario, $tipo, $proxKm, $idV)
    {
    	$resultado = parent::showRes("CALL SP_newMtto({$idPreorden}, {$idUsuario}, '{$comentario}', '{$tipo}', {$proxKm}, {$idV})");

    	if ($resultado)
    	{
    		$dato = $resultado->fetch_assoc();
    		return $dato['idMtto'];
    	}
    	else
    	{
    		return false;
    	}
    }

    public function newDetalleMtto($idMtto, $idProducto, $cantidad, $numFactura, $punitario, $idprov)
    {
    	$resultado = parent::showRes("
    		INSERT INTO tbl_detallemttos VALUES ({$idMtto}, {$idProducto}, {$cantidad}, '{$numFactura}', {$punitario}, {$idprov})
    	");

    	if ($resultado)
    		return true;
    	else
    		return false;
    }

    public function billDetail($idPreorden)
    {
    	$resultado = parent::showRes("CALL SP_detalleFactura({$idPreorden})");

    	if ($resultado)
    	{
    		if ($resultado->num_rows > 0)
    		{
    			$datos = [];
    			while ($data = $resultado->fetch_assoc())
    			{
    			    $datos['idMtto'][] = $data['idMtto'];
    			    $datos['producto'][] = $data['producto'];
    			    $datos['cantidad'][] = $data['cantidad'];
    			    $datos['numFactura'][] = $data['numFactura'];
    			    $datos['proveedor'][] = $data['proveedor'];
    			    $datos['punitario'][] = $data['precioUnitario'];
    			    $datos['total'][] = $data['total'];
    			}
    			return $datos;
    		}
    		else
    		{
    			return false;
    		}
    	}
    	else
    	{
    		return false;
    	}
    }

    public function detalleFactura($idMtto, $nfact)
    {
        $r = parent::showRes("CALL SP_infoFactura({$idMtto}, '{$nfact}')");

        if ($r)
        {
            if ($r->num_rows > 0)
            {
                $datos = [];
                while ($data = $r->fetch_assoc())
                {
                    $datos['producto'][] = $data['producto'];
                    $datos['cantidad'][] = $data['cantidad'];
                    $datos['numFactura'][] = $data['numFactura'];
                    $datos['proveedor'][] = $data['proveedor'];
                    $datos['punitario'][] = $data['precioUnitario'];
                    $datos['total'][] = $data['total'];
                }
                return $datos;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public function updateAutoPics($info_pictures)
    {
        $res = parent::showRes("SELECT idHoja FROM `tbl_hojaSalidas` hs INNER JOIN tbl_Bitacoras b on hs.idBitacora = b.idBitacora WHERE b.idVehiculo = {$idVehiculo}");

        $idHojas = [];

        while ($dato = $res->fetch_assoc())
        {
            $idHojas[] = $dato['idHoja'];
        }

        $idHojas = array_reverse($idHojas);

        foreach ($idHojas as $value)
        {
            $res = parent::showRes("CALL SP_verificaDetalleGolpe({$value})");
            $dato = $res->fetch_assoc();
            if ($dato['resultado'] == 'success') { $idHoja = $value; break; }
        }
    }

    public function addAction($action)
    {
        $resultado = parent::showRes("INSERT INTO tbl_accionesMant VALUES (NULL, '{$action}')");

        if ($resultado)
            return true;
        else
            return false;
    }

    public function updateAction($action, $idAction)
    {
        $resultado = parent::showRes("
            UPDATE tbl_accionesMant SET accion = '{$action}' WHERE idAccion = {$idAction}
        ");

        if ($resultado)
            return true;
        else
            return false;
    }

    public function delActionMant($idAction)
    {
        $resultado = parent::showRes("DELETE FROM tbl_accionesMant WHERE idAccion = {$idAction}");

        if ($resultado)
            return true;
        else
            return false;
    }
}