<?php

require_once APP.'/config/Conection.php';

class Model extends Conection
{
	public function showRes($query)
	{
		$c = parent::conectar();
		$c->set_charset('utf8');
		$res = $c->query($query);
		parent::desconectar();
		return $res;
	}

	public function validata($data)
	{
		$x = true;

		foreach ($data as $value)
		{
			if (empty($value))
			{
				$x = false;
				break;
			}
		}

		return $x;
	}

	public function logInfo($email, $pass)
	{
		$resultado = $this->showRes("SELECT * FROM tbl_usuarios");

		$x = false;

		while ($fila = $resultado->fetch_assoc())
		{
		    if ($fila['email'] == $email && password_verify($pass, $fila['pass']))
		    {
		    	$x = true;
		    	$id = $fila['idUsuario'];
		    	break;
		    }
		}

		if ($x)
		{
			$conexion = parent::conectar();
			$conexion->set_charset('utf8');
			$stm = $conexion->prepare("call SP_login(?);");
			$stm->bind_param('i', $id);
			$stm->execute();

   			$stm->bind_result(
   				$idUsuario,
		        $nivel
   			);

			if (!empty($stm))
			{
				while($stm->fetch())
				{
                    $resultado = $this->showRes("SELECT * FROM `tbl_entradas` WHERE idUsuario = {$idUsuario}");

                    if ($resultado->num_rows < 1)
                    {
                        return 'firstIn';
                    }
                    else
                    {
    					$_SESSION['log'] = [
    						'id' => $idUsuario,
    				        'level' => $nivel
    					];

                        $this->showRes("INSERT INTO `tbl_entradas`(`idUsuario`) VALUES ({$idUsuario})");
                    }
				}

				$stm->close();

				return true;
			}
			else
			{
				$stm->close();

				return false;
			}
		}
		else
		{
			return false;
		}
	}

    public function salida($id)
    {
        $this->showRes("INSERT INTO `tbl_salidas`(`idUsuario`) VALUES ({$id})");
    }

	public function infoUsuario($idUsuario)
    {
    	$resultado = $this->showRes("CALL SP_infoUsuario(".$idUsuario.")");

    	if ($resultado->num_rows > 0)
		{
			$info = [];

			while ($fila = $resultado->fetch_assoc())
			{
                $info['id'] = $fila['idUsuario'];
				$info['nombre1'] = $fila['firstName'];
				$info['nombre2'] = $fila['secndName'];
                $info['apellido1'] = $fila['firstApe'];
                $info['apellido2'] = $fila['secndApe'];
				$info['email'] = $fila['email'];
				$info['nivel'] = $fila['Nivel'];
				$info['cargo'] = $fila['Cargo'];
				$info['region'] = $fila['Region'];
				$info['pais'] = $fila['Pais'];
                $info['foto'] = base64_encode($fila['foto']);
                $info['estado'] = $fila['estado'];
			}

            $info['nombreCompleto'] = $info['nombre1']." ".$info['nombre2']." ".$info['apellido1']." ".$info['apellido2'];

			return $info;
		}
		else
		{
			return false;
		}

		$resultado->free();

		parent::desconectar();
    }

    public function actualizarUsuario($nombre1, $nombre2, $apellido1, $apellido2, $cargo, $idRegion)
    {
    	$idUsuario = $_SESSION['log']['id'];

    	$resultado = $this->showRes("CALL SP_actualizarUsuario('$nombre1', '$nombre2', '$apellido1', '$apellido2', '$cargo', $idRegion, $idUsuario)");

    	if ($resultado)
    		return true;
    	else
    		return false;
    }

	public function setCookieToken($email, $pass, $token)
	{
		$resultado = $this->showRes("INSERT INTO tbl_cookies VALUES ('".$email."', '".$pass."', '".$token."')");

		if($resultado)
			return true;
		else
			return false;
	}

	public function getCookieToken($token)
	{
		$resultado = $this->showRes("SELECT email, pass FROM tbl_cookies WHERE sessionToken = '".$token."'");

		$info = [];

		while ($campo = $resultado->fetch_assoc())
		{
		    $info['usuario'] = $campo['email'];
		    $info['pass'] = $campo['pass'];
		}

		if($resultado)
			return $info;
		else
			return false;
	}

	public function setResetToken($email, $key)
	{
		$resultado = $this->showRes("UPDATE tbl_usuarios SET token = '".$key."', fechaToken = NOW() WHERE email = '".$email."'");

		if ($resultado)
			return true;
		else
			return false;
	}

	public function validarToken($token)
	{
		if (strlen($token) == 50)
		{
			$resultado = $this->showRes("SELECT * FROM tbl_usuarios WHERE token = '".$token."'");

			if ($resultado->num_rows > 0)
				return true;
			else
				return false;
		}
	}

	public function recoverPassword($password, $token)
	{
        $res = $this->showRes("SELECT idUsuario FROM tbl_usuarios WHERE token = '{$token}'");

        if ($res->num_rows > 0)
        {
            $dato = $res->fetch_assoc();

            $id = $dato['idUsuario'];

            $this->showRes("INSERT INTO `tbl_entradas`(`idUsuario`) VALUES ({$id})");

            $token = $this->showRes("
                UPDATE
                    tbl_usuarios
                SET
                    fechaToken = NULL,
                    pass = '{$password}',
                    token = ''
                WHERE
                    idUsuario = {$id}");

    		if ($token)
    			return true;
    		else
    			return false;
        }
        else
        {
            return false;
        }
	}

	public function validaPassword($currentPass)
	{
		$resultado = $this->showRes("SELECT * FROM tbl_usuarios");

		$x = false;

		while ($fila = $resultado->fetch_assoc())
		{
		    if ($fila['idUsuario'] == $_SESSION['log']['id'] && password_verify($currentPass, $fila['pass']))
		    {
		    	$x = true;
		    	break;
		    }
		}

		return $x;
	}

	public function updatePassword($password)
	{
		$query = "
		UPDATE
			tbl_usuarios
		SET
			pass = '".$password."'
		WHERE
			idUsuario = ".$_SESSION['log']['id'];

		$resultado = $this->showRes($query);

		if ($resultado)
			return true;
		else
			return false;
	}

	public function listarPaises()
    {
        $paises = [];

        $paises = $this->showRes("SELECT * FROM tbl_Paises");

        while ($data = $paises->fetch_assoc()) {
            $paises['id'][] = $data['idPais'];
            $paises['pais'][] = $data['Pais'];
        }

        return $paises;
    }

	public function getIdPais($pais)
    {
        $pais = $this->showRes("SELECT idPais FROM tbl_Paises WHERE pais = '".$pais."'");

        $data = $pais->fetch_assoc();

        $idPais = $data['idPais'];

        return $idPais;
    }

    public function getIdRegion($region)
    {
        $pais = $this->showRes("SELECT idRegion FROM tbl_Regiones WHERE region = '".$region."'");

        $data = $pais->fetch_assoc();

        $idPais = $data['idRegion'];

        return $idPais;
    }

	public function listarRegiones($idPais)
    {
        $regiones = [];

        $resultado = $this->showRes("SELECT * FROM tbl_Regiones WHERE idPais = ".$idPais);

        while ($data = $resultado->fetch_assoc())
        {
            $regiones['id'][] = $data['idRegion'];
            $regiones['region'][] = $data['region'];
        }

        return $regiones;
    }

    public function listarVehiculos($idPais, $tipo)
    {
    	$vehiculos = [];

        switch ($tipo)
        {
            case 'motos':
                $query = "CALL SP_listaMotos(".$idPais.")";
                break;

            case 'autos':
                $query = "CALL SP_listaVehiculos(".$idPais.")";
                break;

            case 'all':
                $query = "CALL SP_allCars(".$idPais.")";
                break;

            default:
                $query = false;
                break;
        }

        if (!$query)
        {
            return false;
        }
        else
        {
            $resultado = $this->showRes($query);

            if ($resultado->num_rows > 0)
            {
    	        while ($data = $resultado->fetch_assoc())
    	        {
    	            $vehiculos['idVehiculo'][] = $data['idVehiculo'];
    	            $vehiculos['placa'][] = $data['numeroPlaca'];
    	            $vehiculos['color'][] = $data['color'];
    	            $vehiculos['marca'][] = $data['marca'];
    	            $vehiculos['modelo'][] = $data['modelo'];
    	            $vehiculos['anno'][] = $data['anno'];
    	            $vehiculos['motor'][] = $data['motor'];
    	            $vehiculos['chasis'][] = $data['chasis'];
    	            $vehiculos['vin'][] = $data['vin'];
    	            $vehiculos['proveedor'][] = $data['nombre'];
    	            $vehiculos['region'][] = $data['region'];
    	            $vehiculos['mtto'][] = $data['proxMtto'];
    	            $vehiculos['kilometraje'][] = $data['kilometraje'];
    	            $vehiculos['tipo'][] = $data['tipo'];
    	            $vehiculos['estado'][] = $data['estado'];
    	        }

    	        return $vehiculos;
            }
            else
            {
            	return false;
            }
        }
    }

    public function infoAuto($idVehiculo)
    {
    	$vehiculo = [];

        $resultado = $this->showRes("CALL SP_infoVehiculo(".$idVehiculo.")");

        while ($data = $resultado->fetch_assoc())
        {
            $vehiculo['idVehiculo'] = $data['idVehiculo'];
            $vehiculo['placa'] = $data['numeroPlaca'];
            $vehiculo['color'] = $data['color'];
            $vehiculo['marca'] = $data['marca'];
            $vehiculo['modelo'] = $data['modelo'];
            $vehiculo['anno'] = $data['anno'];
            $vehiculo['motor'] = $data['motor'];
            $vehiculo['chasis'] = $data['chasis'];
            $vehiculo['vin'] = $data['vin'];
            $vehiculo['proveedor'] = $data['nombre'];
            $vehiculo['region'] = $data['region'];
            $vehiculo['mtto'] = $data['proxMtto'];
            $vehiculo['kilometraje'] = $data['kilometraje'];
            $vehiculo['tipo'] = $data['tipo'];
            $vehiculo['estado'] = $data['estado'];
        }

        return $vehiculo;
    }

    public function validaBitacora($idVehiculo)
    {
        $resultado = $this->showRes("SELECT COUNT(*) AS 'total' FROM tbl_Bitacoras WHERE idVehiculo = {$idVehiculo}");

        $dato = $resultado->fetch_assoc();

        return ($dato['total'] <= 1) ? true : false;
    }

    public function isAvailable($idCar)
    {
        $query = "SELECT idEstado FROM tbl_Bitacoras WHERE idVehiculo = ".$idCar." ORDER BY idEstado DESC LIMIT 1";
        $resultado = $this->showRes($query);

        $data = $resultado->fetch_assoc();

        if ($data['idEstado'] == '6')
            return true;
        else
            return false;
    }

    public function listarProveedores($idPais)
    {
        $providers = [];

        $resultado = $this->showRes("CALL SP_listaProveedores(".$idPais.")");

        if ($resultado->num_rows > 0)
        {
            while ($data = $resultado->fetch_assoc())
            {
                $providers['idProveedor'][] = $data['idProveedor'];
                $providers['nombre'][] = $data['nombre'];
                $providers['contacto'][] = $data['contacto'];
                $providers['telefono'][] = $data['telefono'];
                $providers['tipo'][] = $data['tipoProveedor'];
                $providers['pais'][] = $data['pais'];
            }

            return $providers;
        }
        else
        {
            return false;
        }
    }

    public function listarMarcas()
    {
    	$marcas = [];

        $resultado = $this->showRes("SELECT * FROM tbl_marcas");

        while ($data = $resultado->fetch_assoc())
        {
            $marcas['idMarca'][] = $data['idMarca'];
            $marcas['marca'][] = $data['marca'];
        }

        return $marcas;
    }

    public function listarTipoAutos()
    {
    	$tipo = [];

        $resultado = $this->showRes("SELECT * FROM tbl_tipoVehiculos");

        while ($data = $resultado->fetch_assoc())
        {
            $tipo['id'][] = $data['idTipoVehiculo'];
            $tipo['tipo'][] = $data['vehiculo'];
        }

        return $tipo;
    }

    public function infoProveedor($id)
    {
    	$provider = [];

        $resultado = $this->showRes("CALL SP_infoProveedor(".$id.")");

        if ($resultado->num_rows > 0)
        {
            while ($data = $resultado->fetch_assoc())
            {
                $provider['idProveedor'] = $data['idProveedor'];
                $provider['nombre'] = $data['nombre'];
                $provider['contacto'] = $data['contacto'];
                $provider['telefono'] = $data['telefono'];
                $provider['tipo'] = $data['tipoProveedor'];
                $provider['pais'] = $data['pais'];
            }
            return $provider;
        }
        else
        {
            return false;
        }
    }

    public function autosProveedores($idProveedor)
    {
    	$vehiculos = [];

        $resultado = $this->showRes("CALL SP_autosProveedores(".$idProveedor.")");

        if ($resultado->num_rows > 0)
        {
	        while ($data = $resultado->fetch_assoc())
	        {
	            $vehiculos['idVehiculo'][] = $data['idVehiculo'];
	            $vehiculos['placa'][] = $data['numeroPlaca'];
	            $vehiculos['color'][] = $data['color'];
	            $vehiculos['marca'][] = $data['marca'];
	            $vehiculos['proveedor'][] = $data['nombre'];
	            $vehiculos['region'][] = $data['region'];
	            $vehiculos['mtto'][] = $data['proxMtto'];
	            $vehiculos['kilometraje'][] = $data['kilometraje'];
	            $vehiculos['tipo'][] = $data['vehiculo'];
	            $vehiculos['estado'][] = $data['estado'];
	        }

	        return $vehiculos;
        }
        else
        {
        	return false;
        }
    }

    public function listarProductCatalog()
    {
    	$productos = [];

        $resultado = $this->showRes("CALL SP_listarProductCatalog()");

        if ($resultado->num_rows > 0)
        {
	        while ($data = $resultado->fetch_assoc())
	        {
	            $productos['idProducto'][] = $data['idProducto'];
	            $productos['nombre'][] = $data['nombre'];
	            $productos['idCategoria'][] = $data['idCategoria'];
	            $productos['categoria'][] = $data['categoria'];
	        }

	        return $productos;
        }
        else
        {
        	return false;
        }
    }

    public function listarActionCatalog()
    {
        $acciones = [];

        $resultado = $this->showRes("SELECT * FROM tbl_accionesMant");

        if ($resultado->num_rows > 0)
        {
            while ($data = $resultado->fetch_assoc())
            {
                $acciones['idAccion'][] = $data['idAccion'];
                $acciones['accion'][] = $data['accion'];
            }

            return $acciones;
        }
        else
        {
            return false;
        }
    }

    public function nombreProducto($id)
    {
    	$resultado = $this->showRes("SELECT nombre FROM tbl_Productos WHERE idProducto = ".$id);

    	if ($resultado->num_rows > 0)
        {
        	$data = $resultado->fetch_assoc();

        	$nombre = $data['nombre'];

	        return $nombre;
        }
        else
        {
        	return false;
        }
    }

    public function infoAccion($id)
    {
        $resultado = $this->showRes("SELECT * FROM tbl_accionesMant WHERE idAccion = ".$id);

        if ($resultado->num_rows > 0)
        {
            $accion = [];

            while($data = $resultado->fetch_assoc())
            {
                $accion['idAccion'] = $data['idAccion'];
                $accion['accion'] = $data['accion'];
            }

            return $accion;
        }
        else
        {
            return false;
        }
    }

    public function infoProducto($id)
    {
    	$resultado = $this->showRes("SELECT * FROM tbl_Productos WHERE idProducto = ".$id);

    	if ($resultado->num_rows > 0)
        {
        	$producto = [];

        	while($data = $resultado->fetch_assoc())
        	{
        		$producto['idProducto'] = $data['idProducto'];
        		$producto['idCategoria'] = $data['idCategoria'];
        		$producto['nombre'] = $data['nombre'];
        	}

	        return $producto;
        }
        else
        {
        	return false;
        }
    }

    public function infoCategoria($id)
    {
        $resultado = $this->showRes("SELECT * FROM tbl_Categorias WHERE idCategoria = ".$id);

        if ($resultado->num_rows > 0)
        {
            $categoria = [];

            while($data = $resultado->fetch_assoc())
            {
                $categoria['idCategoria'] = $data['idCategoria'];
                $categoria['nombre'] = $data['nombre'];
            }

            return $categoria;
        }
        else
        {
            return false;
        }
    }

    public function listarCategorias()
    {
    	$categorias = [];

        $resultado = $this->showRes("SELECT * FROM tbl_Categorias");

        if ($resultado->num_rows > 0)
        {
	        while ($data = $resultado->fetch_assoc())
	        {
	            $categorias['idCategoria'][] = $data['idCategoria'];
	            $categorias['nombre'][] = $data['nombre'];
	        }

	        return $categorias;
        }
        else
        {
        	return false;
        }
    }

    public function tipoProveedores()
    {
    	$tipos = [];
        
        $resultado = $this->showRes("SELECT * FROM tbl_TipoProveedores");

        if ($resultado->num_rows > 0)
        {	
	        while ($data = $resultado->fetch_assoc())
	        {
	            $tipos['idTipo'][] = $data['idTipoProveedor'];
	            $tipos['tipo'][] = $data['tipoProveedor'];
	        }

	        return $tipos;
        }
        else
        {
        	return false;
        }
    }

    public function insertarCompra($numfactura, $total, $proveedor, $idUsuario)
    {
    	$resultado = $this->showRes("CALL SP_insertarCompra('".$numfactura."', ".$total.", ".$proveedor.", ".$idUsuario.")");

    	if ($resultado->num_rows > 0)
    	{
    		$data = $resultado->fetch_assoc();

    		$idCompra = $data['idCompra'];

    		return $idCompra;
    	}
    	else
    	{
    		return false;
    	}
    }

    public function insertarDetalleCompra($idProducto, $cantidad, $punitario, $total, $idCompra)
    {
    	$resultado = $this->showRes("CALL SP_insertarDetalleCompra(".$idProducto.", ".$cantidad.", ".$punitario.", ".$total.", ".$idCompra.")");

    	if ($resultado)
    		return true;
    	else
    		return false;
    }

    public function insertarInventario($idProducto, $cantidad, $numFactura, $punitario)
    {
    	$resultado = $this->showRes("CALL SP_insertarInventario(".$idProducto.", ".$cantidad.", ".$numFactura.", ".$punitario.")");

    	if ($resultado)
    		return true;
    	else
    		return false;
    }

    public function listaInventario()
    {
    	$resultado = $this->showRes("CALL SP_listaInventario()");

    	if ($resultado)
    	{
    		$inventario = [];

    		while ($data = $resultado->fetch_assoc())
    		{
    		    $inventario['idProducto'][] = $data['idProducto'];
    		    $inventario['nombre'][] = $data['nombre'];
    		    $inventario['categoria'][] = $data['categoria'];
    		    $inventario['cantidad'][] = $data['cantidad'];
    		}

    		return $inventario;
    	}
    	else
    	{
    		return false;
    	}
    }

    public function listaProductoInventario($idProducto)
    {
    	$resultado = $this->showRes("CALL SP_listaProductoInventario(".$idProducto.")");

    	if ($resultado)
    	{
    		$detalleProducto = [];

    		while ($data = $resultado->fetch_assoc())
    		{
    		    $detalleProducto['idProducto'][] = $data['idProducto'];
    		    $detalleProducto['nombre'][] = $data['nombre'];
    		    $detalleProducto['categoria'][] = $data['categoria'];
    		    $detalleProducto['cantidad'][] = $data['cantidad'];
    		    $detalleProducto['numFactura'][] = $data['numFactura'];
    		    $detalleProducto['precioUnitario'][] = $data['precioUnitario'];
    		}

    		return $detalleProducto;
    	}
    	else
    	{
    		return false;
    	}
    }

    public function bitacorasVehiculo($idVehiculo)
    {
        $resultado = $this->showRes("CALL SP_bitacorasVehiculo(".$idVehiculo.")");

        if ($resultado->num_rows > 0)
        {
            $info = [];

            while ($data = $resultado->fetch_assoc())
            {
                $info['idBitacora'][] = $data['idBitacora'];
                $info['piloto'][] = $data['piloto'];
                $info['fechaSalida'][] = $data['fechaSalida'];
                $info['horaSalida'][] = $data['horaSalida'];
                $info['fechaEntrada'][] = $data['fechaEntrada'];
                $info['horaEntrada'][] = $data['horaEntrada'];
                $info['kmInicial'][] = $data['kmInicial'];
                $info['kmFinal'][] = $data['kmFinal'];
                $info['destino'][] = $data['destino'];
                $info['motivo'][] = $data['motivo'];
                $info['observaciones'][] = $data['observaciones'];
                $info['estado'][] = $data['estado'];
            }

            return $info;
        }
        else
        {
            return false;
        }
    }

    public function profilePics()
    {
        $resultado = $this->showRes("SELECT * FROM tbl_fotoPerfiles");

        if ($resultado)
        {
            $fotos = [];

            while ($data = $resultado->fetch_assoc())
            {
                $fotos['idFoto'][] = $data['idFoto'];
                $fotos['nombre'][] = $data['nombre'];
                $fotos['formato'][] = $data['formato'];
                $fotos['foto'][] = base64_encode($data['foto']);
            }

            return $fotos;
        }
        else
        {
            return false;
        }
    }

    public function updtPicProfile($idFoto, $idUsuario)
    {
        $resultado = $this->showRes("UPDATE tbl_usuarios SET idFoto = ".$idFoto." WHERE idUsuario = ".$idUsuario);

        if ($resultado)
            return true;
        else
            return false;
    }

    public function lastKm($idVehiculo)
    {
        $resultado = $this->showRes("SELECT kmFinal FROM tbl_Bitacoras WHERE idVehiculo = {$idVehiculo} ORDER BY idBitacora DESC LIMIT 1");

        $data = $resultado->fetch_assoc();

        return $data['kmFinal'];
    }

    public function ultimoDetalleHoja($idVehiculo)
    {
        $resultado = $this->showRes("CALL SP_ultimoDetalleHoja(".$idVehiculo.")");

        if ($resultado->num_rows > 0)
        {
            while ($datos = $resultado->fetch_assoc())
            {
                $detalle['idItem'][] = $datos['idItem'];
                $detalle['item'][] = $datos['item'];
                $detalle['idEstado'][] = $datos['idEstado'];
                $detalle['estado'][] = $datos['estado'];
            }

            return $detalle;
        }
        else
        {
            return false;
        }
    }

    public function ultimaHoja($idVehiculo)
    {
        $resultado = $this->showRes("CALL SP_ultimaHoja(".$idVehiculo.")");

        if ($resultado->num_rows > 0)
        {
            while ($datos = $resultado->fetch_assoc())
            {
                $hoja['idHoja'] = $datos['idHoja'];
                $hoja['idBitacora'] = $datos['idBitacora'];
                $hoja['nivelGas'] = $datos['nivelGas'];
                $hoja['observacionInicial'] = $datos['observacionInicial'];
                $hoja['observacionFinal'] = $datos['observacionFinal'];
            }

            return $hoja;
        }
        else
        {
            return false;
        }
    }

    public function nuevaBitacora($bitacora)
    {
        $hoja = $this->ultimaHoja($_SESSION['autoSelected']);

        if (empty($bitacora[8]))
            $observacion = $hoja['observacionFinal'];
        else
            $observacion = $bitacora[8];

        $query = "CALL SP_nuevaBitacora(".$bitacora[0].", ".$bitacora[1].", '".$bitacora[2]."', '".$bitacora[3]."', ".$bitacora[4].", '".$bitacora[5]."', '".$bitacora[6]."', '".$bitacora[7]."', '".$observacion."', '".$hoja['nivelGas']."')";

        $resultado = $this->showRes($query);

        if ($resultado->num_rows > 0)
        {
            $data = $resultado->fetch_assoc();

            $idHoja = $data['idHoja'];

            $x = true;

            /*Insertando detalles de items*/

            $detalle = $_SESSION['ultimoDetalleHoja'];

            foreach ($detalle['idItem'] as $key => $value)
            {
                $query = "INSERT INTO tbl_detalleHojas VALUES (".$idHoja.", ".$detalle['idItem'][$key].", ".$bitacora[9][$key].")";

                $res = $this->showRes($query);
            }

            if ($x)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public function autoItemsList()
    {
        $items = [];

        $resultado = $this->showRes("SELECT * FROM tbl_autoItems WHERE idEstado = 1");

        if ($resultado->num_rows > 0)
        {
            while ($data = $resultado->fetch_assoc())
            {
                $items['id'][] = $data['idItem'];
                $items['item'][] = $data['item'];
            }

            return $items;
        }
        else
        {
            return false;
        }

        if ($resultado)
            return true;
        else
            return false;
    }

    public function infoBitacora($idUsuario)
    {
        $resultado = $this->showRes("SELECT * FROM tbl_Bitacoras WHERE idUsuario = ".$idUsuario." AND idEstado = 7");

        if ($resultado->num_rows > 0)
        {
            $bitacora = [];

            while ($data = $resultado->fetch_assoc())
            {
                $bitacora['idBitacora'] = $data['idBitacora'];
                $bitacora['idVehiculo'] = $data['idVehiculo'];
                $bitacora['kmInicial'] = $data['kmInicial'];
                $bitacora['kmFinal'] = $data['kmFinal'];
                $bitacora['fechaSalida'] = $data['fechaSalida'];
                $bitacora['fechaEntrada'] = $data['fechaEntrada'];
                $bitacora['HoraSalida'] = $data['HoraSalida'];
                $bitacora['HoraEntrada'] = $data['HoraEntrada'];
                $bitacora['idUsuario'] = $data['idUsuario'];
                $bitacora['destino'] = $data['destino'];
                $bitacora['motivo'] = $data['motivo'];
                $bitacora['observaciones'] = $data['observaciones'];
                $bitacora['idEstado'] = $data['idEstado'];
            }

            return $bitacora;
        }
        else
        {
            return false;
        }
    }

    public function validarBitacora($idUsuario)
    {
        $resultado = $this->showRes("SELECT * FROM tbl_Vehiculos");

        if ($resultado->num_rows > 0)
        {
            $query = "SELECT * FROM tbl_Bitacoras WHERE idUsuario = ".$idUsuario." AND idEstado = 7";

            $resultado = $this->showRes($query);

            if ($resultado->num_rows > 0)
            {
                $val = $resultado->fetch_assoc();

                return URL."?req=finishLog&val=".$val['idVehiculo'];
            }
            else
            {
                return "available";
            }
        }
        else
        {
            return "is_empty";
        }
    }

    public function listarEstados()
    {
        $resultado = $this->showRes("SELECT * FROM tbl_Estados");

        if ($resultado->num_rows > 0)
        {
            while($data = $resultado->fetch_assoc())
            {
                $estados['idEstado'][] = $data['idEstado'];
                $estados['estado'][] = $data['Estado'];
            }

            return $estados;
        }
        else
        {
            return false;
        }
    }

    public function ultimoDetalleGolpes($idVehiculo)
    {
        $res = $this->showRes("SELECT idHoja FROM `tbl_hojaSalidas` hs INNER JOIN tbl_Bitacoras b on hs.idBitacora = b.idBitacora WHERE b.idVehiculo = {$idVehiculo}");

        $idHojas = [];

        while ($dato = $res->fetch_assoc())
        {
            $idHojas[] = $dato['idHoja'];
        }

        $idHojas = array_reverse($idHojas);

        foreach ($idHojas as $value)
        {
            $res = $this->showRes("CALL SP_verificaDetalleGolpe({$value})");
            $dato = $res->fetch_assoc();
            if ($dato['resultado'] == 'success') { $idHoja = $value; break; }
        }

        $resultado = $this->showRes("CALL SP_ultimoDetalleGolpes(".$idHoja.")");

        if ($resultado->num_rows > 0)
        {
            while ($data = $resultado->fetch_assoc())
            {
                $golpes['idHoja'][] = $data['idHoja'];
                $golpes['costado'][] = $data['costado'];
                $golpes['foto'][] = $data['foto'];
                $golpes['idTipo'][] = $data['idTipo'];
                $golpes['tipo'][] = $data['tipo'];
                $golpes['descripcion'][] = $data['descripcion'];
            }
            return $golpes;
        }
        else
        {
            return false;
        }
    }

    public function updateBitacora($kmFinal, $idBitacora, $horaEntrada)
    {
        $resultado = $this->showRes("CALL SP_updateBitacora({$kmFinal}, {$idBitacora}, '{$horaEntrada}')");

        if ($resultado)
            return true;
        else
            return false;
    }

    public function updateHojaSalida($nivelGas, $hojaObserv, $idBitacora)
    {
        $resultado = $this->showRes("CALL SP_updateHoja('".$hojaObserv."', '".$nivelGas."', ".$idBitacora.")");

        if ($resultado)
            return true;
        else
            return false;
    }

    public function updateDetalleHoja($detalleHoja, $idBitacora)
    {
        $resultado = $this->showRes("SELECT idHoja FROM tbl_hojasalidas WHERE idBitacora = ".$idBitacora);

        $dato = $resultado->fetch_assoc();

        $idHoja = $dato['idHoja'];

        $detalle = $_SESSION['ultimoDetalleHoja'];

        $validata = true;

        foreach ($detalle['idItem'] as $key => $value)
        {
            $query = "UPDATE tbl_detalleHojas SET idEstado = ".$detalle['idEstado'][$key]." WHERE idHoja = ".$idHoja;

            $res = $this->showRes($query);

            if (!$res) { $validata = false; break; }
        }

        if ($validata)
            return true;
        else
            return false;
    }

    public function insertarDetalleGolpes($idHoja, $rutas)
    {
        $lados = ["Derecha", "Izquierda", "Adelante", "Atrás"];

        $valida = true;

        foreach ($lados as $key => $lado)
        {
            $query = "INSERT INTO `tbl_detalleGolpes` VALUES ({$idHoja}, '{$lado}', '{$rutas[$key]}', 1)";

            if (!$resultado = $this->showRes($query)) { $valida = false; break; }
        }

        if ($valida)
            return true;
        else
            return false;
    }

    public function updateDetalleGolpes($idHoja, $rutas)
    {
        $lados = ["Derecha", "Izquierda", "Adelante", "Atrás"];

        $valida = true;

        foreach ($lados as $key => $lado)
        {
            $query = "UPDATE `tbl_detalleGolpes` SET foto = '{$rutas[$key]}'WHERE idHoja = {$idHoja} AND costado = '{$lado}'";

            if (!$resultado = $this->showRes($query)) { $valida = false; break; }
        }

        if ($valida)
            return true;
        else
            return false;
    }

    public function listaVales()
    {
        $resultado = $this->showRes("CALL SP_listaVales()");

        if ($resultado)
        {
            if ($resultado->num_rows > 0)
            {
                while ($data = $resultado->fetch_assoc())
                {
                    if ($data['tipoIngreso'] == 'vouchers')
                        $vales['tipoIngreso'][] = 'Vale de combustible';
                    else
                        $vales['tipoIngreso'][] = 'Factura de caja chica';

                    $vales['idVehiculo'][] = $data['idVehiculo'];
                    $vales['nIngreso'][] = $data['nIngreso'];
                    $vales['valor'][] = $data['valor'];
                    $vales['fechaIngreso'][] = $data['fechaIngreso'];
                    $vales['fecha'][] = $data['fecha'];
                    $vales['hora'][] = $data['hora'];
                    $vales['numeroPlaca'][] = $data['numeroPlaca'];
                    $vales['tipo'][] = $data['tipo'];
                    $vales['piloto'][] = $data['piloto'];
                    $vales['kmSalida'][] = $data['kmSalida'];
                    $vales['kmEntrada'][] = $data['kmEntrada'];
                }

                return $vales;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public function verificaLicencia($idUsuario)
    {
        $resultado = $this->showRes("CALL SP_verificaLicencia(".$idUsuario.")");

        if ($resultado)
        {
            $data = $resultado->fetch_assoc();

            if ($data['resultado'] == 'success')
                return true;
            else
                return false;
        }
    }

    public function infoLicencia($idUsuario)
    {
        $resultado = $this->showRes("CALL SP_infoLicencia(".$idUsuario.")");

        if ($resultado)
        {
            while ($data = $resultado->fetch_assoc())
            {
                $licencia['idLicencia'][] = $data['idLicencia'];
                $licencia['idUsuario'][] = $data['idUsuario'];
                $licencia['firstName'][] = $data['firstName'];
                $licencia['secndName'][] = $data['secndName'];
                $licencia['firstApe'][] = $data['firstApe'];
                $licencia['secndApe'][] = $data['secndApe'];
                $licencia['nombre'][] = $data['nombre'];
                $licencia['nit'][] = $data['nit'];
                $licencia['dui'][] = $data['dui'];
                $licencia['fechaExpd'][] = $data['fechaExpd'];
                $licencia['fechaVenc'][] = $data['fechaVenc'];
                $licencia['tipoLicencia'][] = $data['tipoLicencia'];
                $licencia['idEstado'][] = $data['idEstado'];
                $licencia['aprobacion'][] = $data['aprobacion'];
            }

            return $licencia;
        }
        else
        {
            return false;
        }
    }

    public function insertarLicencia($licencia)
    {
        $query = "CALL SP_insertarLicencia(".$_SESSION['log']['id'].",'".$licencia['nit']."', '".$licencia['dui']."', '".$licencia['fechaExp']."', '".$licencia['fechaVenc']."', '".$licencia['tipo']."')";

        $resultado = $this->showRes($query);

        $data = $resultado->fetch_assoc();

        if ($data['resultado'] == 'success')
            return true;
        else
            return false;
    }

    public function actualizarLicencia($licencia, $idLicencia)
    {
        $query = "CALL SP_actualizarLicencia(".$idLicencia.",'".$licencia['nit']."', '".$licencia['dui']."', '".$licencia['fechaExp']."', '".$licencia['fechaVenc']."', '".$licencia['tipo']."')";

        $resultado = $this->showRes($query);

        if ($resultado)
            return true;
        else
            return false;
    }

    public function listaUsuarios()
    {
        $resultado = $this->showRes("CALL SP_listaUsuarios()");

        if ($resultado)
        {
            if ($resultado->num_rows > 0)
            {
                while ($data = $resultado->fetch_assoc())
                {
                    $lista['idUsuario'][] = $data['idUsuario'];
                    $lista['nombre1'][] = $data['nombre1'];
                    $lista['nombre2'][] = $data['nombre2'];
                    $lista['apellido1'][] = $data['apellido1'];
                    $lista['apellido2'][] = $data['apellido2'];
                    $lista['nombreCompleto'][] = $data['nombreCompleto'];
                    $lista['region'][] = $data['region'];
                    $lista['email'][] = $data['email'];
                    $lista['cargo'][] = $data['cargo'];
                    $lista['idEstado'][] = $data['idEstado'];
                    $lista['estado'][] = $data['estado'];
                }

                return $lista;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public function listaPilotos()
    {
        $resultado = $this->showRes("CALL SP_listaPilotos()");

        if ($resultado)
        {
            if ($resultado->num_rows > 0)
            {
                while ($data = $resultado->fetch_assoc())
                {
                    $lista['idUsuario'][] = $data['idUsuario'];
                    $lista['nombre1'][] = $data['nombre1'];
                    $lista['nombre2'][] = $data['nombre2'];
                    $lista['apellido1'][] = $data['apellido1'];
                    $lista['apellido2'][] = $data['apellido2'];
                    $lista['nombreCompleto'][] = $data['nombreCompleto'];
                    $lista['region'][] = $data['region'];
                    $lista['email'][] = $data['email'];
                    $lista['cargo'][] = $data['cargo'];
                    $lista['tipoLicencia'][] = $data['tipoLicencia'];
                    $lista['estado'][] = $data['estado'];
                    $lista['aprobacion'][] = $data['aprobacion'];
                }

                return $lista;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public function bitacorasUsuarios($idUsuario)
    {
        $resultado = $this->showRes("CALL SP_bitacorasUsuarios(".$idUsuario.")");

        if ($resultado)
        {
            if ($resultado->num_rows > 0)
            {
                while ($data = $resultado->fetch_assoc())
                {
                    $info['numeroPlaca'][] = $data['numeroPlaca'];
                    $info['color'][] = $data['color'];
                    $info['marca'][] = $data['marca'];
                    $info['idBitacora'][] = $data['idBitacora'];
                    $info['idVehiculo'][] = $data['idVehiculo'];
                    $info['kmInicial'][] = $data['kmInicial'];
                    $info['kmFinal'][] = $data['kmFinal'];
                    $info['fechaSalida'][] = $data['fechaSalida'];
                    $info['fechaEntrada'][] = $data['fechaEntrada'];
                    $info['HoraSalida'][] = $data['HoraSalida'];
                    $info['HoraEntrada'][] = $data['HoraEntrada'];
                    $info['idUsuario'][] = $data['idUsuario'];
                    $info['destino'][] = $data['destino'];
                    $info['motivo'][] = $data['motivo'];
                    $info['observaciones'][] = $data['observaciones'];
                    $info['idEstado'][] = $data['idEstado'];
                }
                return $info;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public function autorizarLicPiloto($idUsuario)
    {
        if ($this->showRes("UPDATE tbl_licencias SET aprobacion = 1 WHERE idUsuario = ".$idUsuario))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function desautorizarLicPiloto($idUsuario)
    {
        if ($this->showRes("UPDATE tbl_licencias SET aprobacion = 0 WHERE idUsuario = ".$idUsuario))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function validaLicencia($idUsuario)
    {
        $resultado = $this->showRes("SELECT * FROM tbl_licencias WHERE idUsuario = ".$idUsuario." AND aprobacion = 1");

        if ($resultado)
        {
            if ($resultado->num_rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public function listarAccionesMant()
    {
        $resultado = $this->showRes("SELECT * FROM tbl_accionesMant");

        if ($resultado)
        {
            if ($resultado->num_rows > 0)
            {
                while ($data = $resultado->fetch_assoc())
                {
                    $acciones['idAccion'][] = $data['idAccion'];
                    $acciones['accion'][] = $data['accion'];
                }

                return $acciones;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public function validaVehiculos()
    {
        $resultado = $this->showRes("SELECT * FROM tbl_Vehiculos");

        if ($resultado)
        {
            if ($resultado->num_rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public function insertComment($comment, $idUsuario)
    {
        if (!empty($comment))
        {
            $query = "INSERT INTO tbl_comentarioSistema VALUES (NULL, ".$idUsuario.", '".$comment."', CURDATE(), TIME_FORMAT(NOW(), '%H:%i'))";
            $resultado = $this->showRes($query);
        }
    }

    public function delComment($idComment, $idUsuario)
    {
        $res = $this->showRes("SELECT * FROM tbl_comentarioSistema WHERE idComentario = ".$idComment." AND idUsuario = ".$idUsuario);

        if ($res && $res->num_rows > 0)
        {
            $resultado = $this->showRes("DELETE FROM tbl_comentarioSistema WHERE idComentario = ".$idComment);
        }
    }

    public function getComments()
    {
        $resultado = $this->showRes("SELECT * FROM tbl_comentarioSistema ORDER BY idComentario DESC");

        if ($resultado)
        {
            if ($resultado->num_rows > 0)
            {
                while ($data = $resultado->fetch_assoc())
                {
                    $comments['idComentario'][] = $data['idComentario'];
                    $comments['idUsuario'][] = $data['idUsuario'];
                    $comments['comentario'][] = $data['comentario'];
                    $comments['fecha'][] = $data['fecha'];
                    $comments['hora'][] = $data['hora'];
                }

                return $comments;
            }
            else
            {
                return false;
            }
        }
    }

    public function getIdCar($placa)
    {
        $query = "SELECT idVehiculo FROM tbl_Vehiculos WHERE numeroPlaca = '{$placa}'";
        $dato = $this->showRes($query)->fetch_assoc();
        return $dato['idVehiculo'];
    }

    public function validarDatosAuto($placa, $motor, $chasis, $id = null)
    {
        $query = (is_null($id)) ? "CALL SP_valDatCarCreate('{$placa}', '{$motor}', '{$chasis}')" : "CALL SP_valDatCarUpdate('{$placa}', '{$motor}', '{$chasis}', {$id})";

        $resultado = $this->showRes($query);

        $dato = $resultado->fetch_assoc();

        return ($dato['resultado'] == 'success') ? true : false;
    }

    public function ingresarAuto($datos)
    {
        $query = "CALL SP_ingresarAuto('".$datos['placa']."', '".$datos['color']."', ".$datos['marca'].", '".$datos['modelo']."', ".$datos['anno'].", '".$datos['motor']."', '".$datos['chasis']."', '".$datos['vin']."', ".$datos['proveedor'].", ".$datos['region'].", ".$datos['mtto'].", ".$datos['km'].", ".$datos['tipo'].", ".$_SESSION['log']['id'].")";

        $resultado = $this->showRes($query);

        if ($resultado)
        {
            $resultado = $this->showRes("SELECT idBitacora FROM tbl_Bitacoras ORDER BY idBitacora DESC LIMIT 1");

            if ($resultado->num_rows > 0)
            {
                $data = $resultado->fetch_assoc();

                return $data["idBitacora"];
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public function insertarHojaSalida($idBitacora, $info_hoja)
    {
        $resultado = $this->showRes("CALL SP_primerHojaSalida(".$idBitacora.")");

        if ($resultado->num_rows > 0)
        {
            $data = $resultado->fetch_assoc();
            $idHoja = $data['idHoja'];

            $query = "INSERT INTO `tbl_detalleHojas` VALUES ";
            foreach ($info_hoja as $key => $value)
            {
                if ($key == (count($info_hoja)-1)) {
                    $query .= "(".$idHoja.", ".$value.", 9);";
                }else {
                    $query .= "(".$idHoja.", ".$value.", 9),";
                }
            }

            $resultado = $this->showRes($query);

            if ($resultado) return $idHoja; else return false;
        }
        else
        {
            return false;
        }
    }
}