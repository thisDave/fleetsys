<?php require_once APP."/views/master/header.php"; ?>

<!-- DataTables -->
<link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

<?php require_once APP."/views/master/eng-nav.php"; ?>

<?php $bitacora = $model->bitacorasVehiculo($_SESSION['val']); ?>
<?php $autoPerfil = $model->infoAuto($_SESSION['val']); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1><?= $autoPerfil['placa'] ?> <?= $autoPerfil['marca'] ?> <?= $autoPerfil['modelo'] ?></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= URL ?>?request=home">Inicio</a></li>
              <li class="breadcrumb-item"><a href="<?= URL ?>?req=bitacoras&val=cars">Bitácoras</a></li>
              <li class="breadcrumb-item active">Vehículo</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Registros actuales</h3>
                <h3 class="card-title float-right">
                  <?php if ($bitacora['estado'][count($bitacora['idBitacora'])-1] == 'En parqueo'): ?>
                    <span class="badge badge-success">Actualmente en parqueo</span>
                  <?php else: ?>
                    <span class="badge badge-danger">Actualmente en ruta</span>
                  <?php endif ?>
                </h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered table-striped datable">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Piloto</th>
                    <th>Fecha Salida</th>
                    <th>Hora Salida</th>
                    <th>Km Salida</th>
                    <th>Fecha Entrada</th>
                    <th>Hora Entrada</th>
                    <th>Km Entrada</th>
                    <th>Destino</th>
                    <th>Motivo</th>
                    <th>Acciones</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php if ($bitacora): ?>  
                  <?php $No = 1; ?>
                  <?php foreach ($bitacora['idBitacora'] as $key => $value): ?>
                    <tr>
                      <td><?= $No ?></td>
                      <td><?= $bitacora['piloto'][$key] ?></td>
                      <td><?= date_format(date_create($bitacora['fechaSalida'][$key]), 'd-M-Y') ?></td>
                      <td><?= $bitacora['horaSalida'][$key] ?></td>
                      <td><?= $bitacora['kmInicial'][$key] ?></td>
                      <td><?= date_format(date_create($bitacora['fechaEntrada'][$key]), 'd-M-Y') ?></td>
                      <td><?= $bitacora['horaEntrada'][$key] ?></td>
                      <td><?= $bitacora['kmFinal'][$key] ?></td>
                      <td><?= $bitacora['destino'][$key] ?></td>
                      <td><?= $bitacora['motivo'][$key] ?></td>
                      <td>
                        <a href="#" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#proximamente">
                          Ver Hoja de salida
                        </a>
                      </td>
                    </tr>
                  <?php endforeach ?>
                  <?php endif ?>
                  </tbody>
                </table>
                 <!-- Modal -->
                <div class="modal fade" id="proximamente" tabindex="-1" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-body">
                                <h1 class="display-4 text-primary">
                                    Próximamente <i class="fad fa-digging"></i>
                                </h1>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
              <div class="card-footer">
                <a href="<?= URL ?>?request=bitacoras" class="btn btn-dark btn-sm">Volver</a>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
                  
                

<!-- REQUIRED SCRIPTS -->

<?php require_once APP."/views/master/footer_js.php"; ?>

<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="dist/js/datatable.js"></script>

<?php require_once APP."/views/master/footer_end.php"; ?>