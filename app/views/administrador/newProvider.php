<?php require_once APP."/views/master/header.php"; ?>

<!-- SweetAlert2 -->
<link rel="stylesheet" href="plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">

<?php require_once APP."/views/master/admin-nav.php"; ?>

<?php $providers = $model->listarProveedores($model->getIdPais($user['pais'])); ?>
<?php $tipos = $model->tipoProveedores(); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Nuevo Proveedor</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= URL ?>?request=home">Inicio</a></li>
                        <li class="breadcrumb-item active">Nuevo Proveedor</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-12">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Nuevo Proveedor</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form action="<?= URL ?>" method="post" accept-charset="utf-8">
                            <div class="card-body">
                                <div class="form-row">
                                    <div class="col-12 col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="Nombre">Nombre del proveedor</label>
                                            <input type="text" class="form-control" id="Nombre" name="nombre" required autofocus>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="tipo">Tipo de proveedor</label>
                                            <select class="form-control" id="tipo" name="tipo" required>
                                                <?php foreach ($tipos['idTipo'] as $key => $value): ?>
                                                <option value="<?= $tipos['idTipo'][$key] ?>">
                                                    <?= $tipos['tipo'][$key] ?>
                                                </option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-12 col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="contacto">Contacto</label>
                                            <input type="text" class="form-control" id="contacto" name="contacto" value="Sin Datos" required>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="telefono">Teléfono</label>
                                            <input type="text" class="form-control" id="telefono" name="telefono" value="Sin Datos" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" name="newProvider" class="btn btn-primary">Registrar Proveedor</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.card -->
                </div>
                <!--/.col (left) -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- REQUIRED SCRIPTS -->

<?php require_once APP."/views/master/footer_js.php"; ?>

<!-- SweetAlert2 -->
<script src="plugins/sweetalert2/sweetalert2.min.js"></script>

<?= $objController->sweetAlert(2000); ?>

<?php require_once APP."/views/master/footer_end.php"; ?>