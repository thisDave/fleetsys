<?php require_once APP."/views/master/header.php"; ?>

<!-- SweetAlert2 -->
<link rel="stylesheet" href="plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
<!-- Select2 -->
<link rel="stylesheet" href="plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<!-- DataTables -->
<link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

<?php require_once APP."/views/master/admin-nav.php"; ?>

<?php $providers = $model->listarProveedores($model->getIdPais($user['pais'])); ?>
<?php $products = $model->listarProductCatalog(); ?>
<?php $categorias = $model->listarCategorias(); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <?php if (isset($_SESSION['ingresoProductos'])): ?>

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Nueva factura de mantenimiento</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= URL ?>?request=home">Inicio</a></li>
                        <li class="breadcrumb-item active">Ingresos</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-12">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Ingresar producto</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col-12 col-sm-6 col-md-2">
                                    <div class="form-group">
                                        <label>No. Factura</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">N°</span>
                                            </div>
                                            <input type="text" class="form-control" value="<?= $_SESSION['factura'] ?>" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-2">
                                    <div class="form-group">
                                        <label>Total factura</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">$</span>
                                            </div>
                                            <input type="text" class="form-control" value="<?= $_SESSION['total-factura'] ?>" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label>Proveedor</label>
                                        <select class="form-control" disabled>
                                            <?php foreach ($providers['idProveedor'] as $key => $value): ?>
                                            <?php if ($providers['idProveedor'][$key] == $_SESSION['proveedor']): ?>
                                            <option value="<?= $providers['idProveedor'][$key] ?>" selected>
                                                <?= $providers['nombre'][$key] ?>
                                            </option>
                                            <?php endif ?>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-5">
                                    <div class="form-group">
                                        <button type="button" class="btn btn-link text-danger float-right" data-toggle="modal" data-target="#cancel">
                                            <i class="far fa-trash-alt"></i> Cancelar Todo
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <?php if (!isset($_SESSION['dataVerificada'])): ?>
                            <div class="form-row">
                                <div class="col-12 col-sm-6 col-md-12">
                                    <div class="form-group">
                                        <button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#nuevoIngreso">
                                            <i class="fas fa-edit"></i> Editar encabezado
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <form action="<?= URL ?>" method="post" accept-charset="utf-8">
                                <div class="form-row">
                                    <div class="col-12 col-sm-6 col-md-1">
                                        <div class="form-group">
                                            <label for="cantidad">Cantidad</label>
                                            <input type="number" class="form-control" id="cantidad" min="1" name="cantidad" required autofocus>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="producto">Acción o producto</label>
                                            <select class="form-control select2" id="producto" name="producto" style="width: 100%;" required>
                                                <?php foreach ($products['idProducto'] as $key => $value): ?>
                                                <option value="<?= $products['idProducto'][$key] ?>">
                                                    <?= $products['nombre'][$key] ?>
                                                </option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 col-md-2">
                                        <div class="form-group">
                                            <label for="punitario">P. Unitario</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">$</span>
                                                </div>
                                                <input type="text" class="form-control" id="punitario" name="punitario" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-12 col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <button type="submit" name="addProd" class="btn btn-primary">Ingresar</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <?php else: ?>
                            <div class="form-row">
                                <div class="col-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <?php if (isset($_SESSION['editBill'])): ?>
                                        <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#editEntry">
                                            <i class="fas fa-check"></i> Editar ingreso
                                        </button>
                                        <?php else: ?>
                                        <button type="button" class="btn btn-blue" data-toggle="modal" data-target="#saveEntry">
                                            <i class="fas fa-check"></i> Finalizar ingreso
                                        </button>
                                        <?php endif ?>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <a href="<?= URL ?>?event=delVerify" class="btn btn-outline-info float-right">
                                            <i class="fas fa-edit"></i> Seguir editando
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <?php endif ?>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
                <!--/.col (left) -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

    <?php if (isset($_SESSION['productos'])): ?>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-12">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <!-- /.card-header -->
                        <!-- form start -->
                        <div class="card-body">
                            <table class="table table-bordered table-striped datable">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Cantidad</th>
                                        <th>Producto</th>
                                        <th>Precio Unitario</th>
                                        <th>Total</th>
                                        <?php if (!isset($_SESSION['dataVerificada'])): ?>
                                        <th>Acciones</th>
                                        <?php endif ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $x = 1; ?>
                                    <?php foreach ($_SESSION['productos']['cantidad'] as $key => $value): ?>
                                    <tr>
                                        <td><?= $x ?></td>
                                        <td><?= $_SESSION['productos']['cantidad'][$key] ?></td>
                                        <?php $nombre = $model->nombreProducto($_SESSION['productos']['producto'][$key]); ?>
                                        <td><?= $nombre ?></td>
                                        <td>$<?= $_SESSION['productos']['punitario'][$key] ?></td>
                                        <td>$<?= $_SESSION['productos']['totalProd'][$key] ?></td>
                                        <?php if (!isset($_SESSION['dataVerificada'])): ?>
                                        <td>
                                            <a href="<?= URL ?>?event=delProd&val=<?= $_SESSION['productos']['id'][$key] ?>" class="btn btn-sm btn-danger">
                                                Eliminar
                                            </a>
                                        </td>
                                        <?php endif ?>
                                    </tr>
                                    <?php $x++; ?>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
                <!--/.col (left) -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    <?php endif ?>
    <?php endif ?>
</div>
<!-- /.content-wrapper -->

<div class="modal fade" id="nuevoIngreso" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Factura de mantenimiento</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= URL ?>" method="post" accept-charset="utf-8">
                    <div class="card-body">
                        <div class="form-row">
                            <div class="col-12 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label for="factura">No. Factura</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">N°</span>
                                        </div>
                                        <?php if (isset($_SESSION['factura'])): ?>
                                        <input type="text" class="form-control" id="factura" name="factura" value="<?= $_SESSION['factura'] ?>" required autofocus>
                                        <?php else: ?>
                                        <input type="text" class="form-control" id="factura" name="factura" required autofocus>
                                        <?php endif ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label for="total">Total factura</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">$</span>
                                        </div>
                                        <?php if (isset($_SESSION['total-factura'])): ?>
                                        <input type="text" class="form-control" id="total" name="total" value="<?= $_SESSION['total-factura'] ?>" required>
                                        <?php else: ?>
                                        <input type="text" class="form-control" id="total" name="total" required>
                                        <?php endif ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-12 col-sm-6 col-md-12">
                                <div class="form-group">
                                    <label for="proveedor">Proveedor</label>
                                    <select class="form-control" id="proveedor" name="proveedor" required>
                                        <option></option>
                                        <?php foreach ($providers['idProveedor'] as $key => $value): ?>

                                            <?php if ($providers['tipo'][$key] != 'Venta de Vehiculos' && $providers['nombre'][$key] != 'Educo'): ?>

                                                <?php if (isset($_SESSION['proveedor'])): ?>

                                                    <?php if ($providers['idProveedor'][$key] == $_SESSION['proveedor']): ?>

                                                        <option value="<?= $providers['idProveedor'][$key] ?>" selected>
                                                            <?= $providers['nombre'][$key] ?>
                                                        </option>

                                                    <?php else: ?>

                                                        <option value="<?= $providers['idProveedor'][$key] ?>">
                                                            <?= $providers['nombre'][$key] ?>
                                                        </option>

                                                    <?php endif ?>

                                                <?php else: ?>

                                                    <option value="<?= $providers['idProveedor'][$key] ?>">
                                                        <?= $providers['nombre'][$key] ?>
                                                    </option>

                                                <?php endif ?>

                                            <?php endif ?>

                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <a href="<?= URL ?>?req=preordenProfile&val=<?= $_SESSION['preordenAbierta'] ?>" class="btn btn-danger float-left">Volver</a>
                        <?php if (isset($_SESSION['factura'])): ?>
                        <button type="submit" class="btn btn-info float-right" name="updateEntry">Actualizar ingreso</button>
                        <?php else: ?>
                        <button type="submit" class="btn btn-success float-right" name="newEntry">Iniciar ingreso</button>
                        <?php endif ?>
                    </div>
                </form>
            </div>
            <div class="modal-footer bg-primary justify-content-between">
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="cancel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="card-body text-center">
                    <p class="display-7"><strong> ¿Realmente desea cancelar el ingreso de la factura? </strong></p>
                    <p class="display-7"><strong> Se perderá todo el progreso del registro. </strong></p>
                </div>
                <div class="card-footer">
                        <button type="button" class="btn btn-default float-left" data-dismiss="modal">Cerrar</button>
                        <a href="<?= URL ?>?event=cancelEntry" class="btn btn-danger float-right">Cancelar ingreso</a>
                </div>
            </div>
            <div class="modal-footer bg-primary justify-content-between">
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<?php if (isset($_SESSION['dataVerificada'])): ?>

<div class="modal fade" id="saveEntry">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="card-body text-center">
                    <p class="display-7"><strong> ¿Realmente desea finalizar el ingreso de la factura? </strong></p>
                    <p class="display-7"><strong> Los registros se guardarán permanentemente. </strong></p>
                </div>
                <div class="card-footer">
                    <button type="button" class="btn btn-default float-left" data-dismiss="modal">Cerrar</button>
                    <a href="<?= URL ?>?event=saveBill" class="btn btn-success float-right">
                        <i class="fas fa-check-double"></i> Guardar y Finalizar
                    </a>
                </div>
            </div>
            <div class="modal-footer bg-primary justify-content-between">
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="editEntry">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="card-body text-center">
                    <p class="display-7"><strong> ¿Realmente desea editar el ingreso de la factura? </strong></p>
                    <p class="display-7"><strong> Los registros se guardarán permanentemente. </strong></p>
                </div>
                <div class="card-footer">
                    <button type="button" class="btn btn-default float-left" data-dismiss="modal">Cerrar</button>
                    <a href="<?= URL ?>?event=editBillEntry&val=<?= $_SESSION['val'] ?>" class="btn btn-warning float-right">
                        <i class="fas fa-check-double"></i> Editar y Finalizar
                    </a>
                </div>
            </div>
            <div class="modal-footer bg-primary justify-content-between">
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<?php endif ?>

<!-- REQUIRED SCRIPTS -->

<?php require_once APP."/views/master/footer_js.php"; ?>

<!-- SweetAlert2 -->
<script src="plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Select2 -->
<script src="plugins/select2/js/select2.full.min.js"></script>
<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="dist/js/datatable.js"></script>

<?= $objController->sweetAlert(2000) ?>

<!-- /********************************************************************/ -->

<?php if (isset($_SESSION['alertResultOne']) && $_SESSION['alertResult'] == false): ?>

<meta http-equiv="refresh" content="3;URL=<?= URL ?>?delalert=alertResultOne">

<script>
  var Toast = Swal.mixin({
    toast: false,
    position: 'center',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true
  });

  Toast.fire({
    icon: 'error',
    title: 'Error ingresando datos',
    text: 'Por favor, revise los datos ingresados'
  });
</script>

<?php else: ?>

    <?php if (!isset($_SESSION['ingresoProductos'])): ?>
    <script>
        $(document).ready(function() {
            $('#nuevoIngreso').modal("show");
        });
    </script>
    <?php else: ?>
    <script>
        $(document).ready(function () {
            $('.select2').select2({
                theme: 'bootstrap4'
            })
        });
    </script>
    <?php endif ?>

<?php endif ?>

<?php require_once APP."/views/master/footer_end.php"; ?>
