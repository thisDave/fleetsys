<?php require_once APP."/views/master/header.php"; ?>

<!-- DataTables -->
<link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<!-- SweetAlert2 -->
<link rel="stylesheet" href="plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">

<?php require_once APP."/views/master/admin-nav.php"; ?>

<?php if (isset($_SESSION['delResult'])) { unset($_SESSION['delResult']); } ?>

<?php $providers = $model->listarProveedores($model->getIdPais($user['pais'])); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Proveedores</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= URL ?>?request=home">Inicio</a></li>
              <li class="breadcrumb-item active">Proveedores</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Lista de Proveedores</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered table-striped datable">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Proveedor</th>
                    <th>Contacto</th>
                    <th>Número de telefono</th>
                    <th>Tipo</th>
                    <th>Acciones</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php if ($providers): ?>
                  <?php $x = 1; ?>
                  <?php foreach ($providers['idProveedor'] as $key => $value): ?>
                  <tr>
                    <td><?= $x ?></td>
                    <td><?= $providers['nombre'][$key] ?></td>
                    <td><?= $providers['contacto'][$key] ?></td>
                    <td><?= $providers['telefono'][$key] ?></td>
                    <td><?= $providers['tipo'][$key] ?></td>
                    <td>
                      <a href="<?= URL ?>?req=providerProfile&val=<?= $providers['idProveedor'][$key] ?>" class="btn btn-sm btn-primary">
                        Ver detalle
                      </a>
                    </td>
                  </tr>
                  <?php $x++; ?>
                  <?php endforeach ?>
                  <?php endif ?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<!-- REQUIRED SCRIPTS -->

<?php require_once APP."/views/master/footer_js.php"; ?>

<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="dist/js/datatable.js"></script>
<!-- SweetAlert2 -->
<script src="plugins/sweetalert2/sweetalert2.min.js"></script>

<?= $objController->SweetAlert(2000); ?>

<?php require_once APP."/views/master/footer_end.php"; ?>