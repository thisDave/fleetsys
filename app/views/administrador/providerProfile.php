<?php require_once APP."/views/master/header.php"; ?>

<!-- DataTables -->
<link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<!-- SweetAlert2 -->
<link rel="stylesheet" href="plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">

<?php require_once APP."/views/master/admin-nav.php"; ?>

<?php $provider = $model->infoProveedor($_SESSION['val']); ?>

<?php $vehiculos = $model->autosProveedores($provider['idProveedor']); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Detalles del proveedor</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= URL ?>?request=home">Inicio</a></li>
              <li class="breadcrumb-item"><a href="<?= URL ?>?request=providers">Proveedores</a></li>
              <li class="breadcrumb-item active">Perfil Proveedor</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle" src="dist/img/user.svg" alt="User profile picture">
                </div>

                <h3 class="profile-username text-center"><?= $provider['nombre'] ?></h3>

                <p class="text-muted text-center"><?= $provider['tipo'] ?></p>

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>País</b> <br>
                    <a class="float-left"><?= $provider['pais'] ?></a>
                  </li>
                  <li class="list-group-item">
                    <b>Contacto</b> <br>
                    <a class="float-left"><?= $provider['contacto'] ?></a>
                  </li>
                  <li class="list-group-item">
                    <b>Teléfono</b> <br>
                    <a class="float-left"><?= $provider['telefono'] ?></a>
                  </li>
                  <li class="list-group-item">
                    <button type="button" class="btn btn-sm btn-danger float-left" data-toggle="modal" data-target="#del-profile">
                      Eliminar proveedor
                    </button>
                    <a href="<?= URL ?>?request=providers" class="btn btn-sm btn-secondary float-right">Volver</a>
                  </li>
                </ul>
                <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#edit-profile">
                  Editar proveedor
                </button>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item">
                    <a class="nav-link active" href="#settings" data-toggle="tab">Información</a>
                  </li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="active tab-pane" id="settings">
                    <table class="table table-bordered table-striped datable">
	                  <thead>
	                  <tr>
	                    <th>Placa</th>
	                    <th>Marca</th>
	                    <th>Color</th>
	                    <th>Región</th>
                      <th>Tipo</th>
	                    <th>Acción</th>
	                  </tr>
	                  </thead>
	                  <tbody>
	                  <?php if ($vehiculos): ?>
	                  <?php foreach ($vehiculos['idVehiculo'] as $key => $value): ?>
	                  <tr>
	                    <td><?= $vehiculos['placa'][$key] ?></td>
	                    <td><?= $vehiculos['marca'][$key] ?></td>
	                    <td><?= $vehiculos['color'][$key] ?></td>
	                    <td><?= $vehiculos['region'][$key] ?></td>
                      <td><?= $vehiculos['tipo'][$key] ?></td>
	                    <td>
	                      <a href="<?= URL ?>?req=autoProfile&val=<?= $vehiculos['idVehiculo'][$key] ?>" class="btn btn-sm btn-primary">
	                        Ver Perfil
	                      </a>
	                    </td>
	                  </tr>
	                  <?php endforeach ?>
	                  <?php endif ?>
	                  </tbody>
	                </table>
                  </div>
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

  </div>
  <!-- /.content-wrapper -->

  <div class="modal fade" id="edit-profile">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Editar Proveedor</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="<?= URL ?>" method="post" accept-charset="utf-8">
            <div class="card-body">
              <div class="form-group">
                <label for="nombre">Nombre</label>
                <input type="text" class="form-control" id="nombre" name="nombre" value="<?= $provider['nombre'] ?>" required>
              </div>
              <div class="form-group">
                <label for="contacto">Contacto</label>
                <input type="text" class="form-control" id="contacto" name="contacto" value="<?= $provider['contacto'] ?>" required>
              </div>
              <div class="form-group">
                <label for="telefono">Teléfono</label>
                <input type="text" class="form-control" id="telefono" name="telefono" value="<?= $provider['contacto'] ?>" required>
              </div>
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
              <button type="button" class="btn btn-default float-left" data-dismiss="modal">Cerrar</button>
              <input type="hidden" name="id" value="<?= $provider['idProveedor'] ?>">
              <button type="submit" name="updateProvider" class="btn btn-primary float-right">Actualizar datos</button>
            </div>
          </form>
        </div>
        <div class="modal-footer bg-primary justify-content-between">
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

  <div class="modal fade" id="del-profile">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Eliminar Proveedor</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="<?= URL ?>" method="get" accept-charset="utf-8">
            <p>
              ¿Está seguro que desea Eliminar este proveedor?
            </p>
            <!-- /.card-body -->

            <div class="card-footer">
              <button type="button" class="btn btn-default float-left" data-dismiss="modal">Cerrar</button>
              <input type="hidden" name="val" value="<?= $provider['idProveedor'] ?>">
              <button type="submit" class="btn btn-danger float-right" name="event" value="delProvider">Eliminar proveedor</button>
            </div>
          </form>
        </div>
        <div class="modal-footer bg-primary justify-content-between">
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

<!-- REQUIRED SCRIPTS -->

<?php require_once APP."/views/master/footer_js.php"; ?>

<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="dist/js/datatable.js"></script>
<!-- SweetAlert2 -->
<script src="plugins/sweetalert2/sweetalert2.min.js"></script>

<?= $objController->SweetAlert(2000); ?>

<?php require_once APP."/views/master/footer_end.php"; ?>