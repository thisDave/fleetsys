<?php require_once APP."/views/master/header.php"; ?>

<!-- DataTables -->
<link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

<?php require_once APP."/views/master/admin-nav.php"; ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Usuarios Educo El Salvador</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?= URL ?>?request=home">Inicio</a></li>
						<li class="breadcrumb-item">Usuarios</li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header">
							<h3 class="card-title">Lista de usuarios</h3>
						</div>
						<!-- /.card-header -->
						<div class="card-body">
							<table class="table table-bordered table-striped datable">
								<thead>
									<tr>
										<th>No.</th>
										<th>Nombre completo</th>
										<th>Region</th>
										<th>Cargo</th>
										<th>Tipo Licencia</th>
										<th>Estado Licencia</th>
										<th>Uso de vehículos</th>
										<th>Acciones</th>
									</tr>
								</thead>
								<tbody>
									<?php $pilots = $model->listaPilotos(); ?>
									<?php if ($pilots): ?> 
										<?php foreach ($pilots['idUsuario'] as $key => $value): ?>
											<tr>
												<td><?= $key+1 ?></td>
												<td><?= $pilots['nombreCompleto'][$key] ?></td>
												<td><?= $pilots['region'][$key] ?></td>
												<td><?= $pilots['cargo'][$key] ?></td>
												<td><?= $pilots['tipoLicencia'][$key] ?></td>
												<td><?= $pilots['estado'][$key] ?></td>
												<td>
													<?php if ($pilots['aprobacion'][$key] == 0): ?>
													<span class="badge badge-danger">No autorizado</span>
													<?php else: ?>
													<span class="badge badge-success">Autorizado</span>
													<?php endif ?>
												</td>
												<td>
													<a href="<?= URL ?>?req=userProfile&val=<?= $pilots['idUsuario'][$key] ?>" class="btn btn-sm btn-primary">
														Ver perfil
													</a>
												</td>
											</tr>
										<?php endforeach ?>
									<?php endif ?>
								</tbody>
							</table>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container-fluid -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- REQUIRED SCRIPTS -->

<?php require_once APP."/views/master/footer_js.php"; ?>

<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="dist/js/datatable.js"></script>

<?php require_once APP."/views/master/footer_end.php"; ?>