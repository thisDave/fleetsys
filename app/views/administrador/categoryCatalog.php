<?php require_once APP."/views/master/header.php"; ?>

<!-- DataTables -->
<link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<!-- SweetAlert2 -->
<link rel="stylesheet" href="plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">

<?php require_once APP."/views/master/admin-nav.php"; ?>

<?php $categorias = $model->listarCategorias(); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Banco de categorías</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= URL ?>?request=home">Inicio</a></li>
              <li class="breadcrumb-item active">Categorías</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <?php if (isset($_SESSION['editCategory'])): ?>

    <?php $categoria = $model->infoCategoria($_SESSION['editCategory']); ?>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-4">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Actualizar categoría</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="<?= URL ?>" method="post">
                <div class="card-body">
                  <div class="form-group">
                    <label for="categoria">Categoría</label>
                    <input type="text" class="form-control" id="categoria" name="categoria" value="<?= $categoria['nombre'] ?>" autofocus>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" name="updateCategory" value="<?= $categoria['idCategoria'] ?>" class="btn btn-sm btn-blue">
                    Actualizar
                  </button>
                  <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#delCategory">
                    Eliminar
                  </button>
                  <a href="<?= URL ?>?event=backCategories" class="btn btn-sm btn-secondary">Volver</a>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (left) -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->

    <div class="modal fade" id="delCategory">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Eliminar categoría</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form action="<?= URL ?>" method="get" accept-charset="utf-8">
              <p>
                ¿Está seguro que desea eliminar esta categoría?
              </p>
              <!-- /.card-body -->

              <div class="card-footer">
                <button type="button" class="btn btn-default float-left" data-dismiss="modal">Cerrar</button>
                <input type="hidden" name="val" value="<?= $categoria['idCategoria'] ?>">
                <button type="submit" class="btn btn-danger float-right" name="event" value="delCategory">
                  Eliminar categoría
                </button>
              </div>
            </form>
          </div>
          <div class="modal-footer bg-primary justify-content-between">
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <?php else: ?>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Lista de categorías</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered table-striped datable">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Categoría</th>
                    <th>Acciones</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php if ($categorias): ?>
                  <?php $x = 1; ?>
                  <?php foreach ($categorias['idCategoria'] as $key => $value): ?>
                  <tr>
                    <td><?= $x ?></td>
                    <td><?= $categorias['nombre'][$key] ?></td>
                    <td>
                      <form action="<?= URL ?>" method="get">
                        <button type="submit" name="editCategory" value="<?= $categorias['idCategoria'][$key] ?>" class="btn btn-sm btn-primary">
                          Editar categoría
                        </button>
                      </form>
                    </td>
                  </tr>
                  <?php $x++; ?>
                  <?php endforeach ?>
                  <?php endif ?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <button type="button" class="btn btn-sm btn-blue" data-toggle="modal" data-target="#newCategory">
                  Nueva Categoría
                </button>
              </div>
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->

    <?php endif ?>
    
  </div>
  <!-- /.content-wrapper -->

  <div class="modal fade" id="newCategory">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Nueva categoría</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="<?= URL ?>" method="get" accept-charset="utf-8">
            <div class="card-body">
              <div class="form-group">
                <input type="text" class="form-control" id="nombre" name="val" placeholder="Nombre de la categoría" required>
              </div>
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
              <button type="button" class="btn btn-default float-left" data-dismiss="modal">Cerrar</button>
              <button type="submit" name="event" value="newCategory" class="btn btn-success float-right">Agregar categoría</button>
            </div>
          </form>
        </div>
        <div class="modal-footer bg-primary justify-content-between">
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

<!-- REQUIRED SCRIPTS -->

<?php require_once APP."/views/master/footer_js.php"; ?>

<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="dist/js/datatable.js"></script>
<!-- SweetAlert2 -->
<script src="plugins/sweetalert2/sweetalert2.min.js"></script>

<?= $objController->sweetAlert(2000); ?>

<?php require_once APP."/views/master/footer_end.php"; ?>