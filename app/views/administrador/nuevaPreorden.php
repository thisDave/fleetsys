<?php require_once APP."/views/master/header.php"; ?>

<!-- DataTables -->
<link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<!-- Select2 -->
<link rel="stylesheet" href="plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<!-- SweetAlert2 -->
<link rel="stylesheet" href="plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">

<?php require_once APP."/views/master/admin-nav.php"; ?>

<?php
if (isset($_SESSION['val'])) {
	$_SESSION['inicioPreorden'] = true;
	$_SESSION['tipoPreorden'] = 'interna';
	$datos = explode('(', $_SESSION['val']);
	$_SESSION['placa'] = $datos[0];
	$_SESSION['kmActual'] = $datos[1];
}
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Nueva Pre orden</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?= URL ?>?request=home">Inicio</a></li>
						<li class="breadcrumb-item">Mantenimientos</li>
						<li class="breadcrumb-item active">Nueva Pre orden</li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<?php if ($model->validaVehiculos()): ?>

		<?php if (isset($_SESSION['inicioPreorden'])): ?>

			<!-- Main content -->
			<section class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-12">
							<div class="card card-primary">
								<div class="card-header">
									<h3 class="card-title">Pre-orden <?= $_SESSION['tipoPreorden'] ?></h3>
								</div>
								<!-- /.card-header -->
								<div class="card-body">
									<!-- form start -->
									<form action="<?= URL ?>" method="post" accept-charset="utf-8">
										<div class="card-body">
											<div class="form-row">
												<div class="col-12 col-sm-6 col-md-2">
													<div class="form-group">
													<label>Fecha emisión</label>
														<input type="text" class="form-control" value="<?= $objController->date_time('date'); ?>" disabled>
													</div>
												</div>
												<div class="col-12 col-sm-6 col-md-2">
													<div class="form-group">
														<div class="form-group">
															<label for="placa">Placa del vehículo</label>
															<?php if (!isset($_SESSION['placa'])) { $_SESSION['placa'] = ''; $_SESSION['kmActual'] = ''; } ?>
															<input type="text" class="form-control" id="placa" name="placa" value="<?= $_SESSION['placa'] ?>" required>
														</div>
													</div>
												</div>
												<div class="col-12 col-sm-6 col-md-2">
													<div class="form-group">
													<label>Kilometraje actual</label>
													<input type="text" class="form-control" id="kmActual" value="<?= $_SESSION['kmActual'] ?>" disabled>
													</div>
												</div>
											</div>
											<div class="form-row">
												<div class="col-12 col-sm-6 col-md-5">
													<div class="form-group">
														<label for="accion">Acciones</label>
														<?php $acciones = $model->listarAccionesMant(); ?>
														<select class="form-control select2" id="accion" name="accion" style="width: 100%;" required>
															<?php foreach ($acciones['idAccion'] as $key => $value): ?>
																<option value="<?= $key ?>"><?= $acciones['accion'][$key] ?></option>
															<?php endforeach ?>
														</select>
													</div>
												</div>
											</div>
											<div class="form-row">
												<div class="col-12 col-sm-6 col-md-12">
													<div class="form-group">
														<button type="submit" name="addAction" class="btn btn-sm btn-primary">Agregar acción</button>
													</div>
												</div>
											</div>
										</div>
									</form>
									<div class="card-body">
										<table class="table table-bordered table-striped datable">
											<thead>
												<tr>
													<th>No</th>
													<th>Mantenimiento</th>
													<th>Acción</th>
												</tr>
											</thead>
											<tbody>
												<?php if (isset($_SESSION['mmtos'])): ?>
													<?php if ($_SESSION['mmtos']): ?>
														<?php foreach ($_SESSION['mmtos']['id'] as $key => $value): ?>
															<tr>
																<td><?= $key+1 ?></td>
																<td><?= $_SESSION['mmtos']['accion'][$key] ?></td>
																<td>
																<a href="<?= URL ?>?delAction&val=<?= $key ?>" class="btn btn-sm btn-danger">
																	Eliminar item
																</a>
																</td>
															</tr>
														<?php endforeach ?>
													<?php endif ?>
												<?php endif ?>
											</tbody>
										</table>
									</div>
									<div class="card-body">
										<label for="comments">Comentarios</label>
										<?php $comentarios = (isset($_SESSION['comentarios'])) ? $_SESSION['comentarios'] : ''; ?>
										<textarea name="comments" id="comments" rows="2" class="form-control"><?= $comentarios ?></textarea>
									</div>
									<div class="card-footer">
										<a href="<?= URL ?>?newPreorder" class="btn btn-success">Enviar pre orden de mantenimiento</a>
										<a href="<?= URL ?>?cancelPreorden" class="btn btn-outline-danger float-right">Cancelar pre orden</a>
									</div>
								</div>
								<!-- /.card-body -->
							</div>
							<!-- /.card -->
						</div>
						<!-- /.col -->
					</div>
					<!-- /.row -->
				</div>
				<!-- /.container-fluid -->
			</section>

		<?php else: ?>

				<!-- Main content -->
				<section class="content">
					<div class="container-fluid">
						<div class="row">
							<div class="col-12">
								<div class="card card-primary">
									<div class="card-header">
										<h3 class="card-title">Nueva Pre orden</h3>
									</div>
									<!-- /.card-header -->
									<div class="card-body">
										<form action="<?= URL ?>" method="post" accept-charset="utf-8">
											<div class="form-row">
												<div class="col-12 col-sm-6 col-md-2">
													<div class="form-group">
														<label for="tipo">Tipo de pre orden:</label>
														<select id="tipo" name="tipo" class="form-control">
															<option value="interna">Interna</option>
															<option value="externa">Externa</option>
														</select>
													</div>
												</div>
											</div>
											<div class="form-row">
												<div class="col-12 col-sm-6 col-md-2">
													<div class="form-group">
														<button type="submit" class="btn btn-primary" name="iniciarPreorden">
															Iniciar pre orden
														</button>
													</div>
												</div>
											</div>
										</form>
									</div>
									<!-- /.card-body -->
								</div>
								<!-- /.card -->
							</div>
							<!-- /.col -->
						</div>
						<!-- /.row -->
					</div>
					<!-- /.container-fluid -->
				</section>

		<?php endif ?>

	<?php else: ?>

		<!-- Main content -->
		<section class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-12">
						<div class="card card-primary">
							<div class="card-header">
								<h3 class="card-title">Nueva Pre orden</h3>
							</div>
							<!-- /.card-header -->
							<div class="card-body">
								<div class="row">
									<div class="col-12 col-sm-6 col-md-6">
										<div class="info-box mb-3 bg-warning">
											<span class="info-box-icon">
												<i class="fas fa-empty-set"></i>
											</span>
											<div class="info-box-content">
												<span class="info-box-number">Actualmente no se encuentran vehículos registrados.</span>
											</div>
											<!-- /.info-box-content -->
										</div>
									</div>
								</div>
							</div>
							<!-- /.card-body -->
						</div>
						<!-- /.card -->
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->
			</div>
			<!-- /.container-fluid -->
		</section>

	<?php endif ?>
</div>
<!-- /.content-wrapper -->

<!-- REQUIRED SCRIPTS -->

<?php require_once APP."/views/master/footer_js.php"; ?>

<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="dist/js/datatable.js"></script>
<!-- Select2 -->
<script src="plugins/select2/js/select2.full.min.js"></script>
<!-- SweetAlert2 -->
<script src="plugins/sweetalert2/sweetalert2.min.js"></script>

<?= $objController->sweetAlert(2000); ?>

<script>
$(document).ready(function () {
	$('.select2').select2({
		theme: 'bootstrap4'
	});

	$("#placa").keyup(function() {
		var placa = $("#placa").val();
		var ruta = "buscarKm=&placa="+placa;
		$.ajax({
			type: 'POST',
			url: 'tools/data/search.php',
			data: ruta
		})
		.done(function(resultado){
			$("#kmActual").val(resultado);
		});
	});

	$("#comments").keyup(function () {
		let comment = $("#comments").val();
		let ruta = "comments="+comment;
		$.ajax({
			type: 'POST',
			url: 'tools/data/search.php',
			data: ruta
		});
	});
});
</script>

<?php require_once APP."/views/master/footer_end.php"; ?>