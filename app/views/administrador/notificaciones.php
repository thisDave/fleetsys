<?php require_once APP."/views/master/header.php"; ?>

<!-- DataTables -->
<link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<!-- SweetAlert2 -->
<link rel="stylesheet" href="plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">

<?php require_once APP."/views/master/admin-nav.php"; ?>

<?php $notify = $modelAdmin->getNotifies(); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Notificaciones</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?= URL ?>?request=home">Inicio</a></li>
						<li class="breadcrumb-item">Mantenimientos</li>
						<li class="breadcrumb-item active">Notificaciones</li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

		<!-- Main content -->
		<section class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-12">
						<div class="card card-primary">
							<div class="card-header">
								<h3 class="card-title">Notificaciones encontradas</h3>
							</div>
							<!-- /.card-header -->
							<div class="card-body">
								<div class="row">
									<div class="col-12">
										<table class="table table-bordered table-striped datable">
											<thead>
												<tr>
													<th>No</th>
													<th>Información</th>
													<th>Placa</th>
													<th>Km Actual</th>
													<th>Prox mtto</th>
													<th>Acciones</th>
												</tr>
											</thead>
											<tbody>
												<?php if ($notify): ?>
												<?php foreach ($notify['idAlerta'] as $key => $value): ?>
												<tr>
													<td><?= $value ?></td>
													<td><?= $notify['detalles'][$key] ?></td>
													<td><?= $notify['placa'][$key] ?></td>
													<td><?= $notify['kmActual'][$key] ?></td>
													<td><?= $notify['mtto'][$key] ?></td>
													<td><a href="<?= URL ?>?req=nuevaPreorden&val=<?= $pool['placa'][$key] ?>(<?= $pool['kmActual'][$key] ?>" class="btn btn-dark btn-sm">Realizar preorden</a></td>
												</tr>
												<?php endforeach ?>
												<?php endif ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<!-- /.card-body -->
						</div>
						<!-- /.card -->
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->
			</div>
			<!-- /.container-fluid -->
		</section>

</div>
<!-- /.content-wrapper -->

<!-- REQUIRED SCRIPTS -->

<?php require_once APP."/views/master/footer_js.php"; ?>

<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="dist/js/datatable.js"></script>
<!-- SweetAlert2 -->
<script src="plugins/sweetalert2/sweetalert2.min.js"></script>

<?= $objController->sweetAlert(2000); ?>

<?php require_once APP."/views/master/footer_end.php"; ?>