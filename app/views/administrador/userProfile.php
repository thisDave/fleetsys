<?php require_once APP . "/views/master/header.php"; ?>

<?php $usr = $model->infoUsuario($_SESSION['val']); ?>

<!-- DataTables -->
<link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<!-- SweetAlert2 -->
<link rel="stylesheet" href="plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">

<?php require_once APP."/views/master/admin-nav.php"; ?>

<?php $regiones = $model->listarRegiones($model->getIdPais($usr['pais'])); ?>

<?php $fotos = $model->profilePics(); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Administración de perfiles</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?= URL ?>?request=home">Inicio</a></li>
            <li class="breadcrumb-item"><a href="<?= URL ?>?request=users">Usuarios</a></li>
            <li class="breadcrumb-item active">Perfil de usuario</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="card card-primary card-outline">
            <div class="card-body box-profile">
              <div class="text-center">
                <img class="profile-user-img img-fluid img-circle" src="data:image/png;base64,<?= $usr['foto'] ?>" alt="User profile picture">
              </div>

              <h3 class="profile-username text-center"><?= $usr['nombre1'] ?> <?= $usr['apellido1'] ?></h3>

              <p class="text-muted text-center"><?= $usr['region'] ?></p>

              <ul class="list-group list-group-unbordered mb-3">
                <li class="list-group-item">
                  <b>País</b> <br>
                  <a class="float-left"><?= $usr['pais'] ?></a>
                </li>
                <li class="list-group-item">
                  <b>Cargo</b> <br>
                  <a class="float-left"><?= $usr['cargo'] ?></a>
                </li>
                <li class="list-group-item">
                  <b>E-mail</b> <br>
                  <a class="float-left"><?= $usr['email'] ?></a>
                </li>
              </ul>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="card">
            <div class="card-header p-2">
              <ul class="nav nav-pills">
                <li class="nav-item"><a class="nav-link active" href="#settings" data-toggle="tab">Información</a></li>
                <li class="nav-item"><a class="nav-link" href="#licencia" data-toggle="tab">Licencia</a></li>
                <li class="nav-item"><a class="nav-link" href="#bitacoras" data-toggle="tab">Registro de bitácoras</a></li>
              </ul>
            </div><!-- /.card-header -->
            <div class="card-body">
              <div class="tab-content">
                <div class="active tab-pane" id="settings">
                  <form class="form-horizontal" action="<?= URL ?>" method="post" accept-charset="utf-8">
                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Primer nombre</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" value="<?= $usr['nombre1'] ?>" disabled>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Segundo nombre</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" value="<?= $usr['nombre2'] ?>" disabled>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Primer Apellido</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" value="<?= $usr['apellido1'] ?>" disabled>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Segundo Apellido</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" value="<?= $usr['apellido2'] ?>" disabled>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="region" class="col-sm-2 col-form-label">Región</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" value="<?= $usr['region'] ?>" disabled>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Cargo</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" value="<?= $usr['cargo'] ?>" disabled>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Estado</label>
                      <div class="col-sm-10">
                        <?php if ($usr['estado'] == 'Activo'): ?>
                        <button type="button" class="btn btn-success">
                          <?= $usr['estado'] ?>
                        </button>
                        <?php else: ?>
                        <button type="button" class="btn btn-danger">
                          <?= $usr['estado'] ?>
                        </button>
                        <?php endif ?>
                      </div>
                    </div>
                  </form>
                </div>
                <!-- /. Settings -->

                <div class="tab-pane" id="licencia">
                  <?php if ($model->verificaLicencia($_SESSION['val'])): ?>
                  <?php $licencia = $model->infoLicencia($_SESSION['val']); ?> 
                  <div class="row">
                  <?php foreach ($licencia['idLicencia'] as $key => $value): ?>
                    <div class="col-12 col-sm-12 col-md-4">
                      <div class="card card-widget widget-user">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header bg-primary">
                          <h3 class="widget-user-username"><?= $licencia['tipoLicencia'][$key] ?></h3>
                        </div>
                        <div class="widget-user-image">
                          <img class="img-circle elevation-2" src="data:image/png;base64,<?= $usr['foto'] ?>" alt="User Avatar">
                        </div>
                        <div class="card-footer">
                          <div class="row">
                            <div class="col-sm-4 border-right">
                              <div class="description-block">
                                <h5 class="description-header">Expedido el:</h5>
                                <span class="description-text">
                                  <?= date_format(date_create($licencia['fechaExpd'][$key]), 'd/M/Y') ?>
                                </span>
                              </div>
                              <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4 border-right">
                              <div class="description-block">
                                <h5 class="description-header">Vence el:</h5>
                                <span class="description-text">
                                  <?= date_format(date_create($licencia['fechaVenc'][$key]), 'd/M/Y') ?>
                                </span>
                              </div>
                              <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4">
                              <div class="description-block">
                                <h5 class="description-header">Estado:</h5>
                                <?php if ($licencia['aprobacion'][$key] == 1): ?>
                                <span class="description-text badge badge-success">
                                  Autorizado
                                </span>
                                <?php else: ?>
                                <span class="description-text badge badge-danger">
                                  No Autorizado
                                </span>
                                <?php endif ?>
                              </div>
                              <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                          </div>
                          <!-- /.row -->
                        </div>
                      </div>
                      <!-- /.widget-user -->
                    </div>
                    <!-- Modal -->
                    <div class="modal fade" id="AprobarLicencia" tabindex="-1" aria-hidden="true">
                      <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Autorizar piloto</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                            <div class="row">
                              <div class="col-12">
                                <h5>
                                  ¿Autorizar a <?= $usr['nombre1'] ?> <?= $usr['apellido1'] ?> para el manejo de vehículos institucionales?
                                </h5>
                              </div>
                            </div>
                            <div class="row mt-3">
                              <div class="col-12">
                                <button type="button" class="btn btn-sm btn-default float-left" data-dismiss="modal">
                                  Cerrar
                                </button>
                                <a href="<?= URL ?>?autorizarPiloto=<?= $_SESSION['val'] ?>" class="btn btn-sm btn-success float-right">
                                  Autorizar piloto
                                </a>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer bg-primary">
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- /. Modal -->
                    <!-- Modal -->
                    <div class="modal fade" id="DesaprobarLicencia" tabindex="-1" aria-hidden="true">
                      <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Desautorizar piloto</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                            <div class="row">
                              <div class="col-12">
                                <h5>
                                  ¿Desautorizar a <?= $usr['nombre1'] ?> <?= $usr['apellido1'] ?> para el manejo de vehículos institucionales?
                                </h5>
                              </div>
                            </div>
                            <div class="row mt-3">
                              <div class="col-12">
                                <button type="button" class="btn btn-sm btn-default float-left" data-dismiss="modal">
                                  Cerrar
                                </button>
                                <a href="<?= URL ?>?desautorizarPiloto=<?= $_SESSION['val'] ?>" class="btn btn-sm btn-danger float-right">
                                  Desautorizar piloto
                                </a>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer bg-primary">
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- /. Modal -->
                  <?php endforeach ?>
                  </div>
                  <div class="row">
                    <div class="col-sm-12">
                      <?php if ($licencia['aprobacion'][$key] == 1): ?>
                      <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#DesaprobarLicencia">
                        <i class="fas fa-thumbs-up"></i> Desautorizar piloto
                      </button>
                      <?php else: ?>
                      <button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="#AprobarLicencia">
                        <i class="fas fa-thumbs-up"></i> Autorizar piloto
                      </button>
                      <?php endif ?>
                      
                    </div>
                  </div>
                  <?php else: ?>
                  <div class="row">
                    <div class="col-12 col-sm-6 col-md-6">
                      <div class="info-box mb-3 bg-warning">
                        <span class="info-box-icon">
                          <i class="fas fa-empty-set"></i>
                        </span>
                        <div class="info-box-content">
                          <span class="info-box-text"><?= $usr['nombre1'] ?> <?= $usr['apellido1'] ?></span>
                          <span class="info-box-number">Actualmente no tiene licencias registradas</span>
                        </div>
                        <!-- /.info-box-content -->
                      </div>
                    </div>
                  </div>
                  <?php endif ?>
                </div>
                <!-- /. Licencia -->
                <!-- Modal -->
                <div class="modal fade" id="newLicense" tabindex="-1" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Nueva Licencia</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <form action="<?= URL ?>" method="post" accept-charset="utf-8">
                          <div class="card-body">
                            <div class="form-group">
                              <label for="dui">DUI</label>
                              <input type="text" name="dui" class="form-control" id="dui" placeholder="00000000-0" required autofocus>
                            </div>
                            <div class="form-group">
                              <label for="nit">NIT</label>
                              <input type="text" name="nit" class="form-control" id="nit" placeholder="0000-000000-000-0" required>
                              <small id="mnsj" class="form-text text-muted"></small>
                            </div>
                            <div class="form-group">
                              <label for="date1">Fecha de expedición de tu licencia</label>
                              <input type="date" name="fechaExp" class="form-control" id="date1" required>
                            </div>
                            <div class="form-group">
                              <label for="date2">Fecha de vencimiento de tu licencia</label>
                              <input type="date" name="fechaVenc" class="form-control" id="date2" required>
                            </div>
                            <div class="form-group">
                              <label for="tipo">Tipo de licencia</label>
                              <select name="type" class="form-control">
                                <option value="Licencia Particular">Licencia Particular</option>
                                <option value="Licencia Liviana">Licencia Liviana</option>
                                <option value="Licencia Pesada">Licencia Pesada</option>
                                <option value="Licencia Pesada-T">Licencia Pesada-T</option>
                                <option value="Licencia de motociclista">Licencia de motociclista</option>
                              </select>
                            </div>
                            <div class="row">
                              <div class="col-12">
                                <button type="button" class="btn btn-default float-left" data-dismiss="modal">Cerrar</button>
                                <button type="submit" class="btn btn-primary float-right" name="newLicense">Ingresar Licencia</button>
                              </div>
                            </div>
                          </div>
                        </form>
                      </div>
                      <div class="modal-footer bg-primary">
                      </div>
                    </div>
                  </div>
                </div>
                <!-- /. Modal -->
                <div class="tab-pane" id="bitacoras">
                  <div class="row">
                    <div class="col-12">
                      <?php $bitacora = $model->bitacorasUsuarios($_SESSION['val']); ?>
                      <table class="table table-bordered table-striped datable">
                        <thead>
                          <tr>
                            <th>Fecha Salida</th>
                            <th>Hora Salida</th>
                            <th>Km Salida</th>
                            <th>Fecha Entrada</th>
                            <th>Hora Entrada</th>
                            <th>Km Entrada</th>
                            <th>Destino</th>
                            <th>Motivo</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php if ($bitacora): ?>
                        <?php foreach ($bitacora['idBitacora'] as $key => $value): ?>
                          <tr>
                            <td><?= date_format(date_create($bitacora['fechaSalida'][$key]), 'd-M-Y') ?></td>
                            <td><?= $bitacora['HoraSalida'][$key] ?></td>
                            <td><?= $bitacora['kmInicial'][$key] ?></td>
                            <td><?= date_format(date_create($bitacora['fechaEntrada'][$key]), 'd-M-Y') ?></td>
                            <td><?= $bitacora['HoraEntrada'][$key] ?></td>
                            <td><?= $bitacora['kmFinal'][$key] ?></td>
                            <td><?= $bitacora['destino'][$key] ?></td>
                            <td><?= $bitacora['motivo'][$key] ?></td>
                          </tr>
                        <?php endforeach ?>
                        <?php endif ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <!-- /.tab-pane -->
              </div>
              <!-- /.tab-content -->
            </div><!-- /.card-body -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"><i class="fas fa-key"></i> Actualizar contraseña</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?= URL ?>" method="post" accept-charset="utf-8">
          <div class="card-body">
            <div class="form-group">
              <label for="currentPass">Contraseña actual</label>
              <input type="password" name="currentPass" class="form-control" id="currentPass" placeholder="Password" required autofocus>
            </div>
            <div class="form-group">
              <label for="pass1">Nueva contraseña</label>
              <input type="password" name="pass" class="form-control" id="pass1" placeholder="Password" onkeyup="validapass1()" required>
              <small id="mnsj" class="form-text text-muted"></small>
            </div>
            <div class="form-group">
              <label for="pass2">Repita su contraseña</label>
              <input type="password" name="password" class="form-control" id="pass2" placeholder="Password" onkeyup="validapass2()" required>
              <small id="mnsj2" class="form-text text-muted"></small>
            </div>
            <div class="row">
              <div class="col-12">
                <button type="button" class="btn btn-default float-left" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-primary float-right" id="reset_password" name="update_password">Restablecer contraseña</button>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer bg-primary justify-content-between">
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="picProfile">
  <div class="modal-dialog modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"><i class="fas fa-users"></i> Seleccionar imagen</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table class="table table-borderless">
          <tbody>
            <?php
              $r = round(count($fotos['idFoto']) / 4);
              $y = 3;
              $x = 0;
            ?>
            <?php for ($i = 0; $i <= $r; $i++): ?>
              <tr>
              <?php for ($j = $x; $j <= $y; $j++): ?>
                <?php if ($j != count($fotos['idFoto'])): ?>  
                <td>
                  <a href="<?= URL ?>?updatePicProfile=<?= $fotos['idFoto'][$j] ?>">
                    <img src="data:image/png;base64,<?= $fotos['foto'][$j] ?>" class="w-75 imgProfile">
                  </a>
                </td>
                <?php else: ?>
                <?php break; ?>
                <?php endif ?>
              <?php endfor ?>
              </tr>
              <?php 
                $x = $j;
                $y += ($i == $r) ? 3 : 4;
              ?>
            <?php endfor ?>
          </tbody>
        </table>
      </div>
      <div class="modal-footer bg-primary justify-content-between">
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- REQUIRED SCRIPTS -->

<script src="dist/js/validaciones.js"></script>

<!-- SweetAlert2 -->
<script src="plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="dist/js/datatable.js"></script>

<?php require_once APP . "/views/master/footer_js.php"; ?>

<?= $objController->sweetAlert(2000); ?>

<?php require_once APP . "/views/master/footer_end.php"; ?>