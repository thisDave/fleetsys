<?php require_once APP."/views/master/header.php"; ?>

<!-- DataTables -->
<link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

<?php require_once APP."/views/master/admin-nav.php"; ?>

<?php
  $idPais = $model->getIdPais($user['pais']);
  $autos = ($_SESSION['val'] == 'motos') ? $model->listarVehiculos($idPais, "motos"):$model->listarVehiculos($idPais, "autos");
  $valor = ($_SESSION['val'] == 'motos') ? 'Motocicletas':'Vehículos';
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1><?= $valor ?></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= URL ?>?request=home">Inicio</a></li>
              <li class="breadcrumb-item active"><?= $valor ?></li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <!-- /.card-header -->
              <div class="card-body">
                <a href="<?= URL ?>?req=autos&val=Vehículos" class="btn btn-sm btn-blue">
                  <i class="fad fa-truck-pickup"></i> Vehículos
                </a>
                <a href="<?= URL ?>?req=autos&val=motos" class="btn btn-sm btn-info">
                  <i class="fad fa-motorcycle"></i> Motocicletas
                </a>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Lista de <?= $valor ?></h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered table-striped datable">
                  <thead>
                  <tr>
                    <th>Placa</th>
                    <th>Marca</th>
                    <th>Modelo</th>
                    <th>Color</th>
                    <th>Región</th>
                    <th>Acción</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php if ($autos): ?>
                  <?php foreach ($autos['idVehiculo'] as $key => $value): ?>
                    <tr>
                      <td><?= $autos['placa'][$key] ?></td>
                      <td><?= $autos['marca'][$key] ?></td>
                      <td><?= $autos['modelo'][$key] ?></td>
                      <td><?= $autos['color'][$key] ?></td>
                      <td><?= $autos['region'][$key] ?></td>
                      <td>
                        <a href="<?= URL ?>?req=autoProfile&val=<?= $autos['idVehiculo'][$key] ?>" class="btn btn-sm btn-primary">
                          Ver Perfil
                        </a>
                      </td>
                    </tr>
                  <?php endforeach ?>
                  <?php endif ?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<!-- REQUIRED SCRIPTS -->

<?php require_once APP."/views/master/footer_js.php"; ?>

<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="dist/js/datatable.js"></script>

<?php require_once APP."/views/master/footer_end.php"; ?>