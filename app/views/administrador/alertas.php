<?php require_once APP."/views/master/header.php"; ?>

<!-- Select2 -->
<link rel="stylesheet" href="plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<!-- SweetAlert2 -->
<link rel="stylesheet" href="plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">

<?php require_once APP."/views/master/admin-nav.php"; ?>

<?php $rango ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Alertas</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?= URL ?>?request=home">Inicio</a></li>
						<li class="breadcrumb-item">Mantenimientos</li>
						<li class="breadcrumb-item active">Alertas</li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="card card-primary">
						<div class="card-header">
							<h3 class="card-title">Alertas de mantenimientos</h3>
						</div>
						<!-- /.card-header -->
						<?php $rango = $modelAdmin->obtenerAlerta(); ?>
						<form action="<?= URL ?>" method="post">
							<div class="card-body">
								<div class="form-row">
									<div class="col-12 col-sm-6 col-md-2">
										<div class="form-group">
											<label for="rango">Realizar mtto cada:</label>
											<div class="input-group mb-3">
												<input type="number" class="form-control" id="rango" name="rango" value="<?= $rango['valor'] ?>">
												<div class="input-group-append">
													<span class="input-group-text">Kms</span>
												</div>
							                </div>
										</div>
									</div>
									<div class="col-12 col-sm-6 col-md-2">
										<div class="form-group">
											<label for="alerta">Activar alerta faltando:</label>
											<div class="input-group mb-3">
												<input type="number" class="form-control" id="alerta" name="alerta" value="<?= $rango['alerta'] ?>">
												<div class="input-group-append">
													<span class="input-group-text">Kms</span>
												</div>
							                </div>
										</div>
									</div>
								</div>
							</div>
							<!-- /.card-body -->
							<div class="card-footer">
								<button type="submit" class="btn btn-primary" name="sendAlert">Actualizar alerta</button>
							</div>
						</form>
					</div>
					<!-- /.card -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container-fluid -->
	</section>
</div>
<!-- /.content-wrapper -->

<!-- REQUIRED SCRIPTS -->

<?php require_once APP."/views/master/footer_js.php"; ?>
<!-- Select2 -->
<script src="plugins/select2/js/select2.full.min.js"></script>
<!-- SweetAlert2 -->
<script src="plugins/sweetalert2/sweetalert2.min.js"></script>

<?= $objController->sweetAlert(2000); ?>

<?php require_once APP."/views/master/footer_end.php"; ?>