<?php require_once APP."/views/master/header.php"; ?>

<!-- SweetAlert2 -->
<link rel="stylesheet" href="plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">

<?php require_once APP."/views/master/admin-nav.php"; ?>

<?php $providers = $model->listarProveedores($model->getIdPais($user['pais'])); ?>
<?php $regiones = $model->listarRegiones($model->getIdPais($user['pais'])); ?>
<?php $marcas = $model->listarMarcas(); ?>
<?php $tipos = $model->listarTipoAutos(); ?>
<?php $items = $model->autoItemsList(); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Nuevo Vehículo</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= URL ?>?request=home">Inicio</a></li>
                        <li class="breadcrumb-item active">Nuevo Vehículo</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-12">
                    <!-- general form elements -->
                    <!-- form start -->
                    <form action="<?= URL ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Información del Vehículo</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <div class="form-row">
                                    <div class="col-12 col-sm-6 col-md-1">
                                        <div class="form-group">
                                            <label for="placa">No. Placa</label>
                                            <input type="text" class="form-control" id="placa" name="placa" placeholder="P000000" required autofocus>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 col-md-1">
                                        <div class="form-group">
                                            <label for="anno">Año</label>
                                            <input type="number" class="form-control" id="anno" name="anno" placeholder="Año" required>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="proveedor">Proveedor</label>
                                            <select class="form-control" id="proveedor" name="proveedor" required>
                                                <?php foreach ($providers['idProveedor'] as $key => $value): ?>
                                                <?php if ($providers['tipo'][$key] == 'Venta de Vehiculos'): ?>
                                                <option value="<?= $providers['idProveedor'][$key] ?>">
                                                    <?= $providers['nombre'][$key] ?>
                                                </option>
                                                <?php endif ?>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 col-md-2">
                                        <div class="form-group">
                                            <label for="marca">Marca</label>
                                            <select class="form-control" id="marca" name="marca" required>
                                                <?php foreach ($marcas['idMarca'] as $key => $value): ?>
                                                <option value="<?= $marcas['idMarca'][$key] ?>"><?= $marcas['marca'][$key] ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 col-md-2">
                                        <div class="form-group">
                                            <label for="modelo">Modelo</label>
                                            <input type="text" class="form-control" id="modelo" name="modelo" placeholder="Marca del vehículo" required>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 col-md-2">
                                        <div class="form-group">
                                            <label for="color">Color</label>
                                            <input type="text" class="form-control" id="color" name="color" placeholder="Color del vehículo" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-12 col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="motor">Motor</label>
                                            <input type="text" class="form-control" id="motor" name="motor" placeholder="Registro del motor" required>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="chasis">Chasis</label>
                                            <input type="text" class="form-control" id="chasis" name="chasis" placeholder="Registro del chasis" required>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="vin">VIN</label>
                                            <input type="text" class="form-control" id="vin" name="vin" placeholder="Registro de VIN" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-12 col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="region">Region</label>
                                            <select class="form-control" id="region" name="region" required>
                                                <?php foreach ($regiones['id'] as $key => $value): ?>
                                                <option value="<?= $regiones['id'][$key] ?>"><?= $regiones['region'][$key] ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 col-md-2">
                                        <div class="form-group">
                                            <label for="km">
                                                Kilometraje actual
                                                <span class="text-secondary" data-toggle="popover" title="Kilometraje actual" data-content="Escriba en esta casilla el kilometraje que actualmente posee el vehículo.">
                                                    <i class="fal fa-info-circle"></i>
                                                </span>
                                            </label>
                                            <div class="input-group mb-3">
                                                <input type="number" class="form-control" id="km" name="km" value="0" min="1" required>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">Kms</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 col-md-2">
                                        <div class="form-group">
                                            <label for="mtto">
                                                Próximo Mantenimiento
                                                <span class="text-secondary" data-toggle="popover" title="Próximo mantenimiento" data-content="Digita aquí el próximo kilometraje que tendrá el vehículo para realizar su mantenimiento preventivo.">
                                                    <i class="fal fa-info-circle"></i>
                                                </span>
                                            </label>
                                            <div class="input-group mb-3">
                                                <input type="number" class="form-control" id="mtto" name="mtto" value="0" min="1" required>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">Kms</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 col-md-2">
                                        <div class="form-group">
                                            <label for="tipo">Tipo de vehículo</label>
                                            <select class="form-control" id="tipo" name="tipo" required>
                                                <?php foreach ($tipos['id'] as $key => $value): ?>
                                                <option value="<?= $tipos['id'][$key] ?>"><?= $tipos['tipo'][$key] ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Hoja de salida</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <div class="form-row">
                                    <div class="col-12 col-sm-6">
                                        <p class="display-7"><strong>Selecciona los items que poseerá el vehículo</strong></p>
                                    </div>
                                </div>
                                <?php
                                    $r = round(count($items['id']) / 4);
                                    $y = 3;
                                    $x = 0;
                                ?>
                                <?php for ($i = 0; $i <= $r; $i++): ?>
                                <div class="form-row">
                                <?php for ($j = $x; $j <= $y; $j++): ?>
                                <?php if ($j != count($items['id'])): ?>
                                <div class="col-12 col-sm-6 col-md-2">
                                    <div class="form-group table-responsive">
                                        <div class="custom-control custom-switch custom-switch-on-success">
                                            <input type="checkbox" class="custom-control-input" id="<?= $items['id'][$j] ?>" name="<?= $items['id'][$j] ?>">
                                            <label class="custom-control-label" for="<?= $items['id'][$j] ?>">
                                                <?= $items['item'][$j] ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <?php else: ?>
                                    <?php break; ?>
                                <?php endif ?>
                                <?php endfor ?>
                                </div>
                                <?php
                                    $x = $j;
                                    $y += ($i == $r) ? 3 : 4;
                                ?>
                                <?php endfor ?>
                            </div>
                        </div>
                        <!-- /.card -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Detalles del vehículo</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <div class="form-row">
                                    <div class="col-12 col-sm-6 col-md-12">
                                        <p class="display-7">
                                            <strong>Adjunta las imagenes relacionadas al vehículo</strong>
                                        </p>
                                        <div class="form-row">
                                            <div class="col-12 col-sm-6 col-md-2">
                                                <div class="form-group">
                                                    <label for="customFile">Costado derecho</label>
                                                    <div class="custom-file">
                                                        <input type="file" accept="image/*" class="custom-file-input" id="der" name="derecha" required>
                                                        <label class="custom-file-label" for="der">Choose file</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6 col-md-2">
                                                <div class="form-group">
                                                    <label for="customFile">Costado izquierdo</label>
                                                    <div class="custom-file">
                                                        <input type="file" accept="image/*" class="custom-file-input" id="izq" name="izquierda" required>
                                                        <label class="custom-file-label" for="izq">Choose file</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6 col-md-2">
                                                <div class="form-group">
                                                    <label for="customFile">Bomper frontal</label>
                                                    <div class="custom-file">
                                                        <input type="file" accept="image/*" class="custom-file-input" id="front" name="adelante" required>
                                                        <label class="custom-file-label" for="front">Choose file</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6 col-md-2">
                                                <div class="form-group">
                                                    <label for="customFile">Bomper trasero</label>
                                                    <div class="custom-file">
                                                        <input type="file" accept="image/*" class="custom-file-input" id="back" name="atras" required>
                                                        <label class="custom-file-label" for="back">Choose file</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" name="newAuto" class="btn btn-primary">Registrar vehículo</button>
                            </div>
                        </div>
                    </form>
                </div>
                <!--/.col (left) -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- REQUIRED SCRIPTS -->

<?php require_once APP."/views/master/footer_js.php"; ?>

<!-- SweetAlert2 -->
<script src="plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- bs-custom-file-input -->
<script src="plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>

<script>
    $(document).ready(function() {
        $(function () {
            $('[data-toggle="popover"]').popover({
                container: 'body'
            });
        });

        $(function () {
            bsCustomFileInput.init();
        });
    });
</script>

<?= $objController->sweetAlert(2000); ?>

<?php require_once APP."/views/master/footer_end.php"; ?>