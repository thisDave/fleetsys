<?php require_once APP."/views/master/header.php"; ?>

<!-- DataTables -->
<link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<!-- SweetAlert2 -->
<link rel="stylesheet" href="plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">

<?php require_once APP."/views/master/admin-nav.php"; ?>

<?php $perfil = $model->infoAuto($_SESSION['val']); ?>
<?php $providers = $model->listarProveedores($model->getIdPais($user['pais'])); ?>
<?php $regiones = $model->listarRegiones($model->getIdPais($user['pais'])); ?>
<?php $marcas = $model->listarMarcas(); ?>
<?php $tipos = $model->listarTipoAutos(); ?>
<?php $bitacora = $model->bitacorasVehiculo($_SESSION['val']); ?>
<?php $golpes = $model->ultimoDetalleGolpes($_SESSION['val']); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Perfil general</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= URL ?>?request=home">Inicio</a></li>
                        <?php $auto = ($perfil['tipo'] == 'Motocicleta') ? 'motos':'cars'; ?>
                        <?php $vista = ($perfil['tipo'] == 'Motocicleta') ? 'Motocicletas':'Vehículos'; ?>
                        <li class="breadcrumb-item"><a href="<?= URL ?>?req=autos&val=<?= $auto ?>"><?= $vista ?></a></li>
                        <li class="breadcrumb-item active">Perfil</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card card-solid">
            <div class="card-header bg-primary">
                <h3 class="card-title">Datos Generales</h3>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12 col-sm-6 col-md-2">
                        <div class="info-box mb-3">
                            <span class="info-box-icon bg-primary elevation-1">
                                <i class="fas fa-barcode-alt"></i>
                            </span>

                            <div class="info-box-content">
                                <span class="info-box-text">Placa</span>
                                <span class="info-box-number"><?= $perfil['placa'] ?></span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <div class="col-12 col-sm-6 col-md-2">
                        <div class="info-box mb-3">
                            <span class="info-box-icon bg-info elevation-1">
                                <i class="fas fa-bookmark"></i>
                            </span>

                            <div class="info-box-content">
                                <span class="info-box-text">Modelo</span>
                                <span class="info-box-number"><?= $perfil['modelo'] ?></span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->

                    <div class="clearfix hidden-md-up"></div>

                    <div class="col-12 col-sm-6 col-md-2">
                        <div class="info-box mb-3">
                            <span class="info-box-icon bg-success elevation-1">
                                <i class="fas fa-calendar-alt"></i>
                            </span>

                            <div class="info-box-content">
                                <span class="info-box-text">Año</span>
                                <span class="info-box-number"><?= $perfil['anno'] ?></span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                    <div class="col-12 col-sm-6 col-md-2">
                        <div class="info-box mb-3">
                            <span class="info-box-icon bg-warning elevation-1">
                                <i class="fas fa-tag"></i>
                            </span>

                            <div class="info-box-content">
                                <span class="info-box-text">Proveedor</span>
                                <span class="info-box-number"><?= $perfil['proveedor'] ?></span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                    <div class="clearfix hidden-md-up"></div>

                    <div class="col-12 col-sm-6 col-md-2">
                        <div class="info-box mb-3">
                            <span class="info-box-icon bg-secondary elevation-1">
                                <i class="fab fa-adn"></i>
                            </span>

                            <div class="info-box-content">
                                <span class="info-box-text">Marca</span>
                                <span class="info-box-number"><?= $perfil['marca'] ?></span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                    <div class="col-12 col-sm-6 col-md-2">
                        <div class="info-box mb-3">
                            <span class="info-box-icon bg-blue elevation-1">
                                <i class="fas fa-palette"></i>
                            </span>

                            <div class="info-box-content">
                                <span class="info-box-text">Color</span>
                                <span class="info-box-number"><?= $perfil['color'] ?></span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                </div>
                <div class="row">
                    <div class="col-12 col-sm-6 col-md-2">
                        <div class="info-box mb-3">
                            <span class="info-box-icon bg-blue elevation-1">
                                <i class="far fa-car-building"></i>
                            </span>

                            <div class="info-box-content">
                                <span class="info-box-text">Oficina</span>
                                <span class="info-box-number"><?= $perfil['region'] ?></span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                    <div class="col-12 col-sm-6 col-md-2">
                        <div class="info-box mb-3">
                            <span class="info-box-icon bg-success elevation-1">
                                <i class="fas fa-cogs"></i>
                            </span>

                            <div class="info-box-content">
                                <span class="info-box-text">Motor</span>
                                <span class="info-box-number"><?= $perfil['motor'] ?></span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                    <div class="clearfix hidden-md-up"></div>

                    <div class="col-12 col-sm-6 col-md-2">
                        <div class="info-box mb-3">
                            <span class="info-box-icon bg-info elevation-1">
                                <i class="fas fa-car"></i>
                            </span>

                            <div class="info-box-content">
                                <span class="info-box-text">Chasis</span>
                                <span class="info-box-number"><?= $perfil['chasis'] ?></span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                    <div class="col-12 col-sm-6 col-md-2">
                        <div class="info-box mb-3">
                            <span class="info-box-icon bg-secondary elevation-1">
                                <i class="fas fa-info"></i>
                            </span>

                            <div class="info-box-content">
                                <span class="info-box-text">Vin</span>
                                <span class="info-box-number"><?= $perfil['vin'] ?></span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                    <div class="clearfix hidden-md-up"></div>
                    <div class="col-12 col-sm-6 col-md-2">
                        <div class="info-box mb-3">
                            <span class="info-box-icon bg-warning elevation-1">
                                <i class="fas fa-truck-pickup"></i>
                            </span>

                            <div class="info-box-content">
                                <span class="info-box-text">Tipo</span>
                                <span class="info-box-number"><?= $perfil['tipo'] ?></span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                    <div class="col-12 col-sm-6 col-md-2">
                        <div class="info-box mb-3">
                            <span class="info-box-icon bg-danger elevation-1">
                                <i class="fas fa-tools"></i>
                            </span>

                            <div class="info-box-content">
                                <span class="info-box-text">Prox. Mtto</span>
                                <span class="info-box-number"><?= $perfil['mtto'] ?> Kms</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                </div>
                <div class="row">
                    <div class="col-12">
                        <button type="button" class="btn btn-link text-blue" data-toggle="modal" data-target="#edit-profile">
                            <i class="fad fa-edit"></i>
                            Editar <?= $perfil['tipo'] ?>
                        </button>
                        |
                        <button type="button" class="btn btn-link text-info" data-toggle="modal" data-target="#edit-pics">
                            <i class="fad fa-camera-retro"></i>
                            Editar fotografías
                        </button>
                        |
                        <button type="button" class="btn btn-link text-danger" data-toggle="modal" data-target="#del-profile">
                            <i class="fad fa-trash-alt"></i>
                            Eliminar <?= $perfil['tipo'] ?>
                        </button>
                        |
                        <a href="<?= URL ?>?req=autos&val=cars" class="btn btn-link text-secondary">
                            <i class="fas fa-backward"></i>
                            Volver
                        </a>
                        <a href="#" class="btn btn-sm btn-primary float-right" data-toggle="modal" data-target="#fotosGolpes">Fotos del vehículo</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="card card-solid">
            <div class="card-header bg-primary">
                <h3 class="card-title">Información adicional</h3>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card card-tabs">
                          <div class="card-header p-0 pt-1">
                            <ul class="nav nav-tabs" id="custom-tabs-two-tab" role="tablist">
                              <li class="nav-item">
                                <a class="nav-link active" id="custom-tabs-two-home-tab" data-toggle="pill" href="#bitacoras" role="tab" aria-controls="custom-tabs-two-home" aria-selected="true">Bitácoras</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link" id="custom-tabs-two-profile-tab" data-toggle="pill" href="#vales" role="tab" aria-controls="custom-tabs-two-profile" aria-selected="false">Vales de combustible</a>
                              </li>
                              <!--li class="nav-item">
                                <a class="nav-link" id="custom-tabs-two-messages-tab" data-toggle="pill" href="#incidencias" role="tab" aria-controls="custom-tabs-two-messages" aria-selected="false">Incidencias</a>
                              </li-->
                            </ul>
                          </div>
                          <div class="card-body">
                            <div class="tab-content" id="custom-tabs-two-tabContent">
                                <div class="tab-pane fade show active" id="bitacoras" role="tabpanel" aria-labelledby="custom-tabs-two-home-tab">
                                <table class="table table-bordered table-striped datable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Piloto</th>
                                            <th>Fecha Salida</th>
                                            <th>Hora Salida</th>
                                            <th>Km Salida</th>
                                            <th>Fecha Entrada</th>
                                            <th>Hora Entrada</th>
                                            <th>Km Entrada</th>
                                            <th>Destino</th>
                                            <th>Motivo</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if ($bitacora): ?>
                                            <?php foreach ($bitacora['idBitacora'] as $key => $value): ?>
                                                <tr>
                                                    <td><?= $key+1 ?></td>
                                                    <td><?= $bitacora['piloto'][$key] ?></td>
                                                    <td><?= date_format(date_create($bitacora['fechaSalida'][$key]), 'd-M-Y') ?></td>
                                                    <td><?= $bitacora['horaSalida'][$key] ?></td>
                                                    <td><?= $bitacora['kmInicial'][$key] ?></td>
                                                    <td><?= date_format(date_create($bitacora['fechaEntrada'][$key]), 'd-M-Y') ?></td>
                                                    <td><?= $bitacora['horaEntrada'][$key] ?></td>
                                                    <td><?= $bitacora['kmFinal'][$key] ?></td>
                                                    <td><?= $bitacora['destino'][$key] ?></td>
                                                    <td><?= $bitacora['motivo'][$key] ?></td>
                                                </tr>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </tbody>
                                </table>
                                <!-- Modal -->
                                <div class="modal fade" id="proximamente" tabindex="-1" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <h1 class="display-4 text-primary">
                                                    Próximamente <i class="fad fa-digging"></i>
                                                </h1>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                              </div>
                              <div class="tab-pane fade" id="vales" role="tabpanel" aria-labelledby="custom-tabs-two-profile-tab">
                                <table class="table table-bordered table-striped datable">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>tipo Ingreso</th>
                                            <th>Correlativo</th>
                                            <th>Valor</th>
                                            <th>Fecha voucher</th>
                                            <th>Fecha de ingreso</th>
                                            <th>Placa</th>
                                            <th>Piloto</th>
                                            <th>km Salida</th>
                                            <th>km Entrada</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $vales = $model->listaVales(); ?>
                                        <?php if ($vales): ?>
                                            <?php foreach ($vales['idVehiculo'] as $key => $value): ?>
                                                <?php if ($value == $_SESSION['val']): ?>
                                                <tr>
                                                    <td><?= $key+1 ?></td>
                                                    <td><?= $vales['tipoIngreso'][$key] ?></td>
                                                    <td><?= $vales['nIngreso'][$key] ?></td>
                                                    <td><?= $vales['valor'][$key] ?></td>
                                                    <td><?= date_format(date_create($vales['fechaIngreso'][$key]), 'd-M-Y') ?></td>
                                                    <td><?= date_format(date_create($vales['fecha'][$key]), 'd-M-Y') ?></td>
                                                    <td><?= $vales['numeroPlaca'][$key] ?></td>
                                                    <td><?= $vales['piloto'][$key] ?></td>
                                                    <td><?= $vales['kmSalida'][$key] ?></td>
                                                    <td><?= $vales['kmEntrada'][$key] ?></td>
                                                </tr>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </tbody>
                                </table>
                              </div>
                              <div class="tab-pane fade" id="incidencias" role="tabpanel" aria-labelledby="custom-tabs-two-messages-tab">
                                <table class="table table-bordered table-striped datable">
                                    <thead>
                                        <tr>
                                            <th>Fecha</th>
                                            <th>Código Preorden</th>
                                            <th>Descripción</th>
                                            <th>Proveedor de Mantenimiento</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                01/01/2020
                                            </td>
                                            <td>PREOR-002</td>
                                            <td>Reparación de bujías y suspensión</td>
                                            <td>Interno (Mecánico)</td>
                                            <td>
                                                <a href="<?= URL ?>?req=incidenceProfile&val=" class="btn btn-sm btn-blue">Ver detalles</a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                          <!-- /.card -->
                        </div>
                      </div>
                    </div>
                <!-- /.row -->
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->

</div>
<!-- /.content-wrapper -->

<div class="modal fade" id="edit-profile">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Editar <?= $perfil['tipo'] ?></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <form action="<?= URL ?>" method="post" accept-charset="utf-8">
                    <div class="card-body">
                        <div class="form-row">
                            <div class="col-12 col-sm-6 col-md-2">
                                <div class="form-group">
                                    <label for="placa">No. Placa</label>
                                    <input type="text" class="form-control" id="placa" name="placa" value="<?= $perfil['placa'] ?>" required autofocus>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-2">
                                <div class="form-group">
                                    <label for="anno">Año</label>
                                    <input type="number" class="form-control" id="anno" name="anno" value="<?= $perfil['anno'] ?>" required>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-3">
                                <div class="form-group">
                                    <label for="proveedor">Proveedor</label>
                                    <select class="form-control" id="proveedor" name="proveedor" required>
                                        <?php foreach ($providers['idProveedor'] as $key => $value): ?>
                                        <?php $selected = ($perfil['proveedor'] == $providers['nombre'][$key]) ? 'selected' : ''; ?>
                                        <?php if ($providers['tipo'][$key] == 'Venta de Vehiculos'): ?>
                                        <option value="<?= $providers['idProveedor'][$key] ?>" <?= $selected ?>>
                                            <?= $providers['nombre'][$key] ?>
                                        </option>
                                        <?php endif ?>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-2">
                                <div class="form-group">
                                    <label for="marca">Marca</label>
                                    <select class="form-control" id="marca" name="marca" required>
                                        <?php foreach ($marcas['idMarca'] as $key => $value): ?>
                                        <?php $selected = ($perfil['marca'] == $marcas['marca'][$key]) ? 'selected' : ''; ?>
                                        <option value="<?= $marcas['idMarca'][$key] ?>" <?= $selected ?>>
                                            <?= $marcas['marca'][$key] ?>
                                        </option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-2">
                                <div class="form-group">
                                    <label for="modelo">Modelo</label>
                                    <input type="text" class="form-control" id="modelo" name="modelo" value="<?= $perfil['modelo'] ?>" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-12 col-sm-6 col-md-2">
                                <div class="form-group">
                                    <label for="color">Color</label>
                                    <input type="text" class="form-control" id="color" name="color" value="<?= $perfil['color'] ?>" required>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-3">
                                <div class="form-group">
                                    <label for="motor">Motor</label>
                                    <input type="text" class="form-control" id="motor" name="motor" value="<?= $perfil['motor'] ?>" required>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-3">
                                <div class="form-group">
                                    <label for="chasis">Chasis</label>
                                    <input type="text" class="form-control" id="chasis" name="chasis" value="<?= $perfil['chasis'] ?>" required>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-3">
                                <div class="form-group">
                                    <label for="vin">VIN</label>
                                    <input type="text" class="form-control" id="vin" name="vin" value="<?= $perfil['vin'] ?>" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-12 col-sm-6 col-md-3">
                                <div class="form-group">
                                    <label for="region">Region</label>
                                    <select class="form-control" id="region" name="region" required>
                                        <?php foreach ($regiones['id'] as $key => $value): ?>
                                        <?php $selected = ($perfil['region'] == $regiones['region'][$key]) ? 'selected' : ''; ?>
                                        <option value="<?= $regiones['id'][$key] ?>" <?= $selected ?>>
                                            <?= $regiones['region'][$key] ?>
                                        </option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>
                            <?php if ($model->validaBitacora($_SESSION['val'])): ?>
                                <div class="col-12 col-sm-6 col-md-2">
                                    <div class="form-group">
                                        <label for="kmetraje">Kilometraje</label>
                                        <input type="number" class="form-control" id="kmetraje" name="kmetraje" min="1" value="<?= $perfil['kilometraje'] ?>" required>
                                    </div>
                                </div>
                            <?php endif ?>
                            <div class="col-12 col-sm-6 col-md-2">
                                <div class="form-group">
                                    <label for="km">Prox. Mtto.</label>
                                    <input type="number" class="form-control" id="km" name="km" min="0" value="<?= $perfil['mtto'] ?>" required>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-2">
                                <div class="form-group">
                                    <label for="tipo">Tipo de vehículo</label>
                                    <select class="form-control" id="tipo" name="tipo" required>
                                        <?php foreach ($tipos['id'] as $key => $value): ?>
                                        <?php $selected = ($perfil['tipo'] == $tipos['tipo'][$key]) ? 'selected' : ''; ?>
                                        <option value="<?= $tipos['id'][$key] ?>" <?= $selected ?>>
                                            <?= $tipos['tipo'][$key] ?>
                                        </option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="button" class="btn btn-default float-left" data-dismiss="modal">Cerrar</button>
                        <input type="hidden" name="id" value="<?= $perfil['idVehiculo'] ?>">
                        <button type="submit" name="updateAuto" class="btn btn-primary float-right">Actualizar vehículo</button>
                    </div>
                </form>
            </div>
            <div class="modal-footer justify-content-between bg-primary">
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="edit-pics">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Editar <?= $perfil['tipo'] ?></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <form action="<?= URL ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                    <div class="form-row">
                        <div class="col-12 col-sm-6 col-md-2">
                            <div class="form-group">
                                <label for="customFile">Costado derecho</label>
                                <div class="custom-file">
                                    <input type="file" accept="image/*" class="custom-file-input" id="der" name="derecha" required>
                                    <label class="custom-file-label" for="der">Choose file</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-2">
                            <div class="form-group">
                                <label for="customFile">Costado izquierdo</label>
                                <div class="custom-file">
                                    <input type="file" accept="image/*" class="custom-file-input" id="izq" name="izquierda" required>
                                    <label class="custom-file-label" for="izq">Choose file</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-2">
                            <div class="form-group">
                                <label for="customFile">Bomper frontal</label>
                                <div class="custom-file">
                                    <input type="file" accept="image/*" class="custom-file-input" id="front" name="adelante" required>
                                    <label class="custom-file-label" for="front">Choose file</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-2">
                            <div class="form-group">
                                <label for="customFile">Bomper trasero</label>
                                <div class="custom-file">
                                    <input type="file" accept="image/*" class="custom-file-input" id="back" name="atras" required>
                                    <label class="custom-file-label" for="back">Choose file</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="button" class="btn btn-default float-left" data-dismiss="modal">Cerrar</button>
                        <button type="submit" name="updatePics" value="<?= $golpes['idHoja'][0] ?>" class="btn btn-primary float-right">
                            Actualizar fotografías
                        </button>
                    </div>
                </form>
            </div>
            <div class="modal-footer justify-content-between bg-primary">
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="del-profile">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Eliminar Vehículo</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= URL ?>" method="post" accept-charset="utf-8">
                    <p>
                        ¿Está seguro que desea Eliminar este vehículo?
                    </p>
                    <p>
                        Se perderán permanentemente todos los datos relacionados a este perfil.
                    </p>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button type="button" class="btn btn-default float-left" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-danger float-right" name="delAutoProfile" value="<?= $perfil['idVehiculo'] ?>">Eliminar Vehículo</button>
                    </div>
                </form>
            </div>
            <div class="modal-footer bg-primary justify-content-between">
            </div>
        </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Modal golpes -->
<div class="modal fade" id="fotosGolpes" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Detalles del vehículo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-6">
                        <div class="card w-100">
                            <img class="w-100 card-img-top" src="<?= $golpes['foto'][0] ?>">
                            <div class="card-body">
                                <h5 class="card-title">Lado derecho</h5>
                                <p class="card-text">
                                    Golpes: <strong><?= $golpes['tipo'][0] ?></strong>
                                </p>
                            </div>
                            <div class="card-footer bg-primary"></div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6">
                        <div class="card w-100">
                            <img class="w-100 card-img-top" src="<?= $golpes['foto'][1] ?>">
                            <div class="card-body">
                                <h5 class="card-title">Lado izquierdo</h5>
                                <p class="card-text">
                                    Golpes: <strong><?= $golpes['tipo'][1] ?></strong>
                                </p>
                            </div>
                            <div class="card-footer bg-primary"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-6">
                        <div class="card w-100">
                            <img class="w-100 card-img-top" src="<?= $golpes['foto'][2] ?>">
                            <div class="card-body">
                                <h5 class="card-title">Lado frontal</h5>
                                <p class="card-text">
                                    Golpes: <strong><?= $golpes['tipo'][2] ?></strong>
                                </p>
                            </div>
                            <div class="card-footer bg-primary"></div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6">
                        <div class="card w-100">
                            <img class="w-100 card-img-top" src="<?= $golpes['foto'][3] ?>">
                            <div class="card-body">
                                <h5 class="card-title">Lado trasero</h5>
                                <p class="card-text">
                                    Golpes: <strong><?= $golpes['tipo'][3] ?></strong>
                                </p>
                            </div>
                            <div class="card-footer bg-primary"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- fin golpes -->

<!-- REQUIRED SCRIPTS -->

<?php require_once APP."/views/master/footer_js.php"; ?>
<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="dist/js/datatable.js"></script>
<!-- SweetAlert2 -->
<script src="plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- bs-custom-file-input -->
<script src="plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>

<script>
    $(document).ready(function() {
        $(function () {
            bsCustomFileInput.init();
        });
    });
</script>

<?= $objController->sweetAlert(2000); ?>

<?php require_once APP."/views/master/footer_end.php"; ?>