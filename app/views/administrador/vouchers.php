<?php require_once APP."/views/master/header.php"; ?>

<!-- DataTables -->
<link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

<?php require_once APP."/views/master/admin-nav.php"; ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Vales de combustible</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?= URL ?>?request=home">Inicio</a></li>
						<li class="breadcrumb-item">Combustible</li>
						<li class="breadcrumb-item active">Lista de vales</li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header">
							<h3 class="card-title">Vales de combustibles</h3>
						</div>
						<!-- /.card-header -->
						<div class="card-body">
							<table class="table table-bordered table-striped datable">
								<thead>
									<tr>
										<th>No.</th>
										<th>tipo Ingreso</th>
										<th>Correlativo</th>
										<th>Valor</th>
										<th>Fecha voucher</th>
										<th>Fecha de ingreso</th>
										<th>Placa</th>
										<th>Piloto</th>
										<th>km Salida</th>
										<th>km Entrada</th>
									</tr>
								</thead>
								<tbody>
									<?php $vales = $model->listaVales(); ?>
									<?php if ($vales): ?>
										<?php foreach ($vales['nIngreso'] as $key => $value): ?>
											<tr>
												<td><?= $key+1 ?></td>
												<td><?= $vales['tipoIngreso'][$key] ?></td>
												<td><?= $vales['nIngreso'][$key] ?></td>
												<td><?= $vales['valor'][$key] ?></td>
												<td><?= date_format(date_create($vales['fechaIngreso'][$key]), 'd-M-Y') ?></td>
												<td><?= date_format(date_create($vales['fecha'][$key]), 'd-M-Y') ?></td>
												<td><?= $vales['numeroPlaca'][$key] ?></td>
												<td><?= $vales['piloto'][$key] ?></td>
												<td><?= $vales['kmSalida'][$key] ?></td>
												<td><?= $vales['kmEntrada'][$key] ?></td>
											</tr>
										<?php endforeach ?>
									<?php endif ?>
								</tbody>
							</table>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container-fluid -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- REQUIRED SCRIPTS -->

<?php require_once APP."/views/master/footer_js.php"; ?>

<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="dist/js/datatable.js"></script>

<?php require_once APP."/views/master/footer_end.php"; ?>