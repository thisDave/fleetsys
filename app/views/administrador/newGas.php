<?php require_once APP."/views/master/header.php"; ?>

<!-- SweetAlert2 -->
<link rel="stylesheet" href="plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">

<!-- Select2 -->
<link rel="stylesheet" href="plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

<?php require_once APP."/views/master/admin-nav.php"; ?>

<?php $autos = $model->listarVehiculos($model->getIdPais($user['pais']), "all") ?>
<?php $providers = $model->listarProveedores($model->getIdPais($user['pais'])); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Ingresos de combustible</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= URL ?>?request=home">Inicio</a></li>
                        <li class="breadcrumb-item">Combustible</li>
                        <li class="breadcrumb-item active">Nuevo</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <?php if (isset($_SESSION['startGas'])): ?>

        <?php if (!empty($_SESSION['startGas']['proveedor'])): ?>

            <?php if ($_SESSION['startGas']['tipo'] == 'vouchers'): ?>

                <!-- Main content -->
                <section class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <!-- left column -->
                            <div class="col-12 col-sm-12 col-md-6">
                                <!-- general form elements -->
                                <div class="card card-primary">
                                    <div class="card-header">
                                        <h3 class="card-title">Nuevo ingreso</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <!-- form start -->
                                    <form action="<?= URL ?>" method="post" accept-charset="utf-8">
                                        <div class="card-body">
                                            <div class="form-row">
                                                <div class="col-12 col-sm-6 col-md-3">
                                                    <div class="form-group">
                                                        <label for="fecha">Fecha</label>
                                                        <input type="date" class="form-control" id="fecha" name="fecha" required>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-4">
                                                    <div class="form-group">
                                                        <label>Proveedor</label>
                                                        <select class="form-control" disabled>
                                                            <?php foreach ($providers['idProveedor'] as $key => $value): ?>
                                                                <?php if ($value == $_SESSION['startGas']['proveedor']): ?>
                                                                    <option value="<?= $providers['idProveedor'][$key] ?>" selected>
                                                                        <?= $providers['nombre'][$key] ?>
                                                                    </option>
                                                                <?php endif ?>
                                                            <?php endforeach ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="col-12 col-sm-6 col-md-3">
                                                    <div class="form-group">
                                                        <label for="nIngreso">N° de Vale</label>
                                                        <input type="text" class="form-control" id="nIngreso" name="nIngreso" required>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-4">
                                                    <div class="form-group">
                                                        <label for="auto">Vehículo</label>
                                                        <select class="form-control select2" id="auto" name="auto" style="width: 100%;" required>
                                                            <?php foreach ($autos['idVehiculo'] as $key => $value): ?> 
                                                            <option value="<?= $autos['idVehiculo'][$key] ?>">
                                                                <?= $autos['placa'][$key] ?> / <?= $autos['marca'][$key] ?> / <?= $autos['color'][$key] ?>
                                                            </option>
                                                            <?php endforeach ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-3">
                                                    <div class="form-group">
                                                        <label for="valor">Valor del vale</label>
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">$</span>
                                                            </div>
                                                            <input type="text" id="valor" name="valor" class="form-control" required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="col-12 col-sm-6 col-md-3">
                                                    <div class="form-group">
                                                        <label for="kmSalida">Km Salida</label>
                                                        <input type="text" class="form-control" id="kmSalida" name="kmSalida" required>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-3">
                                                    <div class="form-group">
                                                        <label for="kmEntrada">Km Entrada</label>
                                                        <input type="text" class="form-control" id="kmEntrada" name="kmEntrada" required>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-5">
                                                    <div class="form-group">
                                                        <label for="responsable">Piloto</label>
                                                        <input type="text" class="form-control" id="responsable" name="responsable" disabled>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.card-body -->
                                        <div class="card-footer">
                                            <button type="submit" name="newVoucher" class="btn btn-success float-right">Registrar vale</button>
                                            <a href="<?= URL ?>?endGas" class="btn btn-black"><i class="fas fa-ban"></i> Cancelar ingreso</a>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.card -->
                            </div>
                            <!--/.col (left) -->
                        </div>
                        <!-- /.row -->
                    </div><!-- /.container-fluid -->
                </section>
                <!-- /.content -->

            <?php else: ?>

                <!-- Main content -->
                <section class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <!-- left column -->
                            <div class="col-12 col-sm-12 col-md-6">
                                <!-- general form elements -->
                                <div class="card card-primary">
                                    <div class="card-header">
                                        <h3 class="card-title">Nuevo ingreso</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <!-- form start -->
                                    <form action="<?= URL ?>" method="post" accept-charset="utf-8">
                                        <div class="card-body">
                                            <div class="form-row">
                                                <div class="col-12 col-sm-6 col-md-3">
                                                    <div class="form-group">
                                                        <label for="fecha">Fecha</label>
                                                        <input type="date" class="form-control" id="fecha" name="fecha" required>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-4">
                                                    <div class="form-group">
                                                        <label>Proveedor</label>
                                                        <select class="form-control" disabled>
                                                            <?php foreach ($providers['idProveedor'] as $key => $value): ?>
                                                                <?php if ($value == $_SESSION['startGas']['proveedor']): ?>
                                                                    <option value="<?= $providers['idProveedor'][$key] ?>" selected>
                                                                        <?= $providers['nombre'][$key] ?>
                                                                    </option>
                                                                <?php endif ?>
                                                            <?php endforeach ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="col-12 col-sm-6 col-md-3">
                                                    <div class="form-group">
                                                        <label for="nIngreso">N° de factura</label>
                                                        <input type="text" class="form-control" id="nIngreso" name="nIngreso" required>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-4">
                                                    <div class="form-group">
                                                        <label for="auto">Vehículo</label>
                                                        <select class="form-control select2" id="auto" name="auto" style="width: 100%;" required>
                                                            <?php foreach ($autos['idVehiculo'] as $key => $value): ?> 
                                                            <option value="<?= $autos['idVehiculo'][$key] ?>">
                                                                <?= $autos['placa'][$key] ?> / <?= $autos['marca'][$key] ?> / <?= $autos['color'][$key] ?>
                                                            </option>
                                                            <?php endforeach ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-3">
                                                    <div class="form-group">
                                                        <label for="valor">Valor de la factura</label>
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">$</span>
                                                            </div>
                                                            <input type="text" id="valor" name="valor" class="form-control" required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="col-12 col-sm-6 col-md-3">
                                                    <div class="form-group">
                                                        <label for="kmSalida">Km Salida</label>
                                                        <input type="text" class="form-control" id="kmSalida" name="kmSalida" required>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-3">
                                                    <div class="form-group">
                                                        <label for="kmEntrada">Km Entrada</label>
                                                        <input type="text" class="form-control" id="kmEntrada" name="kmEntrada" required>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-5">
                                                    <div class="form-group">
                                                        <label for="responsable">Piloto</label>
                                                        <input type="text" class="form-control" id="responsable" name="responsable" disabled>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.card-body -->
                                        <div class="card-footer">
                                            <button type="submit" name="newVoucher" class="btn btn-primary">Registrar vale</button>
                                            <a href="<?= URL ?>?endGas" class="btn btn-black float-right"><i class="far fa-home"></i> Inicio</a>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.card -->
                            </div>
                            <!--/.col (left) -->
                        </div>
                        <!-- /.row -->
                    </div><!-- /.container-fluid -->
                </section>
                <!-- /.content -->

            <?php endif ?>

        <?php else: ?>

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-12 col-sm-12 col-md-6">
                            <!-- general form elements -->
                            <div class="card card-primary">
                                <div class="card-header">
                                    <h3 class="card-title">Nuevo ingreso</h3>
                                </div>
                                <!-- /.card-header -->
                                <form action="<?= URL ?>" method="post" accept-charset="utf-8">
                                    <div class="card-body">
                                        <div class="form-row">
                                            <div class="col-12 col-sm-6 col-md-5">
                                                <div class="form-group">
                                                    <label for="provider">Selecciona un proveedor</label>
                                                    <select id="provider" name="provider" class="form-control">
                                                        <?php foreach ($providers['idProveedor'] as $key => $value): ?>
                                                            <?php if ($providers['tipo'][$key] == 'Venta de Servicios'): ?>
                                                                <option value="<?= $providers['idProveedor'][$key] ?>">
                                                                    <?= $providers['nombre'][$key] ?>
                                                                </option>
                                                            <?php endif ?>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.card-body -->
                                    <div class="card-footer">
                                        <button type="submit" name="gasProvider" class="btn btn-primary">Seleccionar proveedor</button>
                                        <a href="<?= URL ?>?endGas" class="btn btn-black float-right"><i class="far fa-home"></i> Inicio</a>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card -->
                        </div>
                        <!--/.col (left) -->
                    </div>
                    <!-- /.row -->
                </div><!-- /.container-fluid -->
            </section>
            <!-- /.content -->
            
        <?php endif ?>

    <?php else: ?>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-12 col-sm-12 col-md-6">
                        <!-- general form elements -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Nuevo ingreso</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <div class="form-row">
                                    <div class="col-12 col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <a href="<?= URL ?>?startGas=vouchers" class="btn btn-blue">
                                                Vales de combustibles
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 col-md-4">
                                        <div class="form-group">
                                            <a href="<?= URL ?>?startGas=invoices" class="btn btn-warning">
                                                Facturas de caja chica
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                    <!--/.col (left) -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
        
    <?php endif ?>

</div>
<!-- /.content-wrapper -->

<!-- REQUIRED SCRIPTS -->

<?php require_once APP."/views/master/footer_js.php"; ?>

<!-- SweetAlert2 -->
<script src="plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Select2 -->
<script src="plugins/select2/js/select2.full.min.js"></script>

<?= $objController->sweetAlert(2000) ?>

<script>
    $(document).ready(function () {
        $('.select2').select2({
            theme: 'bootstrap4'
        });
        
        $("#kmEntrada").keyup(function() {
            var auto = $("#auto").val();
            var kmSalida = $("#kmSalida").val();
            var kmEntrada = $("#kmEntrada").val();
            var ruta = "buscarPiloto=&auto="+auto+"&kmOutput="+kmSalida+"&kmInput="+kmEntrada;
            $.ajax({
                type: 'POST',
                url: 'tools/data/search.php',
                data: ruta
            })
            .done(function(resultado){
                $("#responsable").val(resultado);
            })
        });
    });
</script>

<?php require_once APP."/views/master/footer_end.php"; ?>