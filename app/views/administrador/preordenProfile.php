<?php require_once APP."/views/master/header.php"; ?>

<!-- DataTables -->
<link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<!-- Select2 -->
<link rel="stylesheet" href="plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<!-- SweetAlert2 -->
<link rel="stylesheet" href="plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">

<?php require_once APP."/views/master/admin-nav.php"; ?>

<?php $perfil = $modelAdmin->infoPreorden($_SESSION['val']); ?>
<?php $detalle = $modelAdmin->detallePreorden($perfil['tipoPreorden'], $_SESSION['val']); ?>
<?php $providers = $model->listarProveedores($model->getIdPais($user['pais'])); ?>
<?php $products = $model->listarProductCatalog(); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Perfil de la preorden</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?= URL ?>?request=home">Inicio</a></li>
						<li class="breadcrumb-item"><a href="<?= URL ?>?request=preordenes">Preordenes</a></li>
						<li class="breadcrumb-item active">Perfil</li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="card card-primary">
						<div class="card-header">
							<h3 class="card-title">Preorden <?= $perfil['tipoPreorden'] ?></h3>
						</div>
						<!-- /.card-header -->
						<div class="card-body">
							<!-- form start -->
							<div class="card-body">
								<div class="form-row">
									<div class="col-12 col-sm-6 col-md-2">
										<div class="form-group">
										<label>Fecha emisión</label>
											<input type="text" class="form-control" value="<?= $objController->date_time('date'); ?>" disabled>
										</div>
									</div>
									<div class="col-12 col-sm-6 col-md-2">
										<div class="form-group">
											<div class="form-group">
												<label>Placa del vehículo</label>
												<input type="text" class="form-control" value="<?= $perfil['numeroPlaca'] ?>" disabled>
											</div>
										</div>
									</div>
									<div class="col-12 col-sm-6 col-md-2">
										<div class="form-group">
										<label>Kilometraje actual</label>
										<input type="text" class="form-control" value="<?= $perfil['kilometraje'] ?>" disabled>
										</div>
									</div>
								</div>
							</div>
							<div class="card-body">
								<table class="table table-bordered table-striped datable">
									<thead>
										<tr>
											<th>No</th>
											<th>Mantenimiento</th>
										</tr>
									</thead>
									<tbody>
										<?php if (isset($detalle)): ?>
											<?php if ($detalle): ?>
												<?php foreach ($detalle['accion'] as $key => $value): ?>
													<tr>
														<td><?= $key+1 ?></td>
														<td><?= $value ?></td>
													</tr>
												<?php endforeach ?>
											<?php endif ?>
										<?php endif ?>
									</tbody>
								</table>
							</div>
							<?php if ($perfil['estado'] == 'Finalizado'): ?>
							<?php $bill = $modelAdmin->billDetail($_SESSION['val']); ?>
							<section class="content">
						        <div class="container-fluid">
						            <div class="row">
						                <!-- left column -->
						                <div class="col-12">
						                    <!-- general form elements -->
						                    <div class="card card-primary card-outline">
						                        <div class="card-header">
						                            <h3 class="card-title">Datos de factura</h3>
						                        </div>
						                        <!-- /.card-header -->
						                        <!-- form start -->
						                        <div class="card-body">
						                            <div class="form-row">
						                                <div class="col-12 col-sm-6 col-md-2">
						                                    <div class="form-group">
						                                        <label>No. Factura</label>
						                                        <div class="input-group">
						                                            <div class="input-group-prepend">
						                                                <span class="input-group-text">N°</span>
						                                            </div>
						                                            <input type="text" class="form-control" value="<?= $bill['numFactura'][0] ?>" disabled>
						                                        </div>
						                                    </div>
						                                </div>
						                                <div class="col-12 col-sm-6 col-md-2">
						                                    <div class="form-group">
						                                        <label>Total factura</label>
						                                        <?php $total = 0; ?>
						                                        <?php foreach ($bill['total'] as $value){ $total += $value; } ?>
						                                        <div class="input-group">
						                                            <div class="input-group-prepend">
						                                                <span class="input-group-text">$</span>
						                                            </div>
						                                            <input type="text" class="form-control" value="<?= $total ?>" disabled>
						                                        </div>
						                                    </div>
						                                </div>
						                                <div class="col-12 col-sm-6 col-md-3">
						                                    <div class="form-group">
						                                        <label>Proveedor</label>
						                                        <input type="text" class="form-control" value="<?= $bill['proveedor'][0] ?>" disabled>
						                                    </div>
						                                </div>
						                            </div>
						                            <div class="form-row">
						                                <div class="col-12">
						                                	<table class="table table-bordered table-striped">
								                                <thead>
								                                    <tr>
								                                        <th>No</th>
								                                        <th>Cantidad</th>
								                                        <th>Producto</th>
								                                        <th>Precio Unitario</th>
								                                        <th>Total</th>
								                                    </tr>
								                                </thead>
								                                <tbody>
								                                    <?php $x = 1; ?>
								                                    <?php foreach ($bill['idMtto'] as $key => $value): ?>
								                                    <tr>
								                                        <td><?= $x ?></td>
								                                        <td><?= $bill['cantidad'][$key] ?></td>
								                                        <td><?= $bill['producto'][$key] ?></td>
								                                        <td>$<?= $bill['punitario'][$key] ?></td>
								                                        <td>$<?= $bill['total'][$key] ?></td>
								                                    </tr>
								                                    <?php $x++; ?>
								                                    <?php endforeach ?>
								                                </tbody>
								                            </table>
						                                </div>
						                            </div>
						                            <div class="form-row mt-3">
						                            	<div class="col-12">
						                            		<a href="<?= URL ?>/?request=preordenes" class="btn btn-black">Volver</a>
						                            	</div>
						                            </div>
						                        </div>
						                    </div>
						                    <!-- /.card -->
						                </div>
						                <!--/.col (left) -->
						            </div>
						            <!-- /.row -->
						        </div><!-- /.container-fluid -->
						    </section>
							<?php else: ?>
							<?php if (isset($_SESSION['preordenAbierta']) && $_SESSION['preordenAbierta'] == $_SESSION['val']): ?>
							<?php if (isset($_SESSION['nfact'])): ?>
							<!-- Main content -->
							<?php foreach ($_SESSION['nfact'] as $key => $value): ?>
							<?php if ($value != '0000'): ?>
							<section class="content">
						        <div class="container-fluid">
						            <div class="row">
						                <!-- left column -->
						                <div class="col-12">
						                    <!-- general form elements -->
						                    <div class="card card-info collapsed-card">
						                        <div class="card-header">
						                            <h3 class="card-title">Factura N°<?= $_SESSION['nfact'][$key] ?></h3>
						                            <div class="card-tools">
														<button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i></button>
													</div>
						                        </div>
						                        <!-- /.card-header -->
						                        <!-- form start -->
						                        <div class="card-body">
						                            <div class="form-row">
						                                <div class="col-12 col-sm-6 col-md-2">
						                                    <div class="form-group">
						                                        <label>No. Factura</label>
						                                        <div class="input-group">
						                                            <div class="input-group-prepend">
						                                                <span class="input-group-text">N°</span>
						                                            </div>
						                                            <input type="text" class="form-control" value="<?= $_SESSION['nfact'][$key] ?>" disabled>
						                                        </div>
						                                    </div>
						                                </div>
						                                <div class="col-12 col-sm-6 col-md-2">
						                                    <div class="form-group">
						                                        <label>Total factura</label>
						                                        <div class="input-group">
						                                            <div class="input-group-prepend">
						                                                <span class="input-group-text">$</span>
						                                            </div>
						                                            <input type="text" class="form-control" value="<?= $_SESSION['tfact'][$key] ?>" disabled>
						                                        </div>
						                                    </div>
						                                </div>
						                                <div class="col-12 col-sm-6 col-md-3">
						                                    <div class="form-group">
						                                        <label>Proveedor</label>
						                                        <select class="form-control" disabled>
						                                            <?php foreach ($providers['idProveedor'] as $i => $val): ?>
						                                            <?php if ($providers['idProveedor'][$i] == $_SESSION['idprov'][$key]): ?>
						                                            <option value="<?= $providers['idProveedor'][$i] ?>" selected>
						                                                <?= $providers['nombre'][$i] ?>
						                                            </option>
						                                            <?php endif ?>
						                                            <?php endforeach ?>
						                                        </select>
						                                    </div>
						                                </div>
						                            </div>
						                            <div class="form-row">
						                                <div class="col-12">
						                                	<table class="table table-bordered table-striped datable">
								                                <thead>
								                                    <tr>
								                                        <th>No</th>
								                                        <th>Cantidad</th>
								                                        <th>Producto</th>
								                                        <th>Precio Unitario</th>
								                                        <th>Total</th>
								                                    </tr>
								                                </thead>
								                                <tbody>
								                                    <?php $x = 1; ?>
								                                    <?php foreach ($_SESSION['prods'][$key]['cantidad'] as $j => $valor): ?>
								                                    <tr>
								                                        <td><?= $x ?></td>
								                                        <td><?= $valor ?></td>
								                                        <?php $nombre = $model->nombreProducto($_SESSION['prods'][$key]['producto'][$j]); ?>
								                                        <td><?= $nombre ?></td>
								                                        <td>$<?= $_SESSION['prods'][$key]['punitario'][$j] ?></td>
								                                        <td>$<?= $_SESSION['prods'][$key]['totalProd'][$j] ?></td>
								                                    </tr>
								                                    <?php $x++; ?>
								                                    <?php endforeach ?>
								                                </tbody>
								                            </table>
						                                </div>
						                            </div>
						                            <div class="form-row mt-3">
						                            	<div class="col-12">
						                            		<a href="<?= URL ?>?event=editBill&val=<?= $key ?>" class="btn btn-warning">
						                            			<strong>Editar Factura</strong>
						                            		</a>
						                            		<a href="<?= URL ?>?event=delBill&val=<?= $key ?>" class="btn btn-danger float-right">
						                            			<strong>Eliminar Factura</strong>
						                            		</a>
						                            	</div>
						                            </div>
						                        </div>
						                    </div>
						                    <!-- /.card -->
						                </div>
						                <!--/.col (left) -->
						            </div>
						            <!-- /.row -->
						        </div><!-- /.container-fluid -->
						    </section>
							<?php endif ?>
							<?php endforeach ?>
							<?php $iprod = (in_array('0000', $_SESSION['nfact'])) ? array_search('0000', $_SESSION['nfact']) : false; ?>
							<?php if (!is_bool($iprod)): ?>
							<section class="content">
						        <div class="container-fluid">
						            <div class="row">
						                <!-- left column -->
						                <div class="col-12">
						                    <!-- general form elements -->
						                    <div class="card card-dark collapsed-card">
						                        <div class="card-header">
						                            <h3 class="card-title">Productos varios</h3>
						                            <div class="card-tools">
														<button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i></button>
													</div>
						                        </div>
						                        <!-- /.card-header -->
						                        <!-- form start -->
						                        <div class="card-body">
						                            <div class="form-row">
						                                <div class="col-12">
						                                	<table class="table table-bordered table-striped datable">
								                                <thead>
								                                    <tr>
								                                        <th>No</th>
								                                        <th>Cantidad</th>
								                                        <th>Producto</th>
								                                        <th>Precio Unitario</th>
								                                        <th>Total</th>
								                                        <th>Acción</th>
								                                    </tr>
								                                </thead>
								                                <tbody>
								                                    <?php $x = 1; ?>
								                                    <?php foreach ($_SESSION['prods'][$iprod]['cantidad'] as $j => $valor): ?>
								                                    <tr>
								                                        <td><?= $x ?></td>
								                                        <td><?= $valor ?></td>
								                                        <?php $nombre = $model->nombreProducto($_SESSION['prods'][$iprod]['producto'][$j]); ?>
								                                        <td><?= $nombre ?></td>
								                                        <td>$<?= $_SESSION['prods'][$iprod]['punitario'][$j] ?></td>
								                                        <td>$<?= $_SESSION['prods'][$iprod]['totalProd'][$j] ?></td>
								                                        <td>
								                                        	<a href="<?= URL ?>?event=delProdEntry&val=<?= $iprod ?>_<?= $j ?>" class="btn btn-sm btn-danger">Eliminar</a>
								                                        </td>
								                                    </tr>
								                                    <?php $x++; ?>
								                                    <?php endforeach ?>
								                                </tbody>
								                            </table>
						                                </div>
						                            </div>
						                        </div>
						                    </div>
						                    <!-- /.card -->
						                </div>
						                <!--/.col (left) -->
						            </div>
						            <!-- /.row -->
						        </div><!-- /.container-fluid -->
						    </section>
							<?php endif ?>
							<div class="card-footer">
								<a href="<?= URL ?>?request=ingresos" class="btn btn-sm btn-warning">
									Agregar factura
								</a>
								<a href="<?= URL ?>?request=prodentry" class="btn btn-sm btn-black float-right">
									Agregar producto
								</a>
							</div>
						    <!-- /.content -->
							<div class="card-body">
								<label for="comments">Comentarios generales</label>
								<?php $comentario = isset($_SESSION['comentPreorden']) ? $_SESSION['comentPreorden'] : ''; ?>
								<textarea name="comments" id="comments" rows="2" class="form-control"><?= $comentario ?></textarea>
							</div>
							<div class="card-footer">
								<a href="<?= URL ?>?endPreorder" class="btn btn-success">
									<i class="fas fa-check"></i> Finalizar preorden
								</a>
								<a href="<?= URL ?>?cancelarPreorden" class="btn btn-danger float-right">
									<i class="fas fa-backward"></i> Cancelar y volver
								</a>
							</div>
							<?php else: ?>
							<div class="card-footer">
								<a href="<?= URL ?>?request=ingresos" class="btn btn-warning">
									<strong>Agregar factura</strong>
								</a>
								<a href="<?= URL ?>?request=prodentry" class="btn btn-black">
									Agregar productos
								</a>
								<a href="<?= URL ?>/?cancelarPreorden" class="btn btn-danger float-right">
									<i class="fas fa-backward"></i> Cancelar y volver
								</a>
							</div>
							<?php endif ?>
							<?php else: ?>
							<?php if ($perfil['estado'] != 'Finalizado'): ?>
							<div class="card-footer">
								<a href="<?= URL ?>?openPreorder=<?= $_SESSION['val'] ?>" class="btn btn-success">Iniciar preorden</a>
								<a href="<?= URL ?>/?request=preordenes" class="btn btn-black">Volver</a>
							</div>
							<?php else: ?>
							<div class="card-footer">
								<a href="<?= URL ?>/?request=preordenes" class="btn btn-black">Volver</a>
							</div>
							<?php endif ?>
							<?php endif ?>
							<?php endif ?>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container-fluid -->
	</section>
</div>
<!-- /.content-wrapper -->

<!-- REQUIRED SCRIPTS -->

<?php require_once APP."/views/master/footer_js.php"; ?>

<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="dist/js/datatable.js"></script>
<!-- Select2 -->
<script src="plugins/select2/js/select2.full.min.js"></script>
<!-- SweetAlert2 -->
<script src="plugins/sweetalert2/sweetalert2.min.js"></script>

<?= $objController->sweetAlert(2000); ?>

<script>
$(document).ready(function () {
	$('.select2').select2({
		theme: 'bootstrap4'
	});

	$("#comments").keyup(function() {
		let comment = $("#comments").val();
		let ruta = "commentPreorden="+comment;
		$.ajax({
			type: 'POST',
			url: 'tools/data/search.php',
			data: ruta
		});
	});

});
</script>

<?php require_once APP."/views/master/footer_end.php"; ?>