<?php require_once APP."/views/master/header.php"; ?>

<!-- DataTables -->
<link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

<?php require_once APP."/views/master/admin-nav.php"; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Preordenes</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= URL ?>?request=home">Inicio</a></li>
              <li class="breadcrumb-item">Mantenimientos</li>
              <li class="breadcrumb-item active">Pre ordenes</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Preordenes activas</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered table-striped datable">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Placa</th>
                      <th>Modelo</th>
                      <th>Color</th>
                      <th>Tipo Preorden</th>
                      <th>Fecha Inicial</th>
                      <th>Fecha Final</th>
                      <th>Usuario</th>
                      <th>Estado</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php if ($modelAdmin->listaPreordenes()): ?>
                  <?php $datos = $modelAdmin->listaPreordenes(); $n = 1; ?>
                  <?php foreach ($datos['idPreorden'] as $key => $value): ?>
                    <tr>
                      <td><?= $n ?></td>
                      <td><?= $datos['numeroPlaca'][$key] ?></td>
                      <td><?= $datos['modelo'][$key] ?></td>
                      <td><?= $datos['color'][$key] ?></td>
                      <td><?= $datos['tipoPreorden'][$key] ?></td>
                      <td><?= $objController->date_time('format', $datos['fechaEntrada'][$key]) ?></td>
                      <td><?= (is_null($datos['fechaSalida'][$key])) ? '-' : $datos['fechaSalida'][$key] ?></td>
                      <td><?= $datos['usuario'][$key] ?></td>
                      <td>
                        <?php $badge_type = ($datos['estado'][$key] == 'Pendiente') ? 'badge-warning' : 'badge-success'; ?>
                        <span class="badge <?= $badge_type ?>"><?= $datos['estado'][$key] ?></span>
                      </td>
                      <td><a href="<?= URL ?>?req=preordenProfile&val=<?= $value ?>" class="btn btn-sm btn-primary">Abrir Preorden</a></td>
                    </tr>
                    <?php $n++; ?>
                  <?php endforeach ?>
                  <?php endif ?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<!-- REQUIRED SCRIPTS -->

<?php require_once APP."/views/master/footer_js.php"; ?>

<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="dist/js/datatable.js"></script>

<?php require_once APP."/views/master/footer_end.php"; ?>