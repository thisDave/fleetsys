<?php require_once APP."/views/master/header.php"; ?>

<!-- DataTables -->
<link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<!-- SweetAlert2 -->
<link rel="stylesheet" href="plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">

<?php require_once APP."/views/master/admin-nav.php"; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Acciones de mantenimientos</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= URL ?>?request=home">Inicio</a></li>
              <?php if (isset($_SESSION['editAction'])): ?>
              <li class="breadcrumb-item"><a href="<?= URL ?>?event=backActions">Catálogo de acciones</a></li>
              <li class="breadcrumb-item active">Editar acción</a></li>
              <?php else: ?>
              <li class="breadcrumb-item active">Catálogo de acciones</li>
              <?php endif ?>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <?php if (isset($_SESSION['editAction'])): ?>

    <?php $accion = $model->infoAccion($_SESSION['editAction']); ?>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-4">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Actualizar acción</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="<?= URL ?>" method="post">
                <div class="card-body">
                  <div class="form-group">
                    <label for="action">Acción</label>
                    <input type="text" class="form-control" id="action" name="action" value="<?= $accion['accion'] ?>" autofocus>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" name="actionUpdate" value="<?= $accion['idAccion'] ?>" class="btn btn-sm btn-blue">
                    Actualizar
                  </button>
                  <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#delAction">
                    Eliminar
                  </button>
                  <a href="<?= URL ?>?event=backActions" class="btn btn-sm btn-secondary">Volver</a>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (left) -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->

    <div class="modal fade" id="delAction">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Eliminar acción de mantenimiento</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form action="<?= URL ?>" method="post" accept-charset="utf-8">
              <p>
                ¿Desea eliminar esta acción de mantenimiento?
              </p>
              <!-- /.card-body -->
              <div class="card-footer">
                <button type="button" class="btn btn-default float-left" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-danger float-right" name="delActionMant" value="<?= $accion['idAccion'] ?>">
                  Eliminar acción
                </button>
              </div>
            </form>
          </div>
          <div class="modal-footer bg-primary justify-content-between">
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <?php else: ?>

    <?php $actions = $model->listarActionCatalog(); ?>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Lista de Acciones</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered table-striped datable">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Acción de mantenimiento</th>
                    <th>Acciones</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php if ($actions): ?>
                  <?php $x = 1; ?>
                  <?php foreach ($actions['accion'] as $key => $value): ?>
                  <tr>
                    <td><?= $x ?></td>
                    <td><?= $value ?></td>
                    <td>
                      <form action="<?= URL ?>" method="get">
                        <button type="submit" name="editAction" value="<?= $actions['idAccion'][$key] ?>" class="btn btn-sm btn-primary">
                          Editar acción
                        </button>
                      </form>
                    </td>
                  </tr>
                  <?php $x++; ?>
                  <?php endforeach ?>
                  <?php endif ?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <button type="button" class="btn btn-sm btn-blue" data-toggle="modal" data-target="#newAction">
                  Nueva acción
                </button>
              </div>
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->

    <?php endif ?>

  </div>
  <!-- /.content-wrapper -->

  <div class="modal fade" id="newAction">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Nueva acción de mantenimiento</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="<?= URL ?>" method="post" accept-charset="utf-8">
            <div class="form-group">
              <label for="action">Acción</label>
              <input type="text" class="form-control" id="action" name="action" placeholder="Nombre del producto" required>
            </div>
            <div class="card-footer">
              <button type="button" class="btn btn-default float-left" data-dismiss="modal">Cerrar</button>
              <button type="submit" name="newAction" class="btn btn-success float-right">Agregar acción</button>
            </div>
          </form>
        </div>
        <div class="modal-footer bg-primary justify-content-between">
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

<!-- REQUIRED SCRIPTS -->

<?php require_once APP."/views/master/footer_js.php"; ?>

<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="dist/js/datatable.js"></script>
<!-- SweetAlert2 -->
<script src="plugins/sweetalert2/sweetalert2.min.js"></script>

<?= $objController->sweetAlert(2000); ?>

<?php require_once APP."/views/master/footer_end.php"; ?>