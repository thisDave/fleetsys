<?php require_once APP."/views/master/header.php"; ?>

<!-- SweetAlert2 -->
<link rel="stylesheet" href="plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
<!-- Select2 -->
<link rel="stylesheet" href="plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<!-- DataTables -->
<link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

<?php require_once APP."/views/master/admin-nav.php"; ?>

<?php $products = $model->listarProductCatalog(); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Agregar producto</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= URL ?>?request=home">Inicio</a></li>
                        <li class="breadcrumb-item active">Preordenes</li>
                        <li class="breadcrumb-item active">Agregar productos</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-12">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Ingresar producto</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <div class="card-body">
                            <form action="<?= URL ?>" method="post" accept-charset="utf-8">
                                <div class="form-row">
                                    <div class="col-12 col-sm-6 col-md-1">
                                        <div class="form-group">
                                            <label for="cantidad">Cantidad</label>
                                            <input type="number" class="form-control" id="cantidad" min="1" name="cantidad" required autofocus>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="producto">Producto</label>
                                            <select class="form-control select2" id="producto" name="producto" style="width: 100%;" required>
                                                <?php foreach ($products['idProducto'] as $key => $value): ?>
                                                <option value="<?= $products['idProducto'][$key] ?>">
                                                    <?= $products['nombre'][$key] ?>
                                                </option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 col-md-2">
                                        <div class="form-group">
                                            <label for="punitario">P. Unitario</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">$</span>
                                                </div>
                                                <input type="text" class="form-control" id="punitario" name="punitario" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-12 col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <button type="submit" name="addProdEntry" class="btn btn-primary">
                                                Ingresar producto
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
                <!--/.col (left) -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-12">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <!-- /.card-header -->
                        <!-- form start -->
                        <div class="card-body">
                            <table class="table table-bordered table-striped datable">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Cantidad</th>
                                        <th>Producto</th>
                                        <th>Precio Unitario</th>
                                        <th>Total</th>
                                        <?php if (!isset($_SESSION['dataVerificada'])): ?>
                                        <th>Acciones</th>
                                        <?php endif ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if (isset($_SESSION['productos'])): ?>
                                    <?php $x = 1; ?>
                                    <?php foreach ($_SESSION['productos']['cantidad'] as $key => $value): ?>
                                    <tr>
                                        <td><?= $x ?></td>
                                        <td><?= $_SESSION['productos']['cantidad'][$key] ?></td>
                                        <?php $nombre = $model->nombreProducto($_SESSION['productos']['producto'][$key]); ?>
                                        <td><?= $nombre ?></td>
                                        <td>$<?= $_SESSION['productos']['punitario'][$key] ?></td>
                                        <td>$<?= $_SESSION['productos']['totalProd'][$key] ?></td>
                                        <?php if (!isset($_SESSION['dataVerificada'])): ?>
                                        <td>
                                            <a href="<?= URL ?>?event=delProd&val=<?= $_SESSION['productos']['id'][$key] ?>" class="btn btn-sm btn-danger">
                                                Eliminar
                                            </a>
                                        </td>
                                        <?php endif ?>
                                    </tr>
                                    <?php $x++; ?>
                                    <?php endforeach ?>
                                    <?php endif ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
                <!--/.col (left) -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

    <!-- Main content -->
    <section class="content mb-4">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-12">
                    <?php $clase = ''; ?>
                    <?php if (isset($_SESSION['productos']) && count($_SESSION['productos']['id']) > 0): ?>
                    <?php $clase = 'float-right'; ?>
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#saveProdEntry">
                        Guardar productos
                    </button>
                    <?php endif ?>
                    <button type="button" class="btn btn-danger <?= $clase ?>" data-toggle="modal" data-target="#cancel">
                        Cancelar
                    </button>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<div class="modal fade" id="cancel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="card-body text-center">
                    <p class="display-7"><strong> ¿Realmente desea cancelar el ingreso de la factura? </strong></p>
                    <p class="display-7"><strong> Se perderá todo el progreso del registro. </strong></p>
                </div>
                <div class="card-footer">
                        <button type="button" class="btn btn-sm btn-default float-left" data-dismiss="modal">Cerrar</button>
                        <a href="<?= URL ?>?event=cancelprodEntry" class="btn btn-sm btn-danger float-right">Cancelar y volver</a>
                </div>
            </div>
            <div class="modal-footer bg-primary justify-content-between">
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="saveProdEntry">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="card-body text-center">
                    <p class="display-7"><strong> ¿Desea finalizar el ingreso de los productos? </strong></p>
                    <p class="display-7"><strong> Los registros se guardarán permanentemente. </strong></p>
                </div>
                <div class="card-footer">
                    <button type="button" class="btn btn-sm btn-default float-left" data-dismiss="modal">Cerrar</button>
                    <a href="<?= URL ?>?event=saveProdEntry" class="btn btn-sm btn-success float-right">
                        <i class="fas fa-check-double"></i> Guardar y Finalizar
                    </a>
                </div>
            </div>
            <div class="modal-footer bg-primary justify-content-between">
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- REQUIRED SCRIPTS -->

<?php require_once APP."/views/master/footer_js.php"; ?>

<!-- SweetAlert2 -->
<script src="plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Select2 -->
<script src="plugins/select2/js/select2.full.min.js"></script>
<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="dist/js/datatable.js"></script>

<?= $objController->sweetAlert(2000) ?>

<script>
    $(document).ready(function () {
        $('.select2').select2({
            theme: 'bootstrap4'
        })
    });
</script>

<?php require_once APP."/views/master/footer_end.php"; ?>