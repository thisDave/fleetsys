<!DOCTYPE html>
<html lang="es-SV">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" type="image/x-icon" href="img/icono.ico">

  <title>FleetSys  | Reset Pass</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- SweetAlert2 -->
  <link rel="stylesheet" href="plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <img src="img/logo.png" class="h-75 w-75"><br>
    <a href="<?= URL ?>"><b>Fleet</b>Sys</a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Restablecer contraseña</p>
      <?php if (isset($_SESSION['validation']) && $_SESSION['validation'] == true): ?>
      <div class="row">
        <div class="col-12">
          <a href="<?= URL ?>?action=login" class="btn btn-primary w-100">Iniciar sesión</a>
        </div>
      </div>
      <?php else: ?>
      <form action="<?= URL ?>" method="post" accept-charset="utf-8">
        <div class="card-body">
          <div class="form-group">
            <label for="pass1">Ingrese su nueva contraseña</label>
            <input type="password" name="pass" class="form-control" id="pass1" placeholder="Password" onkeyup="validapass1()" required autofocus>
            <small id="mnsj" class="form-text text-muted"></small>
          </div>
          <div class="form-group">
            <label for="pass2">Repita su contraseña</label>
            <input type="password" name="password" class="form-control" id="pass2" placeholder="Password" onkeyup="validapass2()" required>
            <small id="mnsj2" class="form-text text-muted"></small>
          </div>
          <div class="row">
            <div class="col-12">
              <button type="submit" name="reset_password" id="reset_password" class="btn btn-primary">Restablecer contraseña</button>
            </div>
          </div>
        </div>
      </form>
      <?php endif ?>

    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- SweetAlert2 -->
<script src="plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>

<?php if (!isset($_SESSION['validation'])): ?>
<script src="dist/js/validaciones.js"></script>
<?php elseif (isset($_SESSION['validation']) && $_SESSION['validation'] == false): ?>
<script src="dist/js/validaciones.js"></script>
<?php endif ?>

<?php if (isset($_SESSION['validation']) && $_SESSION['validation'] == false): ?>
<!-- AdminLTE App -->
<script>
  var Toast = Swal.mixin({
    toast: false,
    position: 'center',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true
  });

  Toast.fire({
    icon: 'error',
    title: 'Error al restablecer contraseña.',
    text: 'Por favor verifica que tu contraseña solo posea: Mayúsculas, minúsculas y/o números.'
  });
</script>
<?php elseif (isset($_SESSION['validation']) && $_SESSION['validation'] == true): ?>
<!-- AdminLTE App -->
<script>
  var Toast = Swal.mixin({
    toast: false,
    position: 'center',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true
  });

  Toast.fire({
    icon: 'success',
    title: 'Contraseña restablecida.',
    text: 'Tu contraseña ha sido restablecida con éxito.'
  });
</script>
<?php endif ?>

</body>
</html>