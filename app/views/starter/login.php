<?php $info = (isset($_COOKIE['FLEET'])) ? $model->getCookieToken($_COOKIE['FLEET']) : null; ?>
<!DOCTYPE html>
<html lang="es-SV">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" type="image/x-icon" href="<?= URL ?>img/icono.ico">

  <title>FleetSys  | Login</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <img src="img/logo.png" class="h-75 w-75"><br>
    <a href="<?= URL ?>"><b>Fleet</b>Sys</a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Iniciar sesion</p>

      <form action="<?= URL ?>" method="post">
        <div class="input-group mb-3">
          <?php $email = (!is_null($info)) ? $info['usuario'] : ''; ?>
          <input type="email" name="email" class="form-control" placeholder="Email" value="<?= $email ?>" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <?php $password = (!is_null($info)) ? $info['pass'] : ''; ?>
          <input type="password" name="password" class="form-control" placeholder="Password" value="<?= $password ?>" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-6">
            <div class="icheck-primary">
            <?php if (isset($_COOKIE['FLEET'])): ?>
              <p class="mb-1">
                <a href="<?= URL ?>?action=delPass" class="text-secondary">Dejar de recordar</a>
              </p>
            <?php else: ?>
              <input type="checkbox" name="remember" id="remember" value="1">
              <label for="remember">
                Recuerdame
              </label>
            <?php endif ?>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-6">
            <button type="submit" name="login" class="btn btn-primary btn-block">Iniciar sesión</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
      <p class="mb-1">
        <a href="<?= URL ?>?action=forgot">Perdí mi contraseña</a>
      </p>
    </div>
    <!-- /.login-card-body -->
  </div>
  <?php if (isset($_SESSION['error'])): ?>
  <div class="row">
    <div class="col-12">
        <p class="text-danger">
          <?= $_SESSION['error'] ?>
        </p>
    </div>
  </div>
  <?php endif ?>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="<?= URL ?>plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?= URL ?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= URL ?>dist/js/adminlte.min.js"></script>

</body>
</html>