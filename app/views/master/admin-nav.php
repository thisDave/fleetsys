</head>
<body class="hold-transition sidebar-mini layout-navbar-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="<?= URL ?>?request=home" class="nav-link">Inicio</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="<?= URL ?>?request=support" class="nav-link">Soporte</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="<?= URL ?>?request=info" class="nav-link">Info</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="<?= URL ?>?request=logout" class="nav-link text-danger">
          <i class="fas fa-sign-out-alt"></i> Cerrar Sesión
        </a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown" id="bell">
        <?php $total = $modelAdmin->totalNotificaciones(); ?>
        <a class="nav-link" id="bell-link" data-toggle="dropdown" href="#">
          <i class="far fa-bell"></i>
          <span class="badge badge-danger navbar-badge"><?= $total ?></span>
        </a>
        <?php if ($total > 0): ?>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" id="bells">
          <?php $word = ($total <= 1) ? 'Notificación' : 'Notificaciones'; ?>
          <span class="dropdown-header"><?= $total ?> <?= $word ?></span>
          <div class="dropdown-divider"></div>
          <?php $pool = $modelAdmin->getNotifies(); ?>
          <?php if ($pool): ?>
          <?php foreach ($pool['idAlerta'] as $key => $value): ?>
          <a href="<?= URL ?>?req=nuevaPreorden&val=<?= $pool['placa'][$key] ?>(<?= $pool['kmActual'][$key] ?>" class="dropdown-item">
            <i class="fas fa-tools mr-2"></i>
            <?php $info = $model ?>
            <?= $pool['mensaje'][$key] ?>
            <span class="badge bg-warning text-dark"><?= $pool['placa'][$key] ?></span>
          </a>
          <?php endforeach ?>
          <?php endif ?>
          <div class="dropdown-divider"></div>
          <a href="<?= URL ?>?request=notificaciones" class="dropdown-item dropdown-footer">Ver todas las notificaciones</a>
        </div>
        <?php endif ?>
      </li>
      <li class="nav-item">
        <a class="nav-link text-blue" href="<?= URL ?>">
          <i class="fad fa-sync"></i>
        </a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?= URL ?>?request=home" class="brand-link">
      <img src="img/white.png" alt="FleetSys" class="brand-image" style="opacity: .9">
      <span class="brand-text font-weight-light">FleetSys</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="data:imge/png;base64,<?= $user['foto'] ?>" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="<?= URL ?>?request=profile" class="d-block"><?= $user['nombre1'] ?> <?= $user['apellido1'] ?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview">
            <a href="<?= URL ?>?request=home" class="nav-link">
              <i class="nav-icon fas fa-home"></i>
              <p>
                Inicio
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Usuarios
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= URL ?>?request=pilots" class="nav-link">
                  <i class="fas fa-chevron-circle-right nav-icon"></i>
                  <p>Pilotos</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= URL ?>?request=users" class="nav-link">
                  <i class="fas fa-chevron-circle-right nav-icon"></i>
                  <p>Lista</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-car"></i>
              <p>
                Vehículos
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= URL ?>?request=newVehiculo" class="nav-link">
                  <i class="fas fa-chevron-circle-right nav-icon"></i>
                  <p>Nuevo</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= URL ?>?req=autos&val=cars" class="nav-link">
                  <i class="fas fa-chevron-circle-right nav-icon"></i>
                  <p>Lista</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-car-mechanic"></i>
              <p>
                Mantenimientos
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= URL ?>?request=nuevaPreorden" class="nav-link">
                  <i class="fas fa-chevron-circle-right nav-icon"></i>
                  <p>Nueva Preorden</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= URL ?>?preordenes" class="nav-link">
                  <i class="fas fa-chevron-circle-right nav-icon"></i>
                  <p>Lista Preordenes</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= URL ?>?request=productCatalog" class="nav-link">
                  <i class="fas fa-chevron-circle-right nav-icon"></i>
                  <p>Acciones y productos</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= URL ?>?request=categoryCatalog" class="nav-link">
                  <i class="fas fa-chevron-circle-right nav-icon"></i>
                  <p>Categorías</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= URL ?>?request=acciones" class="nav-link">
                  <i class="fas fa-chevron-circle-right nav-icon"></i>
                  <p>Acciones de Mtto.</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= URL ?>?request=alertas" class="nav-link">
                  <i class="fas fa-chevron-circle-right nav-icon"></i>
                  <p>Alertas de Mttos</p>
                </a>
              </li>
              <?php
              $incidencias = '
              <!--li class="nav-item">
                <a href="<?= URL ?>?request=incidencias" class="nav-link">
                  <i class="fas fa-chevron-circle-right nav-icon"></i>
                  <p>Incidencias</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= URL ?>?request=nuevaIncidencia" class="nav-link">
                  <i class="fas fa-chevron-circle-right nav-icon"></i>
                  <p>Nueva Incidencia</p>
                </a>
              </li-->';
              ?>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-gas-pump"></i>
              <p>
                Combustible
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= URL ?>?request=newGas" class="nav-link">
                  <i class="fas fa-chevron-circle-right nav-icon"></i>
                  <p>Nuevo</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= URL ?>?request=vouchers" class="nav-link">
                  <i class="fas fa-chevron-circle-right nav-icon"></i>
                  <p>Lista</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-calendar-alt"></i>
              <p>
                Salidas
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= URL ?>?request=bitacoras" class="nav-link">
                  <i class="fas fa-chevron-circle-right nav-icon"></i>
                  <p>Bitácoras</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= URL ?>?request=newLog" class="nav-link">
                  <i class="fas fa-chevron-circle-right nav-icon"></i>
                  <p>Llenar bitácora</p>
                </a>
              </li>
            </ul>
          </li>
          <?php
          $repuestos = '
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-tools"></i>
              <p>
                Repuestos
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= URL ?>?request=inventario" class="nav-link">
                  <i class="fas fa-chevron-circle-right nav-icon"></i>
                  <p>Inventario</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= URL ?>?request=ingresos" class="nav-link">
                  <i class="fas fa-chevron-circle-right nav-icon"></i>
                  <p>Nuevo ingreso</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= URL ?>?request=productCatalog" class="nav-link">
                  <i class="fas fa-chevron-circle-right nav-icon"></i>
                  <p>Productos</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= URL ?>?request=categoryCatalog" class="nav-link">
                  <i class="fas fa-chevron-circle-right nav-icon"></i>
                  <p>Categorías</p>
                </a>
              </li>
            </ul>
          </li>';
          ?>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-address-book"></i>
              <p>
                Proveedores
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= URL ?>?request=providers" class="nav-link">
                  <i class="fas fa-chevron-circle-right nav-icon"></i>
                  <p>Lista</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= URL ?>?request=newProvider" class="nav-link">
                  <i class="fas fa-chevron-circle-right nav-icon"></i>
                  <p>Nuevo</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fab fa-buffer"></i>
              <p>
                Otros
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= URL ?>?request=profile" class="nav-link">
                  <i class="fas fa-chevron-circle-right nav-icon"></i>
                  <p>Mi Usuario</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= URL ?>?request=notificaciones" class="nav-link">
                  <i class="fas fa-chevron-circle-right nav-icon"></i>
                  <p>Notificaciones</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= URL ?>?request=support" class="nav-link">
                  <i class="fas fa-chevron-circle-right nav-icon"></i>
                  <p>Soporte</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= URL ?>?request=info" class="nav-link">
                  <i class="fas fa-chevron-circle-right nav-icon"></i>
                  <p>Info</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="<?= URL ?>?request=logout" class="nav-link">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p>
                Cerrar sesión
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
