<?php require_once APP . "/views/master/header.php"; ?>

<!-- SweetAlert2 -->
<link rel="stylesheet" href="plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">

<?php if ($_SESSION['log']['level'] == "administrador"): ?>
<?php require_once APP."/views/master/admin-nav.php"; ?>
<?php else: ?>
<?php require_once APP."/views/master/emp-nav.php"; ?>
<?php endif ?>

<?php $disabled = (isset($_SESSION['updateInfoUser'])) ? '' : 'disabled'; ?>

<?php $regiones = $model->listarRegiones($model->getIdPais($user['pais'])); ?>

<?php $fotos = $model->profilePics(); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Perfil de usuario</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?= URL ?>?request=home">Inicio</a></li>
            <li class="breadcrumb-item active">Perfil de usuario</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="card card-primary card-outline">
            <div class="card-body box-profile">
              <div class="text-center">
                <img class="profile-user-img img-fluid img-circle" src="data:image/png;base64,<?= $user['foto'] ?>" alt="User profile picture">
              </div>

              <h3 class="profile-username text-center"><?= $user['nombre1'] ?> <?= $user['apellido1'] ?></h3>

              <p class="text-muted text-center"><?= $user['region'] ?></p>

              <ul class="list-group list-group-unbordered mb-3">
                <li class="list-group-item">
                  <b>País</b> <br>
                  <a class="float-left"><?= $user['pais'] ?></a>
                </li>
                <li class="list-group-item">
                  <b>Cargo</b> <br>
                  <a class="float-left"><?= $user['cargo'] ?></a>
                </li>
                <li class="list-group-item">
                  <b>Permiso de acceso</b> <br>
                  <a class="float-left"><?= $user['nivel'] ?></a>
                </li>
              </ul>
              <?php if (!isset($_SESSION['updateInfoUser'])) : ?>
                <a href="<?= URL ?>?action=updateInfoUser" class="btn btn-primary btn-block">Actualizar información</a>
              <?php else : ?>
                <a href="<?= URL ?>?delaction=updateInfoUser" class="btn btn-danger btn-block">Finalizar edición</a>
              <?php endif ?>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="card">
            <div class="card-header p-2">
              <ul class="nav nav-pills">
                <li class="nav-item"><a class="nav-link active" href="#settings" data-toggle="tab">Información</a></li>
                <li class="nav-item"><a class="nav-link" href="#licencia" data-toggle="tab">Licencia</a></li>
                <li class="nav-item"><a class="nav-link" href="#access" data-toggle="tab">Acceso</a></li>
              </ul>
            </div><!-- /.card-header -->
            <div class="card-body">
              <div class="tab-content">
                <div class="active tab-pane" id="settings">
                  <form class="form-horizontal" action="<?= URL ?>" method="post" accept-charset="utf-8">
                    <div class="form-group row">
                      <label for="nombre1" class="col-sm-2 col-form-label">Primer nombre</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" name="nombre1" id="nombre1" value="<?= $user['nombre1'] ?>" placeholder="Ingresa aquí tu primer nombre" <?= $disabled ?> required autofocus>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="nombre2" class="col-sm-2 col-form-label">Segundo nombre</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" name="nombre2" id="nombre2" value="<?= $user['nombre2'] ?>" placeholder="Ingresa aquí tu segundo nombre/s" <?= $disabled ?> required autofocus>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="apellido1" class="col-sm-2 col-form-label">Primer Apellido</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" name="apellido1" id="apellido1" value="<?= $user['apellido1'] ?>" placeholder="Ingresa tu primer apellido" <?= $disabled ?> required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="apellido2" class="col-sm-2 col-form-label">Segundo Apellido</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" name="apellido2" id="apellido2" value="<?= $user['apellido2'] ?>" placeholder="Ingresa tu segundo apellido/s" <?= $disabled ?> required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="region" class="col-sm-2 col-form-label">Región</label>
                      <div class="col-sm-10" id="region">
                        <select class="form-control" name="region" <?= $disabled ?> required>
                          <?php foreach ($regiones['id'] as $key => $value) : ?>
                            <?php if ($user['region'] == $regiones['region'][$key]): ?>
                            <option value="<?= $regiones['id'][$key] ?>" selected><?= $regiones['region'][$key] ?></option>
                            <?php else: ?>
                            <option value="<?= $regiones['id'][$key] ?>"><?= $regiones['region'][$key] ?></option>
                            <?php endif ?>
                          <?php endforeach ?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="cargo" class="col-sm-2 col-form-label">Cargo</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" name="cargo" id="cargo" value="<?= $user['cargo'] ?>" placeholder="Cargo" <?= $disabled ?> required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="customFile" class="col-sm-2 col-form-label">Imagen de perfil</label>
                      <div class="col-sm-10">
                        <button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#picProfile"> Seleccionar imagen</button>
                        <?php if (isset($_SESSION['updateInfoUser'])) : ?>
                          <button type="submit" name="updateInfoUser" class="btn btn-success float-right">Guardar cambios</button>
                        <?php endif ?>
                      </div>
                    </div>
                  </form>
                </div>
                <!-- /. Settings -->

                <div class="tab-pane" id="licencia">
                  <?php if ($model->verificaLicencia($_SESSION['log']['id'])): ?>
                  <?php $licencia = $model->infoLicencia($_SESSION['log']['id']); ?>
                  <div class="row">
                  <?php foreach ($licencia['idLicencia'] as $key => $value): ?>
                    <div class="col-12 col-sm-12 col-md-4">
                      <div class="card card-widget widget-user">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header bg-primary">
                          <h3 class="widget-user-username"><?= $licencia['tipoLicencia'][$key] ?></h3>
                        </div>
                        <div class="widget-user-image">
                          <img class="img-circle elevation-2" src="data:image/png;base64,<?= $user['foto'] ?>" alt="User Avatar">
                        </div>
                        <div class="card-footer">
                          <div class="row">
                            <div class="col-sm-4 border-right">
                              <div class="description-block">
                                <h5 class="description-header">Expedido el:</h5>
                                <span class="description-text">
                                  <?= date_format(date_create($licencia['fechaExpd'][$key]), 'd/M/Y') ?>
                                </span>
                              </div>
                              <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4 border-right">
                              <div class="description-block">
                                <h5 class="description-header">Vence el:</h5>
                                <span class="description-text">
                                  <?= date_format(date_create($licencia['fechaVenc'][$key]), 'd/M/Y') ?>
                                </span>
                              </div>
                              <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4">
                              <div class="description-block">
                                <h5 class="description-header">Estado:</h5>
                                <?php if ($licencia['aprobacion'][$key] == 1): ?>
                                <span class="description-text badge badge-success">
                                  Autorizado
                                </span>
                                <?php else: ?>
                                <span class="description-text badge badge-danger">
                                  No Autorizado
                                </span>
                                <?php endif ?>
                              </div>
                              <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                          </div>
                          <!-- /.row -->
                          <div class="row">
                            <div class="col-sm-12">
                              <button type="button" class="btn btn-xs btn-outline-black" data-toggle="modal" data-target="#licencia_<?= $value ?>">
                                <i class="fad fa-address-card"></i> Editar licencia
                              </button>
                              <a href="<?= URL ?>?delLicense=<?= $value ?>" class="btn btn-xs btn-outline-danger">
                                <i class="fad fa-trash"></i> Eliminar licencia
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- /.widget-user -->
                    </div>
                    <!-- Modal -->
                    <div class="modal fade" id="licencia_<?= $value ?>" tabindex="-1" aria-hidden="true">
                      <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Editar Licencia</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                            <form action="<?= URL ?>" method="post" accept-charset="utf-8">
                              <div class="card-body">
                                <div class="form-group">
                                  <label for="dui">DUI</label>
                                  <input type="text" name="dui" class="form-control" id="dui" value="<?= $licencia['dui'][$key] ?>" required autofocus>
                                </div>
                                <div class="form-group">
                                  <label for="nit">NIT</label>
                                  <input type="text" name="nit" class="form-control" id="nit" value="<?= $licencia['nit'][$key] ?>" required>
                                  <small id="mnsj" class="form-text text-muted"></small>
                                </div>
                                <div class="form-group">
                                  <label for="date1">Fecha de expedición de tu licencia</label>
                                  <input type="date" name="fechaExp" class="form-control" id="date1" value="<?= $licencia['fechaExpd'][$key] ?>" required>
                                </div>
                                <div class="form-group">
                                  <label for="date2">Fecha de vencimiento de tu licencia</label>
                                  <input type="date" name="fechaVenc" class="form-control" id="date2" value="<?= $licencia['fechaVenc'][$key] ?>" required>
                                </div>
                                <div class="form-group">
                                  <label for="tipo">Tipo de licencia</label>
                                  <select name="type" class="form-control">
                                    <?php $tipos = ["Licencia Particular", "Licencia Liviana", "Licencia Pesada", "Licencia Pesada-T", "Licencia de motociclista"]; ?>
                                    <?php foreach ($tipos as $tipo): ?>
                                    <?php if ($tipo == $licencia['tipoLicencia'][$key]): ?>
                                    <option value="<?= $tipo ?>" selected><?= $tipo ?></option>
                                    <?php else: ?>
                                    <option value="<?= $tipo ?>"><?= $tipo ?></option>
                                    <?php endif ?>
                                    <?php endforeach ?>
                                  </select>
                                </div>
                                <div class="row">
                                  <div class="col-12">
                                    <button type="button" class="btn btn-default float-left" data-dismiss="modal">Cerrar</button>
                                    <button type="submit" class="btn btn-primary float-right" name="updateLicense" value="<?= $value ?>">Actualizar Licencia</button>
                                  </div>
                                </div>
                              </div>
                            </form>
                          </div>
                          <div class="modal-footer bg-primary">
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- /. Modal -->
                  <?php endforeach ?>
                  </div>
                  <div class="row">
                    <div class="col-12">
                      <button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="#newLicense">
                        Ingresar otra licencia
                      </button>
                    </div>
                  </div>
                  <?php else: ?>
                  <div class="row">
                    <div class="col-12">
                      <button type="button" class="btn btn-info" data-toggle="modal" data-target="#newLicense">
                        Ingresa aqui tu licencia de conducir
                      </button>
                    </div>
                  </div>
                  <?php endif ?>
                </div>
                <!-- /. Licencia -->
                <!-- Modal -->
                <div class="modal fade" id="newLicense" tabindex="-1" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Nueva Licencia</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <form action="<?= URL ?>" method="post" accept-charset="utf-8">
                          <div class="card-body">
                            <div class="form-group">
                              <label for="dui">DUI</label>
                              <input type="text" name="dui" class="form-control" id="dui" placeholder="00000000-0" required autofocus>
                            </div>
                            <div class="form-group">
                              <label for="nit">NIT</label>
                              <input type="text" name="nit" class="form-control" id="nit" placeholder="0000-000000-000-0" required>
                              <small id="mnsj" class="form-text text-muted"></small>
                            </div>
                            <div class="form-group">
                              <label for="date1">Fecha de expedición de tu licencia</label>
                              <input type="date" name="fechaExp" class="form-control" id="date1" required>
                            </div>
                            <div class="form-group">
                              <label for="date2">Fecha de vencimiento de tu licencia</label>
                              <input type="date" name="fechaVenc" class="form-control" id="date2" required>
                            </div>
                            <div class="form-group">
                              <label for="tipo">Tipo de licencia</label>
                              <select name="type" class="form-control">
                                <option value="Licencia Particular">Licencia Particular</option>
                                <option value="Licencia Liviana">Licencia Liviana</option>
                                <option value="Licencia Pesada">Licencia Pesada</option>
                                <option value="Licencia Pesada-T">Licencia Pesada-T</option>
                                <option value="Licencia de motociclista">Licencia de motociclista</option>
                              </select>
                            </div>
                            <div class="row">
                              <div class="col-12">
                                <button type="button" class="btn btn-default float-left" data-dismiss="modal">Cerrar</button>
                                <button type="submit" class="btn btn-primary float-right" name="newLicense">Ingresar Licencia</button>
                              </div>
                            </div>
                          </div>
                        </form>
                      </div>
                      <div class="modal-footer bg-primary">
                      </div>
                    </div>
                  </div>
                </div>
                <!-- /. Modal -->

                <div class="tab-pane" id="access">
                  <form class="form-horizontal">
                    <div class="form-group row">
                      <label for="inputName" class="col-sm-2 col-form-label">Cuenta</label>
                      <div class="col-sm-10">
                        <input type="email" class="form-control" id="inputName" value="<?= $user['email'] ?>" disabled="true">
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="offset-sm-2 col-sm-10">
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-default">
                          <i class="fas fa-key"></i> Actualizar contraseña
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
                <!-- /.tab-pane -->
              </div>
              <!-- /.tab-content -->
            </div><!-- /.card-body -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"><i class="fas fa-key"></i> Actualizar contraseña</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?= URL ?>" method="post" accept-charset="utf-8">
          <div class="card-body">
            <div class="form-group">
              <label for="currentPass">Contraseña actual</label>
              <input type="password" name="currentPass" class="form-control" id="currentPass" placeholder="Password" required autofocus>
            </div>
            <div class="form-group">
              <label for="pass1">Nueva contraseña</label>
              <input type="password" name="pass" class="form-control" id="pass1" placeholder="Password" onkeyup="validapass1()" required>
              <small id="mnsj" class="form-text text-muted"></small>
            </div>
            <div class="form-group">
              <label for="pass2">Repita su contraseña</label>
              <input type="password" name="password" class="form-control" id="pass2" placeholder="Password" onkeyup="validapass2()" required>
              <small id="mnsj2" class="form-text text-muted"></small>
            </div>
            <div class="row">
              <div class="col-12">
                <button type="button" class="btn btn-default float-left" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-primary float-right" name="update_password" id="reset_password">Restablecer contraseña</button>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer bg-primary justify-content-between">
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="picProfile">
  <div class="modal-dialog modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"><i class="fas fa-users"></i> Seleccionar imagen</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table class="table table-borderless">
          <tbody>
            <?php $r = round(count($fotos['idFoto']) / 4); $y = 3; $x = 0; ?>
            <?php for ($i = 0; $i <= $r; $i++): ?>
              <tr>
              <?php for ($j = $x; $j <= $y; $j++): ?>
                <?php if ($j != count($fotos['idFoto'])): ?>
                <td>
                  <a href="<?= URL ?>?updatePicProfile=<?= $fotos['idFoto'][$j] ?>">
                    <img src="data:image/png;base64,<?= $fotos['foto'][$j] ?>" class="w-75 imgProfile">
                  </a>
                </td>
                <?php else: ?>
                <?php break; ?>
                <?php endif ?>
              <?php endfor ?>
              </tr>
              <?php $x = $j; $y += ($i == $r) ? 3 : 4; ?>
            <?php endfor ?>
          </tbody>
        </table>
      </div>
      <div class="modal-footer bg-primary justify-content-between">
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- REQUIRED SCRIPTS -->

<script src="dist/js/validaciones.js"></script>

<!-- SweetAlert2 -->
<script src="plugins/sweetalert2/sweetalert2.min.js"></script>

<?php require_once APP . "/views/master/footer_js.php"; ?>

<?= $objController->sweetAlert(2000); ?>

<?php require_once APP . "/views/master/footer_end.php"; ?>
