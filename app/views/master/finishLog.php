<?php require_once APP."/views/master/header.php"; ?>

<!-- SweetAlert2 -->
<link rel="stylesheet" href="plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">

<?php if ($_SESSION['log']['level'] == "administrador"): ?>
<?php require_once APP."/views/master/admin-nav.php"; ?>
<?php else: ?>
<?php require_once APP."/views/master/emp-nav.php"; ?>
<?php endif ?>

<?php $datos = $model->infoBitacora($_SESSION['log']['id']); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
					<h1>Finalizar bitácora</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                      <li class="breadcrumb-item"><a href="<?= URL ?>?request=home">Inicio</a></li>
                      <li class="breadcrumb-item">Cronogramas</li>
                      <li class="breadcrumb-item active">Finalizar bitácora</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <form action="<?= URL ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data">
	    <section class="content">
	    	<div class="container-fluid">
		        <div class="row">
		        	<!-- left column -->
		        	<div class="col-12">
			            <!-- general form elements -->
			            <div class="card card-primary">
			              	<div class="card-header">
			              		<h3 class="card-title">Finalizar registro de salida</h3>
			              	</div>
							<!-- /.card-header -->
							<!-- form start -->
							<div class="card-body">
								<div class="form-row">
									<div class="col-12 col-sm-6 col-md-2">
										<div class="form-group">
											<label>Fecha de salida</label>
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text">
														<i class="fas fa-calendar-alt"></i>
													</span>
												</div>
												<?php $fechaSalida = date_format(date_create($datos['fechaSalida']), 'd-M-Y'); ?>
												<input type="text" class="form-control" value="<?= $fechaSalida ?>" disabled>
											</div>
										</div>
									</div>
									<div class="col-12 col-sm-6 col-md-2">
										<div class="form-group">
											<label>Hora de salida</label>
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text">
														<i class="fas fa-clock"></i>
													</span>
												</div>
												<input type="text" class="form-control" value="<?= $datos['HoraSalida'] ?>" disabled>
											</div>
										</div>
									</div>
									<div class="col-12 col-sm-6 col-md-2">
										<div class="form-group">
											<label>Kilometraje de salida</label>
											<input type="text" class="form-control" value="<?= $datos['kmInicial'] ?>" disabled>
										</div>
									</div>
								</div>
								<div class="form-row">
									<div class="col-12 col-sm-6 col-md-2">
										<div class="form-group">
											<label>Destino</label>
											<input type="text" class="form-control" value="<?= $datos['destino'] ?>" disabled>
										</div>
									</div>
									<div class="col-12 col-sm-6 col-md-2">
										<div class="form-group">
											<label>Motivo del viaje</label>
											<input type="text" class="form-control" value="<?= $datos['motivo'] ?>" disabled>
										</div>
									</div>
								</div>
								<div class="form-row">
									<div class="col-12 col-sm-6 col-md-6">
										<div class="form-group">
											<label>Observaciones</label>
											<input type="text" class="form-control" value="<?= $datos['observaciones'] ?>" disabled>
										</div>
									</div>
								</div>
								<div class="form-row">
									<div class="col-12 col-sm-6 col-md-2">
										<div class="form-group">
											<label>Fecha de entrada</label>
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text">
														<i class="fas fa-calendar-alt"></i>
													</span>
												</div>
												<input type="text" class="form-control" value="<?= $objController->date_time("date") ?>" disabled>
											</div>
										</div>
									</div>
									<div class="col-12 col-sm-6 col-md-2">
										<div class="form-group">
											<label>Hora de entrada</label>
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text">
														<i class="fas fa-clock"></i>
													</span>
												</div>
												<input type="time" class="form-control" name="horaentrada" required>
											</div>
										</div>
									</div>
									<div class="col-12 col-sm-6 col-md-2">
										<div class="form-group">
											<label for="kmFinal">Kilometraje de entrada</label>
											<input type="text" class="form-control" id="kmFinal" name="kmFinal" autofocus required>
										</div>
									</div>
								</div>
							</div>
			            </div>
			            <!-- /.card -->
		          	</div>
		          	<!--/.col (left) -->
		        </div>
		        <!-- /.row -->
	        	<div class="row">
					<div class="col-12">
						<!-- /.card -->
	                    <div class="card card-primary">
	                        <div class="card-header">
	                            <h3 class="card-title">Hoja de salida</h3>
	                        </div>
	                        <!-- /.card-header -->
	                        <div class="card-body">
	                            <?php $detalle = $model->ultimoDetalleHoja($_SESSION['val']); ?>
	                            <?php $_SESSION['ultimoDetalleHoja'] = $detalle; ?>
	                            <?php $r = round(count($detalle['idItem']) / 4); ?>
	                            <?php $y = 3; ?>
	                            <?php $x = 0; ?>
	                            <?php for ($i = 0; $i <= $r; $i++): ?>
	                            <div class="form-row">
	                            <?php for ($j = $x; $j <= $y; $j++): ?>
	                            <?php if ($j != count($detalle['idItem'])): ?>
	                            <div class="col-12 col-sm-6 col-md-2">
	                            	<div class="form-group">
	                            		<label><?= $detalle['item'][$j] ?></label>
	                            		<?php $estados = $model->listarEstados(); ?>
	                            		<select name="item_<?= $detalle['idItem'][$j] ?>" class="form-control">
	                            			<?php $arreglo = [8, 9, 10]; ?>
	                            			<?php foreach ($estados['idEstado'] as $key => $value): ?>
	                            				<?php if (in_array($value, $arreglo)): ?>
	                            					<?php if ($value == $detalle['idEstado'][$j]): ?>
	                            						<option value="<?= $detalle['idEstado'][$j] ?>" selected><?= $detalle['estado'][$j] ?></option>
	                            					<?php else: ?>
	                            						<option value="<?= $estados['idEstado'][$key] ?>"><?= $estados['estado'][$key] ?></option>
	                            					<?php endif ?>
	                            				<?php endif ?>
	                            			<?php endforeach ?>
	                            		</select>
	                            	</div>
	                            </div>
	                            <?php else: ?>
	                                <?php break; ?>
	                            <?php endif ?>
	                            <?php endfor ?>
	                            </div>
	                            <?php $x = $j; $y += ($i == $r) ? 3 : 4; ?>
	                            <?php endfor ?>
	                            <div class="form-row">
                                	<div class="col-12 col-sm-6 col-md-2">
                                		<div class="form-group">
                                			<label>Nivel actual de Gasolina</label>
                                			<select name="nivelGas" class="form-control" required>
                                				<option value="1">1/4 de tanque</option>
                                				<option value="2">2/4 de tanque</option>
                                				<option value="3">3/4 de tanque</option>
                                				<option value="4">Tanque lleno</option>
                                			</select>
                                		</div>
                                	</div>
                                </div>
	                            <div class="form-row">
									<div class="col-12 col-sm-6 col-md-12">
										<div class="form-row">
											<div class="col-12 col-sm-6 col-md-2">
												<div class="form-group">
													<label for="customFile">Costado derecho</label>
													<div class="custom-file">
														<input type="file" accept="image/*" class="custom-file-input" id="der" name="derecha" required>
														<label class="custom-file-label" for="der">Choose file</label>
													</div>
												</div>
											</div>
											<div class="col-12 col-sm-6 col-md-2">
												<div class="form-group">
													<label for="customFile">Costado izquierdo</label>
													<div class="custom-file">
														<input type="file" accept="image/*" class="custom-file-input" id="izq" name="izquierda" required>
														<label class="custom-file-label" for="izq">Choose file</label>
													</div>
												</div>
											</div>
											<div class="col-12 col-sm-6 col-md-2">
												<div class="form-group">
													<label for="customFile">Bomper frontal</label>
													<div class="custom-file">
														<input type="file" accept="image/*" class="custom-file-input" id="front" name="adelante" required>
														<label class="custom-file-label" for="front">Choose file</label>
													</div>
												</div>
											</div>
											<div class="col-12 col-sm-6 col-md-2">
												<div class="form-group">
													<label for="customFile">Bomper trasero</label>
													<div class="custom-file">
														<input type="file" accept="image/*" class="custom-file-input" id="back" name="atras" required>
														<label class="custom-file-label" for="back">Choose file</label>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
	                            <div class="form-row">
	                            	<div class="col-12 col-sm-6 col-md-12">
	                            		<div class="form-group">
	                            			<label id="hojaObserv">Observaciones</label>
	                            			<textarea class="form-control" name="hojaObserv" id="hojaObserv" rows="1"></textarea>
	                            		</div>
	                            	</div>
	                            </div>
	                        </div>
	                        <div class="card-footer">
	                        	<button type="submit" class="btn btn-danger" name="finalizarRuta">
									<i class="fad fa-flag-checkered"></i> Finalizar Ruta
								</button>
	                        </div>
	                    </div>
	                    <!-- /.card -->
					</div>
				</div>
				<!--/.row -->
	      	</div><!-- /.container-fluid -->
	    </section>
	</form>
    <!-- /.content -->
</div>

<!-- REQUIRED SCRIPTS -->

<?php require_once APP."/views/master/footer_js.php"; ?>

<!-- SweetAlert2 -->
<script src="plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- bs-custom-file-input -->
<script src="plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>

<?= $objController->sweetAlert(2000) ?>

<script>
    $(document).ready(function() {
        $(function () {
            bsCustomFileInput.init();
        });
    });
</script>

<?php require_once APP."/views/master/footer_end.php"; ?>