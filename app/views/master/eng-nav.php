</head>
<body class="hold-transition sidebar-mini layout-navbar-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="<?= URL ?>?request=home" class="nav-link">Inicio</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="<?= URL ?>?request=support" class="nav-link">Soporte</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="<?= URL ?>?request=info" class="nav-link">Info</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="<?= URL ?>?request=logout" class="nav-link text-danger">
          <i class="fas fa-sign-out-alt"></i> Cerrar Sesión
        </a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-bell"></i>
          <!--span class="badge badge-warning navbar-badge">15</span-->
        </a>
        <!--div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-header">15 Notifications</span>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-envelope mr-2"></i> 4 new messages
            <span class="float-right text-muted text-sm">3 mins</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-users mr-2"></i> 8 friend requests
            <span class="float-right text-muted text-sm">12 hours</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-file mr-2"></i> 3 new reports
            <span class="float-right text-muted text-sm">2 days</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
        </div-->
      </li>
      <li class="nav-item">
        <a class="nav-link text-blue" href="<?= URL ?>">
          <i class="fad fa-sync"></i>
          <!--span class="badge badge-warning navbar-badge">15</span-->
        </a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?= URL ?>?request=home" class="brand-link">
      <img src="img/white.png" alt="FleetSys" class="brand-image" style="opacity: .9">
      <span class="brand-text font-weight-light">FleetSys</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="data:imge/png;base64,<?= $user['foto'] ?>" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="<?= URL ?>?request=profile" class="d-block"><?= $user['nombre1'] ?> <?= $user['apellido1'] ?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview">
            <a href="<?= URL ?>?request=home" class="nav-link">
              <i class="nav-icon fas fa-home"></i>
              <p>
                Inicio
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-calendar-alt"></i>
              <p>
                Salidas
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= URL ?>?request=bitacoras" class="nav-link">
                  <i class="fas fa-chevron-circle-right nav-icon"></i>
                  <p>Bitácoras</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= URL ?>?request=newLog" class="nav-link">
                  <i class="fas fa-chevron-circle-right nav-icon"></i>
                  <p>Llenar bitácora</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-truck-pickup"></i>
              <p>
                Vehículos
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= URL ?>?request=incidencias" class="nav-link">
                  <i class="fas fa-chevron-circle-right nav-icon"></i>
                  <p>Incidencias</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= URL ?>?request=nuevaIncidencia" class="nav-link">
                  <i class="fas fa-chevron-circle-right nav-icon"></i>
                  <p>Nueva Incidencia</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fab fa-buffer"></i>
              <p>
                Otros
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= URL ?>?request=profile" class="nav-link">
                  <i class="fas fa-chevron-circle-right nav-icon"></i>
                  <p>Mi Usuario</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= URL ?>?request=support" class="nav-link">
                  <i class="fas fa-chevron-circle-right nav-icon"></i>
                  <p>Soporte</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= URL ?>?request=info" class="nav-link">
                  <i class="fas fa-chevron-circle-right nav-icon"></i>
                  <p>Info</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="<?= URL ?>?request=logout" class="nav-link">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p>
                Cerrar sesión
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>