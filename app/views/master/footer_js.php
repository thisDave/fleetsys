</div>

<?php if (isset($_SESSION['notificaciones']) && $_SESSION['notificaciones'] == true): ?>
<div class="modal fade" id="notificaciones">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"><i class="fas fa-flag-checkered"></i> Acciones pendientes</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<p>
        	Hay preordenes de mantenimiento que deben ser atendidas, para ver las preordenes pendientes puedes dar clic en <i class="far fa-bell"></i>.
      	</p>
      	<a href="<?= URL ?>?dntshwntf" class="btn btn-primary">No volver a mostrar</a>
      </div>
      <div class="modal-footer bg-primary justify-content-between">
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<?php endif ?>

<!-- ./wrapper -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI -->
<script src="plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>

<?php if (isset($_SESSION['notificaciones']) && $_SESSION['notificaciones'] == true): ?>
<script>
	$(document).ready(function() {
		$('#notificaciones').modal('show');
	});
</script>
<?php endif ?>

<script>
	function showbell(obj) {
		document.getElementById(obj.id).classList.add('show');;
		document.getElementById('bells').classList.add('show');;
		document.getElementById('bell-link').setAttribute("aria-expanded", "true");
	}

	function hidebell(obj) {
		document.getElementById(obj.id).classList.remove('show');;
		document.getElementById('bells').classList.remove('show');;
		document.getElementById('bell-link').setAttribute("aria-expanded", "false");
	}
</script>