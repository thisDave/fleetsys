<?php require_once APP."/views/master/header.php"; ?>

<!-- SweetAlert2 -->
<link rel="stylesheet" href="plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">

<!-- Select2 -->
<link rel="stylesheet" href="plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

<?php if ($_SESSION['log']['level'] == "administrador"): ?>
<?php require_once APP."/views/master/admin-nav.php"; ?>
<?php else: ?>
<?php require_once APP."/views/master/emp-nav.php"; ?>
<?php endif ?>

<?php if ($model->validaLicencia($_SESSION['log']['id'])): ?>

<?php $val = $model->validarBitacora($_SESSION['log']['id']); ?>

<?php if ($val == 'is_empty'): ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                  <h1>Llenar bitácora</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?= URL ?>?request=home">Inicio</a></li>
						<li class="breadcrumb-item">Cronogramas</li>
						<li class="breadcrumb-item active">finalizar bitácora</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
		<div class="container-fluid">
			<div class="row">
				<!-- left column -->
				<div class="col-12 col-sm-6 col-md-6">
					<div class="alert alert-info display-6" role="alert">
						Actualmente no hay bitacoras disponibles
					</div>
				</div>
			</div>
		</div>
    </section>
    <!-- /.content -->
</div>

<?php elseif ($val == 'available'): ?>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<?php if (isset($_SESSION['typeSelected']) && isset($_SESSION['autoSelected'])): ?>
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
		    <div class="row mb-2">
				<div class="col-sm-6">
					<h1>Llenar bitácora</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?= URL ?>?request=home">Inicio</a></li>
						<li class="breadcrumb-item">Cronogramas</li>
						<li class="breadcrumb-item active">Llenar bitácora</li>
					</ol>
				</div>
		    </div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<!-- form start -->
			<form action="<?= URL ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data">
				<div class="row">
					<!-- left column -->
					<div class="col-12">
						<!-- general form elements -->
						<div class="card card-primary">
							<div class="card-header">
								<h3 class="card-title">Bitácora</h3>
							</div>
							<!-- /.card-header -->
							<div class="card-body">
								<div class="form-row">
									<div class="col-12 col-sm-6 col-md-2">
										<div class="form-group">
											<label>Fecha de salida</label>
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text">
														<i class="fas fa-calendar-alt"></i>
													</span>
												</div>
												<input type="text" class="form-control" value="<?= $objController->date_time("date") ?>" disabled>
											</div>
										</div>
									</div>
									<div class="col-12 col-sm-6 col-md-2">
										<div class="form-group">
											<label>Hora de salida</label>
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text">
														<i class="fas fa-clock"></i>
													</span>
												</div>
												<input type="time" class="form-control" name="horasalida" required>
											</div>
										</div>
									</div>
									<div class="col-12 col-sm-6 col-md-2">
										<div class="form-group">
											<label>Kilometraje de salida</label>
											<?php $lastKm = $model->lastKm($_SESSION['autoSelected']); ?>
											<input type="text" class="form-control" value="<?= $lastKm ?>" disabled>
										</div>
									</div>
									<div class="col-12 col-sm-6 col-md-2">
										<div class="form-group">
											<label id="destino">Destino</label>
											<input type="text" class="form-control" id="destino" name="destino" autocomplete="off" autofocus required>
										</div>
									</div>
									<div class="col-12 col-sm-6 col-md-2">
										<div class="form-group">
											<label id="motivo">Motivo del viaje</label>
											<input type="text" class="form-control" id="motivo" name="motivo" autocomplete="off" required>
										</div>
									</div>
								</div>
								<div class="form-row">
									<div class="col-12 col-sm-6 col-md-9">
										<div class="form-group">
											<label id="observaciones">Observaciones</label>
											<textarea class="form-control" name="observaciones" id="observaciones" rows="2"></textarea>
										</div>
									</div>
								</div>
							</div>
							<!-- /.card -->
						</div>
						<!-- /.card -->
					</div>
					<!--/.col (left) -->
				</div>
				<!-- /.row -->
				<div class="row">
					<div class="col-12">
						<!-- /.card -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Hoja de salida</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <?php $detalle = $model->ultimoDetalleHoja($_SESSION['autoSelected']); ?>
                                <?php $_SESSION['ultimoDetalleHoja'] = $detalle; ?>
                                <?php $r = round(count($detalle['idItem']) / 4); ?>
                                <?php $y = 3; ?>
                                <?php $x = 0; ?>
                                <?php for ($i = 0; $i <= $r; $i++): ?>
                                <div class="form-row">
                                <?php for ($j = $x; $j <= $y; $j++): ?>
                                <?php if ($j != count($detalle['idItem'])): ?>
                                <div class="col-12 col-sm-6 col-md-2">
                                	<div class="form-group">
                                		<label><?= $detalle['item'][$j] ?></label>
                                		<?php $estados = $model->listarEstados(); ?>
                                		<select name="item_<?= $detalle['idItem'][$j] ?>" class="form-control">
                                			<?php $arreglo = [8, 9, 10]; ?>
                                			<?php foreach ($estados['idEstado'] as $key => $value): ?>
                                				<?php if (in_array($value, $arreglo)): ?>
                                					<?php if ($value == $detalle['idEstado'][$j]): ?>
                                						<option value="<?= $detalle['idEstado'][$j] ?>" selected><?= $detalle['estado'][$j] ?></option>
                                					<?php else: ?>
                                						<option value="<?= $estados['idEstado'][$key] ?>"><?= $estados['estado'][$key] ?></option>
                                					<?php endif ?>
                                				<?php endif ?>
                                			<?php endforeach ?>
                                		</select>
                                	</div>
                                </div>
                                <?php else: ?>
                                    <?php break; ?>
                                <?php endif ?>
                                <?php endfor ?>
                                </div>
                                <?php $x = $j; $y += ($i == $r) ? 3 : 4; ?>
                                <?php endfor ?>
                                <?php $hoja = $model->ultimaHoja($_SESSION['autoSelected']); ?>
                                <div class="form-row">
                                	<div class="col-12 col-sm-6 col-md-2">
                                		<div class="form-group">
                                			<label>Nivel actual de Gasolina</label>
                                			<input type="text" class="form-control" value="<?= $hoja['nivelGas'] ?>" disabled>
                                		</div>
                                	</div>
                                	<div class="col-12 col-sm-6 col-md-3">
                                		<div class="form-group">
                                			<label>Observación actual</label>
                                			<textarea class="form-control" rows="1" disabled><?= $hoja['observacionFinal'] ?></textarea>
                                		</div>
                                	</div>
                                </div>
                                <div class="form-row">
                                	<div class="col-12 col-sm-6 col-md-6">
                                		<div class="form-group">
                                			<label id="hojaObserv">Nuevas Observaciones</label>
                                			<textarea class="form-control" name="hojaObserv" id="hojaObserv" rows="1"></textarea>
                                		</div>
                                	</div>
                                </div>
                                <div class="row">
                                	<div class="col-12 col-sm-6 col-md-12">
                                    	<div class="form-group">
	                                    	<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#fotosGolpes">
		                                        Ver detalles del vehículo
		                                    </button>
	                                	</div>
                                    </div>
                                </div>
                                <?php $golpes = $model->ultimoDetalleGolpes($_SESSION['autoSelected']); ?>
								<!-- Modal golpes -->
								<div class="modal fade" id="fotosGolpes" tabindex="-1" aria-hidden="true">
									<div class="modal-dialog modal-xl">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title">Detalles del vehículo</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												<div class="row">
													<div class="col-12 col-sm-12 col-md-6">
														<div class="card w-100">
															<img class="w-100 card-img-top" src="<?= $golpes['foto'][0] ?>">
															<div class="card-body">
																<h5 class="card-title">Lado derecho</h5>
																<p class="card-text">
																	Golpes: <strong><?= $golpes['tipo'][0] ?></strong>
																</p>
															</div>
															<div class="card-footer bg-primary"></div>
														</div>
													</div>
													<div class="col-12 col-sm-12 col-md-6">
														<div class="card w-100">
															<img class="w-100 card-img-top" src="<?= $golpes['foto'][1] ?>">
															<div class="card-body">
																<h5 class="card-title">Lado izquierdo</h5>
																<p class="card-text">
																	Golpes: <strong><?= $golpes['tipo'][1] ?></strong>
																</p>
															</div>
															<div class="card-footer bg-primary"></div>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-12 col-sm-12 col-md-6">
														<div class="card w-100">
															<img class="w-100 card-img-top" src="<?= $golpes['foto'][2] ?>">
															<div class="card-body">
																<h5 class="card-title">Lado frontal</h5>
																<p class="card-text">
																	Golpes: <strong><?= $golpes['tipo'][2] ?></strong>
																</p>
															</div>
															<div class="card-footer bg-primary"></div>
														</div>
													</div>
													<div class="col-12 col-sm-12 col-md-6">
														<div class="card w-100">
															<img class="w-100 card-img-top" src="<?= $golpes['foto'][3] ?>">
															<div class="card-body">
																<h5 class="card-title">Lado trasero</h5>
																<p class="card-text">
																	Golpes: <strong><?= $golpes['tipo'][3] ?></strong>
																</p>
															</div>
															<div class="card-footer bg-primary"></div>
														</div>
													</div>
												</div>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
											</div>
										</div>
									</div>
								</div>
								<!-- fin golpes -->
                            </div>
                            <div class="card-footer">
                            	<button type="submit" class="btn btn-success" name="iniciarRuta">
									<i class="fad fa-flag-checkered"></i> Iniciar Ruta
								</button>
								<button type="button" class="btn btn-danger float-right" data-toggle="modal" data-target="#cancel">
									<i class="far fa-trash-alt"></i> Cancelar salida
								</button>
                            </div>
                        </div>
                        <!-- /.card -->
					</div>
				</div>
				<!--/.row -->
			</form>
			<!-- /.form -->
		</div>
		<!-- /.container-fluid -->
	</section>
	<!-- /.content -->
<?php endif ?>
</div>
<!-- /.content-wrapper -->

<?php if (!isset($_SESSION['typeSelected'])): ?>

<?php $regiones = $model->listarRegiones($model->getIdPais($user['pais'])); ?>

<div class="modal fade" id="start" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Nuevo llenado de bitácora</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="<?= URL ?>" method="post" accept-charset="utf-8">
					<div class="form-row">
						<div class="col-12">
							<div class="form-group">
								<label for="region">Selecciona tu región</label>
								<select class="form-control" id="region" name="region" required>
									<?php foreach ($regiones['id'] as $key => $value): ?> 
										<option value="<?= $regiones['id'][$key] ?>">
											<?= $regiones['region'][$key] ?>
										</option>
									<?php endforeach ?>
								</select>
							</div>
						</div>
					</div>
					<div class="form-row">
						<div class="col-12">
							<div class="form-group">
								<label for="tipo">Selecciona el tipo de vehículo que utilizarás</label>
								<select class="form-control" id="tipo" name="tipo" required>
									<option value="autos">Vehículo</option>
									<option value="motos">Motocicleta</option>
								</select>
							</div>
						</div>
						<div class="col-12 mt-3">
							<div class="form-group">
								<button type="button" class="btn btn-link text-drk float-left" data-dismiss="modal">Cerrar</button>
								<button type="submit" class="btn btn-sm btn-success float-right" name="startLogging">Iniciar registro</button>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer bg-primary justify-content-between"></div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<?php else: ?>

<?php if (!isset($_SESSION['autoSelected'])): ?>

<?php $autos = $model->listarVehiculos($model->getIdPais($user['pais']), $_SESSION['typeSelected']); ?>
<?php $titulo = ($_SESSION['typeSelected'] == 'motos') ? 'motocicleta':'vehículo'; ?>
<?php $subtitulo = ($_SESSION['typeSelected'] == 'motos') ? 'la motocicleta':'el vehículo'; ?>

<div class="modal fade" id="carList" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Selecciona tu <?= $titulo ?></h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="<?= URL ?>" method="post" accept-charset="utf-8">
					<div class="form-row">
						<div class="col-12">
							<div class="form-group">
								<label for="auto">Selecciona <?= $subtitulo ?> que deseas utilizar</label>
								<select class="form-control select2" id="auto" name="auto" style="width: 100%;" required>
									<?php foreach ($autos['idVehiculo'] as $key => $value): ?>
										<?php if ($_SESSION['region'] == $model->getIdRegion($autos['region'][$key])): ?>
											<?php if ($model->isAvailable($autos['idVehiculo'][$key])): ?>
												<option value="<?= $autos['idVehiculo'][$key] ?>">
													<?= $autos['placa'][$key] ?> | <?= $autos['marca'][$key] ?> | <?= $autos['modelo'][$key] ?>
												</option>
											<?php endif ?>
										<?php endif ?>
									<?php endforeach ?>
								</select>
							</div>
						</div>
						<div class="col-12 mt-3">
							<div class="form-group">
								<a href="<?= URL ?>?selecType" class="btn btn-link text-drk float-left">Volver</a>
								<button type="submit" name="selectCar" class="btn btn-sm btn-success float-right">Seleccionar <?= $titulo ?></button>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer bg-primary justify-content-between"></div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<?php endif ?>

<?php endif ?>

<div class="modal fade" id="cancel">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<div class="card-body text-center">
					<p class="display-7">
						¿Realmente desea cancelar la salida del vehiculo?<br>
						Se perderá todo el progreso del registro.
					</p>
				</div>
				<button type="button" class="btn btn-link text-drk float-left" data-dismiss="modal">Cerrar</button>
				<a href="<?= URL ?>?cancelLogging" class="btn btn-sm btn-danger float-right">Cancelar ingreso</a>
			</div>
			<div class="modal-footer bg-primary justify-content-between"></div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<?php else: ?>

<div class="content-wrapper">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Finalizar bitácora</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?= URL ?>?request=home">Inicio</a></li>
						<li class="breadcrumb-item">Cronogramas</li>
						<li class="breadcrumb-item active">finalizar bitácora</li>
					</ol>
				</div>
			</div>
		</div>
		<!-- /.container-fluid -->
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<!-- left column -->
				<div class="col-12">
					<!-- general form elements -->
					<div class="card card-primary">
						<div class="card-header">
							<h3 class="card-title">Finalizar registro de salida</h3>
						</div>
						<!-- /.card-header -->
						<!-- form start -->
						<div class="card-body">
							<div class="row">
								<div class="col-12 col-sm-6 col-md-6">
									<p>Actualmente posees una bitácora abierta, si deseas utilizar otro vehículo, finaliza la bitácora actual.</p>
									<p>
										<a href="<?= $val ?>" class="btn btn-danger">
											Finalizar bitácora
										</a>
									</p>
								</div>
							</div>
						</div>
					</div>
					<!-- /.card -->
				</div>
				<!--/.col (left) -->
			</div>
		</div>
	</section>
</div>

<?php endif ?>

<?php else: ?>

<div class="content-wrapper">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Bitácoras</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?= URL ?>?request=home">Inicio</a></li>
						<li class="breadcrumb-item">Cronogramas</li>
						<li class="breadcrumb-item active">finalizar bitácora</li>
					</ol>
				</div>
			</div>
		</div>
		<!-- /.container-fluid -->
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<!-- left column -->
				<div class="col-12">
					<!-- general form elements -->
					<div class="card">
						<div class="card-header">
							<h3 class="card-title">Info Bitácoras</h3>
						</div>
						<!-- /.card-header -->
						<!-- form start -->
						<div class="card-body">
							<div class="row">
								<div class="col-12 col-sm-6 col-md-6">
									<div class="info-box mb-3 bg-warning">
										<span class="info-box-icon">
											<i class="fas fa-empty-set"></i>
										</span>
										<div class="info-box-content">
											<span class="info-box-text"><?= $user['nombre1'] ?> <?= $user['apellido1'] ?></span>
											<span class="info-box-number">Actualmente no posees licencias registradas o autorizadas</span>
										</div>
										<!-- /.info-box-content -->
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /.card -->
				</div>
				<!--/.col (left) -->
			</div>
		</div>
	</section>
</div>

<?php endif ?>

<!-- REQUIRED SCRIPTS -->

<?php require_once APP."/views/master/footer_js.php"; ?>

<!-- SweetAlert2 -->
<script src="plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Select2 -->
<script src="plugins/select2/js/select2.full.min.js"></script>

<?= $objController->sweetAlert(2000) ?>

<script>
    $(document).ready(function () {
        $('.select2').select2({
            theme: 'bootstrap4'
        })
    });
</script>

<?php if (!isset($_SESSION['typeSelected'])): ?>

<script>
	$(document).ready(function() {
		$('#start').modal("show");
	});
</script>

<?php else: ?>

<?php if (!isset($_SESSION['autoSelected'])): ?>

<script>
    $(document).ready(function() {
        $('#carList').modal("show");
    });
</script>

<?php endif ?>

<?php endif ?>

<?php require_once APP."/views/master/footer_end.php"; ?>
