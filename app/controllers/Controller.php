<?php

require_once APP.'/models/Model.php';

require_once 'tools/phpMailer/PHPMailerAutoload.php';

class Controller extends Model
{
	public function actions($action, $value = '')
	{
		switch ($action)
		{
			case 'forgot':
				$_SESSION['gestion'] = 'forget';
			break;

			case 'reset':
				if (strlen($value) == 50)
				{
					if (parent::validarToken($value))
					{
						$_SESSION['gestion'] = 'reset';
						$_SESSION['token'] = $value;
					}
					else
					{
						session_destroy();
					}
				}
			break;

			case 'login':
				session_destroy();
			break;

			case 'resetpass':
				$this->resetPass($_SESSION['email']);
			break;

			case 'delresetpass':
				unset($_SESSION['progressBar']);
				unset($_SESSION['email']);
				unset($_SESSION['resetpass']);
			break;

			case 'delPass':
				$this->delCookie();
			break;
		}

		header("Location: ".URL);
	}

	public function login($email, $pass, $remember = null)
	{
		if (strlen($email) != 0 && strlen($pass) != 0)
		{
			$info = parent::logInfo($email, $pass);

			if($info)
			{
				if ($info === 'firstIn')
				{
					$_SESSION['entro'] = 'yes';
					$key = $this->getKey(50);

					if (parent::setResetToken($email, $key))
					{
						$_SESSION['gestion'] = 'reset';
						$_SESSION['token'] = $key;
					}
				}
				else
				{
					if (!is_null($remember) && $remember = '1')
					{
						$token = $this->getKey(100);

						if (parent::setCookieToken($email, $pass, $token)) {
							setcookie('FLEET', $token, strtotime( '+365 days' ));
						}
					}
				}
			}
			else
			{
				$_SESSION['error'] = "Usuario y/o Contraseña incorrectos";
			}
		}
		else
		{
			$_SESSION['error'] = "Usuario y/o Contraseña incorrectos";
		}

		header("Location: ".URL);
	}

	public function delCookie()
	{
		if (isset($_COOKIE['FLEET']))
		{
			parent::showRes("DELETE FROM tbl_cookies WHERE sessionToken = '".$_COOKIE['FLEET']."'");

			setcookie('FLEET', '', 1);
		}

	}

	public function resetPass($email)
	{
		$asunto = 'Restablecer de contraseña';

		if (isset($_SESSION['progressBar']))
		{
			unset($_SESSION['progressBar']);
		}

		if (strlen($email) != 0)
		{
			$key = $this->getKey(50);

			if (parent::setResetToken($email, $key))
			{
				$html = '
				<!DOCTYPE html>
				<html lang="es-SV">
					<head>
						<meta charset="utf-8">
						<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
						<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
						<title>fleetSys</title>
					</head>
					<body>
						<div class="container-fluid">
							<div class="row mt-3">
								<div class="col-3"></div>
								<div class="col-6 border border-dark">
									<div class="container">
										<div class="row mt-2">
				 							<div class="col-12 text-center">
												<img src="img/logo.png" width="25%">
											</div>
										</div>
										<div class="row mt-2">
											<div class="col-12 text-center">
												<h3 class="display-4">Restablecer contraseña</h3>
											</div>
										</div>
										<div class="row">
											<div class="col-12 text-center">
												<p>Recibimos una solicitud para restablecer tu contraseña, si fuiste tú, haz clic sobre el siguiente enlace:</p>
											</div>
										</div>
										<div class="row">
											<div class="col-12 text-center">
												<a href="'.URL.'?action=reset&value='.$key.'" class="btn btn-primary" target="_blank">RESTABLECER CONTRASEÑA</a>
											</div>
										</div>
										<div class="row mt-3">
											<div class="col-12 text-center">
												<p>
													Si no quieres restablecer tu contraseña, ignora este mensaje y continua ingresando con tu contraseña actual.
												</p>
												<p>
													Gracias por confiar en nosotros.
												</p>
												<p>
													Atentamente:<br>
													<strong>Sistemas Educo El Salvador</strong>
												</p>
											</div>
										</div>
									</div>
								</div>
								<div class="col-3"></div>
							</div>
						</div>
					</body>
				</html>
				';

				if ($this->sendMail($email, $asunto, $html))
				{
					$_SESSION['resetpass'] = true;
				}
				else
				{
					$_SESSION['resetpass'] = false;
				}
			}
			else
			{
				$_SESSION['resetpass'] = false;
			}
		}
	}

	public function resetPassword($pass)
	{
		$arr_pass = str_split($pass);

		$banco = 'ABCDEFGHIJKLMNÑOPQRSTUVWXYZ0123456789abcdefghijklmnñopqrstuvwxyz_@-$!';

		$arr_banco = str_split($banco);

		$x = true;

		foreach ($arr_pass as $valor_pass) {
	        if (!in_array($valor_pass, $arr_banco)) { $x = false; }
		}

		if ($x)
		{
			$password = password_hash($pass, PASSWORD_DEFAULT, ['cost' => 12]);

			if (parent::recoverPassword($password, $_SESSION['token']))
				return true;
			else
				return false;
		}
	}

	public function getKey($length)
	{
	    $cadena = "ABCDFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";
	    $longitudCadena = strlen($cadena);
	    $pass = "";
	    for($i=1 ; $i<=$length ; $i++){
	        $pos=rand(0,$longitudCadena-1);
	        $pass .= substr($cadena,$pos,1);
	    }
	    return $pass;
	}

	public function date_time($request, $date = null)
    {
        date_default_timezone_set("America/El_Salvador");
        setlocale(LC_TIME, "spanish");

        switch ($request)
        {
        	case 'format':
        		$date = str_replace("/", "-", $date);
        		return strftime("%d/%B/%Y", strtotime(date('d-M-Y', strtotime($date))));
        		break;

            case 'date':
                return strftime("%d/%B/%Y", strtotime(date('d-M-Y', time())));
                break;

            case 'datadate':
                return date('Y-m-d', time());
                break;

            case 'time':
                return date('H:i:s', time());
                break;

            default:
                return false;
                break;
        }
    }

	public function sweetAlert($timer_toast)
	{
		$script = '';

		if (isset($_SESSION['sweetAlert']))
		{
			$title = ($_SESSION['sweetAlert']['icon'] == 'success') ? 'Información actualizada' : 'Error en la solicitud';

			$timer = $timer_toast / 1000;

			$script = '
			<meta http-equiv="refresh" content="'.$timer.';URL='.URL.'?delalert=sweetAlert">

			<script>
				var Toast = Swal.mixin({
					toast: false,
					position: \'center\',
					showConfirmButton: false,
					timer: '.$timer_toast.',
					timerProgressBar: true
				});

				Toast.fire({
					icon: \''.$_SESSION['sweetAlert']['icon'].'\',
					title: \''.$title.'\',
					text: \''.$_SESSION['sweetAlert']['text'].'\'
				});
			</script>
			';
		}

		return $script;
	}

    public function sendMail($email, $asunto, $html)
	{
		$mensaje = $html;
		$mail = new PHPMailer;
		$mail->IsSMTP();
		$mail->SMTPAuth = true;
		$mail->SMTPSecure = "tls";
		$mail->Host = "smtp.office365.com";
		$mail->Port = 587;
		$mail->Username = "educo@educo.sv";
		$mail->Password = "$3duco2018";
		$mail->CharSet = "UTF-8";
		$mail->From = "educo@educo.sv";
		$mail->FromName = "Sistemas El Salvador";
		$mail->Subject = $asunto;
		$mail->addAddress($email);
		$mail->MsgHTML($mensaje);

		if($mail->Send()){
			return true;
		}else{
			return false;
		}
		parent::desconectar();
	}
}

$objController = new Controller;

$model = new Model;

if (isset($_GET['action']))
{
	if (isset($_GET['value']))
		$objController->actions($_GET['action'], $_GET['value']);
	else
		$objController->actions($_GET['action']);
}

if (isset($_POST['login']))
{
	if (isset($_POST['remember'])) {
		$objController->login($_POST['email'], $_POST['password'], $_POST['remember']);
	}else {
		$objController->login($_POST['email'], $_POST['password']);
	}
}

if (isset($_POST['reset-pass']))
{
	if (strlen($_POST['email']) != 0)
	{
		$_SESSION['progressBar'] = 1;

		$_SESSION['email'] = $_POST['email'];
	}
	else
	{
		$_SESSION['progressBar'] = 0;
	}

	header("Location: ".URL);
}

if (isset($_POST['reset_password']))
{
	if (strlen($_POST['pass']) >= 8 && strlen($_POST['password']) >= 8)
	{
		if ($_POST['pass'] == $_POST['password'])
		{
			if ($objController->resetPassword($_POST['pass']))
			{
				$_SESSION['validation'] = true;
				unset($_SESSION['token']);
			}
			else
			{
				$_SESSION['validation'] = false;
			}
		}
		else
		{
			$_SESSION['validation'] = false;
		}
	}
	else
	{
		$_SESSION['validation'] = false;
	}
}