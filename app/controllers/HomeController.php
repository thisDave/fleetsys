<?php

require_once 'Controller.php';

class HomeController extends Controller
{
	public function reqviews($request, $value = '')
	{
		switch ($request)
		{
			default:
				$_SESSION['view'] = $request;
				$_SESSION['val'] = $value;
			break;
		}

		header("Location: ".URL);
	}

	public function requests($request, $value = '')
	{
		if (isset($_SESSION['val'])) unset($_SESSION['val']);

		switch ($request)
		{
			case 'home':
				unset($_SESSION['view']);
			break;

			case 'logout':
				parent::salida($_SESSION['log']['id']);
				session_destroy();
			break;

			default:
				$_SESSION['view'] = $request;
			break;
		}

		header("Location: ".URL);
	}

	private function getBinary($pictures)
	{
		$binarios = [];

		foreach ($pictures['tempName'] as $key => $value)
		{
			require_once "tools/Resize/Resize.php";

			/******* Extraemos el valor binario de la imagen original *******/

	        $fp = fopen($pictures['tempName'][$key], "r");
	        $picture_binary = addslashes(fread($fp, $pictures['fileSize'][$key]));
	        fclose($fp);

	        /******* Guardamos la img temporalmente *******/

	        $folder = "picTemp/";

	        if (!is_dir($folder)) { mkdir($folder, 0777, true); }

            $extension = explode('.', $pictures['fileName'][$key]);
            $pictures['fileName'][$key] = $this->getKey(5).".".$extension[1];

	        $destino = $folder.$pictures['fileName'][$key];
	        opendir($folder);
	        move_uploaded_file($pictures['tempName'][$key], $destino);

	        /******* Creamos el thumbnail de la img *******/

	        $resizeObj = new Resize("picTemp/".$pictures['fileName'][$key]);
	        $resizeObj -> resizeImage(128, 128, 'crop');
	        $location = 'picTemp/thumbnail_'.$this->getKey(5).'.jpg';
	        $resizeObj -> saveImage($location, 100);
	        $thumbnail_binary = addslashes(file_get_contents($location));

	        /******* Eliminamos las imagenes y el directorio temporal *******/

	        unlink($destino);
	        unlink($location);
	        rmdir($folder);

	        $binarios['picture_binary'][] = $picture_binary;
	        $binarios['picture_type'][] = $pictures['fileType'][$key];
	        $binarios['thumbnail_binary'][] = $thumbnail_binary;
		}

		return $binarios;
	}

	private function getBinaryPictures($pictures)
	{
		$binarios = [];

		foreach ($pictures['tempName'] as $key => $value)
		{
			/******* Extraemos el valor binario de la imagen original *******/

	        $fp = fopen($pictures['tempName'][$key], "r");
	        $picture_binary = addslashes(fread($fp, $pictures['fileSize'][$key]));
	        fclose($fp);

	        /******* Guardamos el binario y su tipo *******/

	        $binarios['picture_binary'][] = $picture_binary;
	        $binarios['picture_type'][] = $pictures['fileType'][$key];
		}

		return $binarios;
	}

	private function compressImage($source, $destination, $quality)
	{
	    // Obtenemos la información de la imagen
	    $imgInfo = getimagesize($source);
	    $mime = $imgInfo['mime'];

	    // Creamos una imagen
	    switch($mime){
	        case 'image/jpeg':
	            $image = imagecreatefromjpeg($source);
	            break;
	        case 'image/png':
	            $image = imagecreatefrompng($source);
	            break;
	        default:
	            $image = imagecreatefromjpeg($source);
	    }

	    // Devolvemos la imagen comprimida
	    return imagejpeg($image, $destination, $quality);
	}

	private function setPics($idHoja, $pictures)
	{
		//Armamos la carpeta destino
		$folder = "files/{$idHoja}/";

		//Verificamos si la ruta existe
		if (!is_dir($folder)) { mkdir($folder, 0777, true); }

		/*************************************************
		****			PARA FOTO DERECHA 			  ****
		*************************************************/

		//Extraemos la extensión de la foto
		$extension = explode('.', $pictures['derecha']['name']);

		//Cambiamos el nombre del archivo
		$pictures['derecha']['name'] = "derecha.".$extension[1];

		//Guardamos el nombre en otra variable para mejor manejo del código
		$derecha = $pictures['derecha']['name'];

		//Verificamos si el archivo existe en la ruta establecida
		if (file_exists($folder.$derecha)) { unlink($folder.$derecha); }

		//Guardamos la ruta completa de la imagen
		$rd = $folder.$derecha;

		//Comprimimos y subimos la imagen
		$this->compressImage($pictures['derecha']['tmp_name'], $rd, 50);

		/*************************************************
		****			PARA FOTO IZQUIERDA			  ****
		*************************************************/

		$extension = explode('.', $pictures['izquierda']['name']);
		$pictures['izquierda']['name'] = "izquierda.".$extension[1];
		$izquierda = $pictures['izquierda']['name'];
		if (file_exists($folder.$izquierda)) { unlink($folder.$izquierda); }
		$ri = $folder.$izquierda;
		$this->compressImage($pictures['izquierda']['tmp_name'], $ri, 50);

		/*************************************************
		****			PARA FOTO FRONTAL			  ****
		*************************************************/

		$extension = explode('.', $pictures['adelante']['name']);
		$pictures['adelante']['name'] = "adelante.".$extension[1];
		$adelante = $pictures['adelante']['name'];
		if (file_exists($folder.$adelante)) { unlink($folder.$adelante); }
		$rf = $folder.$adelante;
		$this->compressImage($pictures['adelante']['tmp_name'], $rf, 50);

		/*************************************************
		****			PARA FOTO TRASERA			  ****
		*************************************************/

		$extension = explode('.', $pictures['atras']['name']);
		$pictures['atras']['name'] = "atras.".$extension[1];
		$atras = $pictures['atras']['name'];
		if (file_exists($folder.$atras)) { unlink($folder.$atras); }
		$rb = $folder.$atras;
		$this->compressImage($pictures['atras']['tmp_name'], $rb, 50);

		$rutas = [$rd, $ri, $rf, $rb];

		return $rutas;
	}

	public function updateInfoUser($nombre1, $nombre2, $apellido1, $apellido2, $cargo, $region)
	{
		$nombre1 = preg_replace('([^A-Za-zÁ-ź0-9 ])', '', trim($nombre1));
		$nombre2 = preg_replace('([^A-Za-zÁ-ź0-9 ])', '', trim($nombre2));
		$apellido1 = preg_replace('([^A-Za-zÁ-ź0-9 ])', '', trim($apellido1));
		$apellido2 = preg_replace('([^A-Za-zÁ-ź0-9 ])', '', trim($apellido2));
		$cargo = preg_replace('([^A-Za-zÁ-ź0-9 ])', '', trim($cargo));
		$region = preg_replace('([^A-Za-zÁ-ź0-9 ])', '', trim($region));

		$x = true;

		$datos = [ $nombre1, $nombre2, $apellido1, $apellido2, $cargo, $region, ];

		foreach ($datos as $value) { if (empty($value)) { $x = false; break; } }

		if ($x)
		{
			if (parent::actualizarUsuario($nombre1, $nombre2, $apellido1, $apellido2, $cargo, $region))
			{
				$_SESSION['sweetAlert']['icon'] = 'success';
				$_SESSION['sweetAlert']['text'] = 'Datos del usuario actualizados exitosamente';
			}
			else
			{
				$_SESSION['sweetAlert']['icon'] = 'error';
				$_SESSION['sweetAlert']['text'] = 'No fue posible conectarse con la base de datos';
			}
		}
		else
		{
			$_SESSION['sweetAlert']['icon'] = 'error';
			$_SESSION['sweetAlert']['text'] = 'Se encontraron datos vacíos en la solicitud.';
		}

		unset($_SESSION['updateInfoUser']);

		header("Location: ".URL);
	}

	public function updatePicProfile($idFoto, $idUser)
	{
		if (!parent::updtPicProfile($idFoto, $idUser))
		{
			$_SESSION['sweetAlert']['icon'] = 'error';
			$_SESSION['sweetAlert']['text'] = '';
		}

		header("Location: ".URL);
	}

	public function updatePass($currentPass, $pass, $password)
	{
		if (parent::validaPassword($currentPass))
		{
			if (strlen($pass) >= 8 && strlen($password) >= 8)
			{
				if ($pass == $password)
				{
					$arr_pass = str_split($pass);

					$banco = 'ABCDEFGHIJKLMNÑOPQRSTUVWXYZ0123456789abcdefghijklmnñopqrstuvwxyz_@-$!';

					$arr_banco = str_split($banco);

					$x = true;

					foreach ($arr_pass as $valor_pass) { $x = (!in_array($valor_pass, $arr_banco)) ? false : true; }

					if ($x)
					{
						$password = password_hash($pass, PASSWORD_DEFAULT, ['cost' => 12]);

						if (parent::updatePassword($password)) {
							$_SESSION['sweetAlert']['icon'] = 'success';
							$_SESSION['sweetAlert']['text'] = 'Contraseña actualizada exitosamente';
						}
						else {
							$_SESSION['sweetAlert']['icon'] = 'error';
							$_SESSION['sweetAlert']['text'] = 'No fue posible conectarse con la base de datos.';
						}
					}
				}
				else
				{
					$_SESSION['sweetAlert']['icon'] = 'error';
					$_SESSION['sweetAlert']['text'] = 'Las contraseñas no coinciden';
				}
			}
			else
			{
				$_SESSION['sweetAlert']['icon'] = 'error';
				$_SESSION['sweetAlert']['text'] = 'La longitud de las contraseñas son incorrectas';
			}

		}
		else
		{
			$_SESSION['sweetAlert']['icon'] = 'error';
			$_SESSION['sweetAlert']['text'] = 'La contraseña actual es incorrecta';
		}

		header("Location:".URL);
	}

	public function support($asunto, $comentario)
	{
		$user = parent::infoUsuario($_SESSION['log']['id']);

		$asunto = preg_replace('([^A-Za-z0-9 ])', '', trim($asunto));
		$comentario = preg_replace('([^A-Za-z0-9 ])', '', trim($comentario));

		if (!empty($asunto) && !empty($comentario))
		{
			$html = '
			<!DOCTYPE html>
			<html lang="es-SV">
			    <head>
			        <meta charset="utf-8">
			        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
			        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
			        <title>fleetSys</title>
			    </head>
			    <body>
			        <div class="container-fluid">
			            <div class="row">
			                <div class="col-12">
			                    <div class="container">
			                        <div class="row">
			                            <div class="col-12">
			                                <h3>Has recibido una solicitud de soporte en FleetSys</h3>
			                            </div>
			                        </div>
			                        <div class="row mt-3">
			                            <div class="col-12">
			                                <p>
			                                   A continuación te presentamos los datos del usuario:
			                                </p>
			                                <p>
			                                    <strong>Nombre:</strong> '.$user['nombreCompleto'].'<br> <br>
			                                    <strong>Email:</strong> '.$user['email'].' <br> <br>
			                                    <strong>Asunto del problema:</strong> '.$asunto.' <br> <br>
			                                    <strong>Comentarios:</strong> <br> '.$comentario.'
			                                </p>
			                                <p>
			                                    Atentamente:<br>
			                                    <strong>Sistemas Educo El Salvador</strong>
			                                </p>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			            </div>
			        </div>
			    </body>
			</html>
			';

			$_SESSION['alertResult'] = (parent::sendMail('isaac.ramos@educo.org', 'Soporte FleetSys', $html)) ? true : false;
		}
		else
		{
			$_SESSION['alertResult'] = false;
		}

		unset($_SESSION['asunto']);
		unset($_SESSION['comentario']);
		unset($_SESSION['progressBar']);

		header("Location: ".URL);
	}

	public function abrirBitacora($bitacora)
	{
		$x = true;

		for ($i = 0; $i <= 6; $i++)
		{
			if (empty(trim($bitacora[$i]))){ $x = false; break; }
		}

		if ($x)
		{
			if (parent::nuevaBitacora($bitacora))
			{
				$url = URL."?req=finishLog&val=".$_SESSION['autoSelected'];

				unset($_SESSION['region']);
				unset($_SESSION['typeSelected']);
				unset($_SESSION['autoSelected']);

				$_SESSION['sweetAlert']['icon'] = "success";
				$_SESSION['sweetAlert']['text'] = "Bitácora iniciada exitosamente.";

				header("Location: ".$url);
			}
			else
			{
				$_SESSION['sweetAlert']['icon'] = "error";
				$_SESSION['sweetAlert']['text'] = "No se pudo ingresar la información";

				header("Location: ".URL);
			}
		}
		else
		{
			$_SESSION['sweetAlert']['icon'] = "error";
			$_SESSION['sweetAlert']['text'] = "Ingresa datos válidos";

			header("Location: ".URL);
		}
	}

	public function cerrarBitacora($bitacora)
	{
		$infoBitacora = parent::infoBitacora($_SESSION['log']['id']);

		/***** Actualizamos tbl_bitacoras *****/

		if (parent::updateBitacora($bitacora['kmFinal'], $infoBitacora['idBitacora'], $bitacora['horaEntrada']))
		{
			/***** Actualizamos tbl_hojaSalidas *****/

			if (parent::updateHojaSalida($bitacora['nivelGas'], $bitacora['hojaObserv'], $infoBitacora['idBitacora']))
			{
				/***** Actualizamos tbl_detalleHojas *****/

				if (parent::updateDetalleHoja($bitacora['detalleHoja'], $infoBitacora['idBitacora']))
				{
					/***** Actualizamos tbl_detalleGolpes *****/

					$query = "SELECT idHoja FROM tbl_hojaSalidas WHERE idBitacora = ".$infoBitacora['idBitacora'];

					$resultado = parent::showRes($query);

			        $dato = $resultado->fetch_assoc();

			        $idHoja = $dato['idHoja'];

			        $rutas = $this->setPics($idHoja, $bitacora['info_pictures']);

					if (parent::insertarDetalleGolpes($idHoja, $rutas))
					{
						unset($_SESSION['ultimoDetalleHoja']);
						unset($_SESSION['val']);

						$_SESSION['sweetAlert']['icon'] = 'success';
						$_SESSION['sweetAlert']['text'] = 'Bitácora finalizada exitosamente';

						header("Location: ".URL."?request=newLog");
					}
					else
					{
						$_SESSION['sweetAlert']['icon'] = 'error';
						$_SESSION['sweetAlert']['text'] = 'No se pudieron agregar las fotografías';

						header("Location: ".URL);
					}
				}
				else
				{
					$_SESSION['sweetAlert']['icon'] = 'error';
					$_SESSION['sweetAlert']['text'] = 'No se pudo actualizar el detalle de las hojas';

					header("Location: ".URL);
				}
			}
			else
			{
				$_SESSION['sweetAlert']['icon'] = 'error';
				$_SESSION['sweetAlert']['text'] = 'No se pudo actualizar la hoja de salida.';

				header("Location: ".URL);
			}
		}
		else
		{
			$_SESSION['sweetAlert']['icon'] = 'error';
			$_SESSION['sweetAlert']['text'] = 'No se pudo actualizar la bitácora.';

			header("Location: ".URL);
		}
	}

	public function newLicense($licencia)
	{
		if (parent::insertarLicencia($licencia))
		{
			$_SESSION['sweetAlert']['icon'] = "success";
			$_SESSION['sweetAlert']['text'] = "La licencia fue ingresada exitosamente.";
		}
		else
		{
			$_SESSION['sweetAlert']['icon'] = "error";
			$_SESSION['sweetAlert']['text'] = "La licencia no pudo ser ingresada, verifica los datos ingresados e intentalo nuevamente.";
		}

		header("Location: ".URL);
	}

	public function updateLicense($licencia, $idLicencia)
	{
		if (parent::actualizarLicencia($licencia, $idLicencia))
		{
			$_SESSION['sweetAlert']['icon'] = "success";
			$_SESSION['sweetAlert']['text'] = "La licencia fue actualizada exitosamente.";
		}
		else
		{
			$_SESSION['sweetAlert']['icon'] = "error";
			$_SESSION['sweetAlert']['text'] = "La licencia no pudo ser actualizada, verifica los datos ingresados e intentalo nuevamente.";
		}

		header("Location: ".URL);
	}

	public function deleteLicense($id)
	{
		if (parent::showRes("DELETE FROM tbl_licencias WHERE idLicencia = ".$id))
		{
			$_SESSION['sweetAlert']['icon'] = "success";
			$_SESSION['sweetAlert']['text'] = "Licencia eliminada exitosamente.";
		}
		else
		{
			$_SESSION['sweetAlert']['icon'] = "error";
			$_SESSION['sweetAlert']['text'] = "Hubo un error al conectarse con la base de datos.";
		}

		header("Location: ".URL);
	}

	public function showComments()
	{
		$comments = parent::getComments();

		if ($comments)
		{
			foreach ($comments['idComentario'] as $key => $value)
			{
				$profile = $this->infoUsuario($comments['idUsuario'][$key]);

				echo '
				<div class="direct-chat-msg">
					<div class="direct-chat-infos clearfix">
						<span class="direct-chat-name float-left">
						'.$profile['nombre1'].' '.$profile['apellido1'].'
						</span>
						<span class="direct-chat-timestamp float-right">
						'.date_format(date_create($comments['fecha'][$key]), 'd-M-Y').' 
						'.$comments['hora'][$key].'
						</span>
					</div>
					<img class="direct-chat-img" src="data:image/png;base64,'.$profile['foto'].'">
					<div class="direct-chat-text">
						'.$comments['comentario'][$key].'
					</div>';

				if ($_SESSION['log']['id'] == $profile['id'])
				{
					echo '
					<a href="#" class="text-sm text-blue" data-toggle="modal" data-target="#proximamente">Editar</a>
					<a href="'.URL.'?delComment='.$value.'" class="text-sm text-danger">Eliminar</a>';
				}
				echo '</div>';
			}
		}
	}

	public function updateCarsPics($idHoja, $pictures)
	{
		$rutas = $this->setPics($idHoja, $pictures);

		if (parent::updateDetalleGolpes($idHoja, $rutas))
		{
			$_SESSION['sweetAlert']['icon'] = 'success';
			$_SESSION['sweetAlert']['text'] = 'Fotografías actualizadas exitosamente';
		}
		else
		{
			$_SESSION['sweetAlert']['icon'] = 'error';
			$_SESSION['sweetAlert']['text'] = 'No se lograron actualizar las fotografías.';
		}
	}

	public function newAuto($info_auto, $info_hoja, $info_pictures)
	{
		$validation = true;

		foreach ($info_auto as $value) { $validation = (empty($value)) ? false : true; }

		if (parent::validarDatosAuto($info_auto['placa'], $info_auto['motor'], $info_auto['chasis']))
		{
			foreach ($info_auto as $value) { $validation = ($value == '') ? false : true; }

			if ($validation)
			{
				$idBitacora = parent::ingresarAuto($info_auto);

				if ($idBitacora)
				{
					$validation = true;

					foreach ($info_hoja as $value) { $validation = (empty($value)) ? false : true; }

					if ($validation)
					{
						$idHoja = parent::insertarHojaSalida($idBitacora, $info_hoja);

						if ($idHoja)
						{
							$rutas = $this->setPics($idHoja, $info_pictures);

							if (parent::insertarDetalleGolpes($idHoja, $rutas))
							{
								$_SESSION['sweetAlert']['icon'] = 'success';
								$_SESSION['sweetAlert']['text'] = 'Vehículo ingresado exitosamente';
							}
							else
							{
								$_SESSION['sweetAlert']['icon'] = 'error';
								$_SESSION['sweetAlert']['text'] = 'No se pudo ingresar el detalle de los golpes.';
							}
						}
						else
						{
							$_SESSION['sweetAlert']['icon'] = 'error';
							$_SESSION['sweetAlert']['text'] = 'No se pudo ingresar el detalle de accesorios.';
						}
					}
					else
					{

						$_SESSION['sweetAlert']['icon'] = 'error';
						$_SESSION['sweetAlert']['text'] = 'No se pudo ingresar la hoja de salida, verifique los datos';
					}
				}
				else
				{
					$_SESSION['sweetAlert']['icon'] = 'error';
					$_SESSION['sweetAlert']['text'] = 'No se pudo conectar con la base de datos';
				}
			}
			else
			{
				$_SESSION['sweetAlert']['icon'] = 'error';
				$_SESSION['sweetAlert']['text'] = 'Ingrese datos válidos';
			}
		}
		else
		{
			$_SESSION['sweetAlert']['icon'] = 'error';
			$_SESSION['sweetAlert']['text'] = 'Uno o varios de los datos ingresados ya existe.';
		}

		header("Location: ".URL);
	}
}

$objHome = new HomeController;

if (isset($_GET['req']))
{
	if (isset($_GET['val']))
		$objHome->reqviews($_GET['req'], $_GET['val']);
	else
		$objHome->reqviews($_GET['req']);
}

if (isset($_GET['request']))
{
	if (isset($_GET['value']))
		$objHome->requests($_GET['request'], $_GET['value']);
	else
		$objHome->requests($_GET['request']);
}

if (isset($_GET['action']))
{
	$_SESSION[$_GET['action']] = 'true';
	header("Location:".URL);
}

if (isset($_GET['delaction']))
{
	unset($_SESSION[$_GET['delaction']]);
	header("Location:".URL);
}

if (isset($_GET['delalert']))
{
	unset($_SESSION[$_GET['delalert']]);
	header("Location: ".URL);
}

if (isset($_POST['updateInfoUser']))
{
	$objHome->updateInfoUser($_POST['nombre1'], $_POST['nombre2'], $_POST['apellido1'], $_POST['apellido2'], $_POST['cargo'], $_POST['region']);
}

if (isset($_POST['update_password']))
{
	$objHome->updatePass($_POST['currentPass'], $_POST['pass'], $_POST['password']);
}

if (isset($_POST['soporte']))
{
	$_SESSION['asunto'] = $_POST['asunto'];
	$_SESSION['comentario'] = $_POST['comentario'];
	$_SESSION['progressBar'] = true;

	header("Location: ".URL);
}

if (isset($_GET['sendSupport']))
{
	$objHome->support($_SESSION['asunto'], $_SESSION['comentario']);
}

if (isset($_GET['updatePicProfile']))
{
	$objHome->updatePicProfile($_GET['updatePicProfile'], $_SESSION['log']['id']);
}

if (isset($_POST['startLogging']))
{
	$_SESSION['region'] = $_POST['region'];
	$_SESSION['typeSelected'] = $_POST['tipo'];

	header("Location: ".URL);
}

if (isset($_GET['selecType'])) { unset($_SESSION['typeSelected']); header("Location: ".URL); }

if (isset($_POST['selectCar']))
{
	$_SESSION['autoSelected'] = $_POST['auto'];

	header("Location: ".URL);
}

if (isset($_GET['cancelLogging']))
{
	unset($_SESSION['region']);
	unset($_SESSION['typeSelected']);
	unset($_SESSION['autoSelected']);

	header("Location: ".URL);
}

if (isset($_POST['iniciarRuta']))
{
	$detalle = $_SESSION['ultimoDetalleHoja'];

	$detalleHoja = [];

	foreach ($detalle['idItem'] as $value)
	{
		$detalleHoja[] = preg_replace('([^A-Za-z0-9 ])', '', trim($_POST['item_'.$value]));
	}

	$bitacora =
	[
		$_SESSION['autoSelected'],
		$model->lastKm($_SESSION['autoSelected']),
		$objController->date_time("datadate"),
		$_POST['horasalida'],
		$_SESSION['log']['id'],
		preg_replace('([^A-Za-z0-9 ])', '', trim($_POST['destino'])),
		preg_replace('([^A-Za-z0-9 ])', '', trim($_POST['motivo'])),
		preg_replace('([^A-Za-z0-9 ])', '', trim($_POST['observaciones'])),
		preg_replace('([^A-Za-z0-9 ])', '', trim($_POST['hojaObserv'])),
		$detalleHoja
	];

	$objHome->abrirBitacora($bitacora);
}

if (isset($_POST['finalizarRuta']))
{
	$validations = [];

	$types = ['image/jpeg', 'image/jpg', 'image/png'];

	$valDtype = (!in_array($_FILES['derecha']['type'], $types)) ? false : true;
	$valDsize = (($_FILES['derecha']['size'] / 1000000) > 10) ? false : true;

	$valD = ($valDtype && $valDsize) ? true : false;

	$valItype = (!in_array($_FILES['izquierda']['type'], $types)) ? false : true;
	$valIsize = (($_FILES['izquierda']['size'] / 1000000) > 10) ? false : true;

	$valI = ($valItype && $valIsize) ? true : false;

	$valFtype = (!in_array($_FILES['adelante']['type'], $types)) ? false : true;
	$valFsize = (($_FILES['adelante']['size'] / 1000000) > 10) ? false : true;

	$valF = ($valFtype && $valFsize) ? true : false;

	$valBtype = (!in_array($_FILES['atras']['type'], $types)) ? false : true;
	$valBsize = (($_FILES['atras']['size'] / 1000000) > 10) ? false : true;

	$valB = ($valBtype && $valBsize) ? true : false;

	$valPics = [$valD, $valI, $valF, $valB];

	foreach ($valPics as $value) { array_push($validations, $value); }

	/**************************************************************/

    $detalle = $_SESSION['ultimoDetalleHoja'];

	$detalleHoja = [];

	foreach ($detalle['idItem'] as $value) {
		$detalleHoja[] = preg_replace('([^A-Za-z0-9 ])', '', trim($_POST['item_'.$value]));
	}

	$valDH = true;

	foreach ($detalleHoja as $value) { if (empty($value)) { $valDH = false; break; } }

	array_push($validations, $valDH);

	/**************************************************************/

	$kmFinal = preg_replace('([^A-Za-z0-9 ])', '', trim(round($_POST['kmFinal'])));
	$valkmFinal = (empty($kmFinal)) ? false : true;

	array_push($validations, $valkmFinal);

	$hojaObserv = preg_replace('([^A-Za-z0-9 ])', '', trim($_POST['hojaObserv']));

	$nivelGas = preg_replace('([^A-Za-z0-9 ])', '', trim($_POST['nivelGas']));

	switch ($nivelGas) {
		case '1': $nivelGas = "1/4 de tanque"; break;
		case '2': $nivelGas = "2/4 de tanque"; break;
		case '3': $nivelGas = "3/4 de tanque"; break;
		case '4': $nivelGas = "Tanque lleno"; break;
		default: $nivelGas = ""; break;
	}

	$valGas = (empty($nivelGas)) ? false : true;

	array_push($validations, $valGas);

	/**************************************************************/

	$datos = $model->infoBitacora($_SESSION['log']['id']);
	$valKmInicial = ($datos['kmInicial'] > $kmFinal) ? false : true;

	array_push($validations, $valKmInicial);

	/**************************************************************/

	if (!in_array(false, $validations))
    {
    	$info_pictures = [
    		'derecha' => $_FILES['derecha'],
    		'izquierda' => $_FILES['izquierda'],
    		'adelante' => $_FILES['adelante'],
    		'atras' => $_FILES['atras']
    	];

		$bitacora = [
			'kmFinal' => $kmFinal,
			'hojaObserv' => $hojaObserv,
			'nivelGas' => $nivelGas,
			'detalleHoja' => $detalleHoja,
			'info_pictures' => $info_pictures,
			'horaEntrada' => $_POST['horaentrada']
		];

		$objHome->cerrarBitacora($bitacora);
	}
	else
	{
		$_SESSION['sweetAlert']['icon'] = 'error';
		$_SESSION['sweetAlert']['text'] = 'Uno o varios de los datos ingresados no son válidos.';

		header("Location: ".URL);
	}
}

if (isset($_POST['newLicense']) || isset($_POST['updateLicense']))
{
	$validacion = true;

	$dui = preg_replace('([^A-Za-z0-9- ])', '', trim($_POST['dui']));
	$nit = preg_replace('([^A-Za-z0-9- ])', '', trim($_POST['nit']));
	$fechaExp = preg_replace('([^A-Za-z0-9- ])', '', trim($_POST['fechaExp']));
	$fechaVenc = preg_replace('([^A-Za-z0-9- ])', '', trim($_POST['fechaVenc']));
	$tipo = preg_replace('([^A-Za-z0-9 ])', '', trim($_POST['type']));

	$licencia = [
		'dui' => $dui,
		'nit' => $nit,
		'fechaExp' => $fechaExp,
		'fechaVenc' => $fechaVenc,
		'tipo' => $tipo
	];

	foreach ($licencia as $value) {
		if (empty($value)) { $validacion = false; break; }
	}

	if ($validacion) {
		if (isset($_POST['newLicense']))
			$objHome->newLicense($licencia);
		else
			$objHome->updateLicense($licencia, $_POST['updateLicense']);
	}else {
		$_SESSION['sweetAlert']['icon'] = "error";
		$_SESSION['sweetAlert']['text'] = "Uno o varios de los datos no son válidos.";
	}
}

if (isset($_GET['delLicense'])) {
	$objHome->deleteLicense(preg_replace('([^A-Za-z0-9- ])', '', trim($_GET['delLicense'])));
}

if (isset($_POST['newComment']))
{
	$model->insertComment(preg_replace('([^A-Za-zÁ-ź0-9-.¡!:\) ])', '', trim($_POST['comment'])), $_SESSION['log']['id']);

	header("Location: ".URL);
}

if (isset($_GET['delComment']))
{
	$model->delComment(preg_replace('([^A-Za-z0-9- ])', '', trim($_GET['delComment'])), $_SESSION['log']['id']);

	header("Location: ".URL);
}

if (isset($_POST['newAuto']))
{
	$validations = [];

	$types = ['image/jpeg', 'image/jpg', 'image/png'];

	$valDtype = (!in_array($_FILES['derecha']['type'], $types)) ? false : true;
	$valDsize = (($_FILES['derecha']['size'] / 1000000) > 10) ? false : true;

	$valD = ($valDtype && $valDsize) ? true : false;

	$valItype = (!in_array($_FILES['izquierda']['type'], $types)) ? false : true;
	$valIsize = (($_FILES['izquierda']['size'] / 1000000) > 10) ? false : true;

	$valI = ($valItype && $valIsize) ? true : false;

	$valFtype = (!in_array($_FILES['adelante']['type'], $types)) ? false : true;
	$valFsize = (($_FILES['adelante']['size'] / 1000000) > 10) ? false : true;

	$valF = ($valFtype && $valFsize) ? true : false;

	$valBtype = (!in_array($_FILES['atras']['type'], $types)) ? false : true;
	$valBsize = (($_FILES['atras']['size'] / 1000000) > 10) ? false : true;

	$valB = ($valBtype && $valBsize) ? true : false;

	$valPics = [$valD, $valI, $valF, $valB];

	foreach ($valPics as $value) { array_push($validations, $value); }

	if (!in_array(false, $valPics))
    {
    	$info_pictures = [
    		'derecha' => $_FILES['derecha'],
    		'izquierda' => $_FILES['izquierda'],
    		'adelante' => $_FILES['adelante'],
    		'atras' => $_FILES['atras']
    	];

		$info_auto = [
			'placa' => preg_replace('([^A-Za-z0-9- ])', '', trim($_POST['placa'])),
			'color' => preg_replace('([^A-Za-z0-9 ])', '', trim($_POST['color'])),
			'marca' => preg_replace('([^A-Za-z0-9 ])', '', trim($_POST['marca'])),
			'modelo' => preg_replace('([^A-Za-z0-9 ])', '', trim($_POST['modelo'])),
			'anno' => preg_replace('([^A-Za-z0-9 ])', '', trim($_POST['anno'])),
			'motor' => preg_replace('([^A-Za-z0-9 ])', '', trim($_POST['motor'])),
			'chasis' => preg_replace('([^A-Za-z0-9 ])', '', trim($_POST['chasis'])),
			'vin' => preg_replace('([^A-Za-z0-9 ])', '', trim($_POST['vin'])),
			'proveedor' => preg_replace('([^A-Za-z0-9 ])', '', trim($_POST['proveedor'])),
			'region' => preg_replace('([^A-Za-z0-9 ])', '', trim($_POST['region'])),
			'mtto' => preg_replace('([^A-Za-z0-9 ])', '', trim($_POST['mtto'])),
			'km' => preg_replace('([^A-Za-z0-9 ])', '', trim($_POST['km'])),
			'tipo' => preg_replace('([^A-Za-z0-9 ])', '', trim($_POST['tipo']))
		];

		$items = $model->autoItemsList();

		foreach ($items['id'] as $key => $value) {
			if (isset($_POST[$value])) { $info_hoja[] = $value; }
		}

		if ($info_auto['mtto'] <= $info_auto['km'])
			$info_auto['mtto'] = $info_auto['km'] + 3000;

		$objHome->newAuto($info_auto, $info_hoja, $info_pictures);

    }
    else
    {
    	$_SESSION['sweetAlert']['icon'] = 'error';
    	$_SESSION['sweetAlert']['text'] = 'El tamaño de una de las imagenes sobrepasa al valor máximo admitido, por favor verifique los datos.';
    }
}

if (isset($_POST['updatePics']))
{
	$validations = [];

	$types = ['image/jpeg', 'image/jpg', 'image/png'];

	$valDtype = (!in_array($_FILES['derecha']['type'], $types)) ? false : true;
	$valDsize = (($_FILES['derecha']['size'] / 1000000) > 10) ? false : true;

	$valD = ($valDtype && $valDsize) ? true : false;

	$valItype = (!in_array($_FILES['izquierda']['type'], $types)) ? false : true;
	$valIsize = (($_FILES['izquierda']['size'] / 1000000) > 10) ? false : true;

	$valI = ($valItype && $valIsize) ? true : false;

	$valFtype = (!in_array($_FILES['adelante']['type'], $types)) ? false : true;
	$valFsize = (($_FILES['adelante']['size'] / 1000000) > 10) ? false : true;

	$valF = ($valFtype && $valFsize) ? true : false;

	$valBtype = (!in_array($_FILES['atras']['type'], $types)) ? false : true;
	$valBsize = (($_FILES['atras']['size'] / 1000000) > 10) ? false : true;

	$valB = ($valBtype && $valBsize) ? true : false;

	$valPics = [$valD, $valI, $valF, $valB];

	foreach ($valPics as $value) { array_push($validations, $value); }

	if (!in_array(false, $valPics))
    {
    	$info_pictures = [
    		'derecha' => $_FILES['derecha'],
    		'izquierda' => $_FILES['izquierda'],
    		'adelante' => $_FILES['adelante'],
    		'atras' => $_FILES['atras']
    	];

	    $objHome->updateCarsPics($_POST['updatePics'], $info_pictures); //updatePics = idHoja
	}
	else
    {
    	$_SESSION['sweetAlert']['icon'] = 'error';
    	$_SESSION['sweetAlert']['text'] = 'El tamaño de una de las imagenes sobrepasa al valor máximo admitido, por favor verifique los datos.';
    }
}
