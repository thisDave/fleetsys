<?php

require_once APP.'/models/Administrador.php';

class AdminController extends Administrador
{
    public function events($event, $value = '')
	{
		switch ($event)
		{
			default:
				$this->$event($value);
			break;
		}

		header("Location: ".URL);
	}

	public function getKey($length)
	{
	    $cadena = "ABCDFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";
	    $longitudCadena = strlen($cadena);
	    $pass = "";
	    for($i=1 ; $i<=$length ; $i++){
	        $pos=rand(0,$longitudCadena-1);
	        $pass .= substr($cadena,$pos,1);
	    }
	    return $pass;
	}

	public function delProvider($id)
	{
		if (parent::showRes("DELETE FROM tbl_proveedores WHERE idProveedor = ".$id))
		{
			$_SESSION['sweetAlert']['icon'] = 'success';
			$_SESSION['sweetAlert']['text'] = 'Proveedor eliminado exitosamente.';
			$_SESSION['view'] = 'providers';
		}
		else
		{
			$_SESSION['sweetAlert']['icon'] = 'error';
			$_SESSION['sweetAlert']['text'] = 'El proveedor posee datos relacionados.';
		}

		header("Location: ".URL);
	}

	public function updateProvider($nombre, $contacto, $telefono, $idProveedor)
	{
		$nombre = ltrim(rtrim($nombre));
		$contacto = ltrim(rtrim($contacto));
		$telefono = ltrim(rtrim($telefono));

		if (!empty($nombre) && !empty($contacto) && !empty($telefono))
		{
			if (parent::actualizarProveedor($nombre, $contacto, $telefono, $idProveedor))
			{
				$_SESSION['sweetAlert']['icon'] = 'success';
				$_SESSION['sweetAlert']['text'] = 'Información actualizada exitosamente.';
			}
			else
			{
				$_SESSION['sweetAlert']['icon'] = 'error';
				$_SESSION['sweetAlert']['text'] = 'No fue posible conectarse a la base de datos.';
			}
		}
		else
		{
			$_SESSION['sweetAlert']['icon'] = 'error';
			$_SESSION['sweetAlert']['text'] = 'Ingrese datos válidos';
		}

		header("Location: ".URL);
	}

	public function newProvider($nombre, $tipo, $contacto, $telefono, $pais)
	{
		$datos = [trim($nombre), trim($tipo), trim($contacto), trim($telefono)];

		$x = parent::validata($datos);

		if ($x)
		{
			if (parent::ingresarProveedor($datos, $pais))
			{
				$_SESSION['sweetAlert']['icon'] = 'success';
				$_SESSION['sweetAlert']['text'] = 'proveedor ingresado con éxito.';
			}
			else
			{
				$_SESSION['sweetAlert']['icon'] = 'error';
				$_SESSION['sweetAlert']['text'] = 'El nombre del proveedor ya existe.';
			}
		}
		else
		{
			$_SESSION['sweetAlert']['icon'] = 'error';
			$_SESSION['sweetAlert']['text'] = 'Ingrese datos válidos';
		}

		header("Location: ".URL);
	}

	public function updateAuto($datos)
	{
		foreach ($datos as $key => $value) { $datos[$key] = preg_replace('([^A-Za-z0-9 ])', '', trim($value)); }

		$x = true;

		$datos['km'] = ($datos['km'] == 0) ? 'cero' : $datos['km'];
		$datos['kmetraje'] = ($datos['kmetraje'] == 0) ? 'cero' : $datos['kmetraje'];

		foreach ($datos as $key => $value) { if (empty($value)) { $x = false; break; } }

		$datos['km'] = ($datos['km'] == 'cero') ? 0 : $datos['km'];
		$datos['kmetraje'] = ($datos['kmetraje'] == 'cero') ? 0 : $datos['kmetraje'];

		if ($x)
		{
			if (parent::validarDatosAuto($datos['placa'], $datos['motor'], $datos['chasis'], $datos['id']))
			{
				if (parent::actualizarAuto($datos)){
					$_SESSION['sweetAlert']['icon'] = 'success';
					$_SESSION['sweetAlert']['text'] = 'Los datos fueron actualizados exitosamente.';
				}
				else {
					$_SESSION['sweetAlert']['icon'] = 'error';
					$_SESSION['sweetAlert']['text'] = 'No fue posible comunicarse con la base de datos.';
				}
			}
			else
			{
				$_SESSION['sweetAlert']['icon'] = 'error';
				$_SESSION['sweetAlert']['text'] = 'Existen datos duplicados con el vehículo.';
			}
		}
		else
		{
			$_SESSION['sweetAlert']['icon'] = 'error';
			$_SESSION['sweetAlert']['text'] = 'Falta de información requerida.';
		}

		header("Location: ".URL);
	}

	public function delAutoProfile($idAuto)
	{
		$idAuto = trim($idAuto);

		if (!empty($idAuto))
		{
			if (parent::delAuto($idAuto))
			{
				header("Location: ".URL."?req=autos&val=cars");
			}
			else
			{
				$_SESSION['sweetAlert']['icon'] = 'error';
				$_SESSION['sweetAlert']['text'] = 'No fue posible eliminar este vehículo.';

				header("Location: ".URL);
			}
		}
		else
		{
			$_SESSION['sweetAlert']['icon'] = 'error';
			$_SESSION['sweetAlert']['text'] = 'Por favor ingrese datos válidos.';

			header("Location: ".URL);
		}
	}

	public function newProduct($nombre, $categoria)
	{
		$nombre = trim($nombre);

		if (!empty($nombre))
		{
			if (parent::insertProduct($nombre, $categoria))
			{
				$_SESSION['sweetAlert']['icon'] = 'success';
				$_SESSION['sweetAlert']['text'] = 'Producto ingresado exitosamente.';
			}
			else
			{
				$_SESSION['sweetAlert']['icon'] = 'error';
				$_SESSION['sweetAlert']['text'] = 'El producto ingresado ya existe.';
			}
		}
		else
		{
			$_SESSION['sweetAlert']['icon'] = 'error';
			$_SESSION['sweetAlert']['text'] = 'Ingrese datos válidos.';
		}
	}

	public function deleteProd($id)
	{
		if (parent::delProd($id))
		{
			unset($_SESSION['editProd']);

			$_SESSION['sweetAlert']['icon'] = 'success';
			$_SESSION['sweetAlert']['text'] = 'Producto eliminado exitosamente.';
		}
		else
		{
			$_SESSION['sweetAlert']['icon'] = 'error';
			$_SESSION['sweetAlert']['text'] = 'El producto posee registros relacionados o no existe.';
		}

		header("Location: ".URL);
	}

	public function delCategory($id)
	{
		if (parent::deleteCategory($id))
		{
			unset($_SESSION['editCategory']);

			$_SESSION['sweetAlert']['icon'] = 'success';
			$_SESSION['sweetAlert']['text'] = 'Categoría eliminada exitosamente.';
		}
		else
		{
			$_SESSION['sweetAlert']['icon'] = 'error';
			$_SESSION['sweetAlert']['text'] = 'La categoría posee registros relacionados o no existe.';
		}

		header("Location: ".URL);
	}

	public function backCatalog()
	{
		unset($_SESSION['editProd']); header("Location: ".URL);
	}

	public function backActions()
	{
		unset($_SESSION['editAction']); header("Location: ".URL);
	}

	public function backCategories()
	{
		unset($_SESSION['editCategory']); header("Location: ".URL);
	}

	public function newCategory($categoria)
	{
		$categoria = trim($categoria);

		if (!empty($categoria))
		{
			if (parent::insertCategory($categoria))
			{
				$_SESSION['sweetAlert']['icon'] = 'success' ;
				$_SESSION['sweetAlert']['text'] = 'La categoría fue ingresada exitosamente.';
			}
			else
			{
				$_SESSION['sweetAlert']['icon'] = 'error' ;
				$_SESSION['sweetAlert']['text'] = 'La categoría ingresada ya existe en el catálogo.';
			}
		}
		else
		{
			$_SESSION['sweetAlert']['icon'] = 'error';
			$_SESSION['sweetAlert']['text'] = 'Por favor escriba un nombre para su categoría';
		}
	}

	public function delProd($val)
	{
		unset($_SESSION['productos']['id'][$val]);
		unset($_SESSION['productos']['cantidad'][$val]);
		unset($_SESSION['productos']['producto'][$val]);
		unset($_SESSION['productos']['punitario'][$val]);
		unset($_SESSION['productos']['totalProd'][$val]);

		header("Location: ".URL);
	}

	public function updateProd($nombre, $categoria, $idProducto)
	{
		$nombre = trim($nombre);
		$categoria = trim($categoria);
		$idProducto = trim($idProducto);

		if (parent::updateProd($nombre, $categoria, $idProducto)) {
			$_SESSION['sweetAlert']['icon'] = 'success';
			$_SESSION['sweetAlert']['text'] = 'Producto actualizado exitosamente.';
		}else {
			$_SESSION['sweetAlert']['icon'] = 'error';
			$_SESSION['sweetAlert']['text'] = 'El producto ya existe.';
		}

		header("Location: ".URL);
	}

	public function updateCategory($nombre, $idCategoria)
	{
		$nombre = trim($nombre);
		$idCategoria = trim($idCategoria);

		if (!empty($nombre) && !empty($idCategoria))
		{
			if (parent::updtCategory($nombre, $idCategoria)) {
				$_SESSION['sweetAlert']['icon'] = 'success';
				$_SESSION['sweetAlert']['text'] = 'Categoría actualizada exitosamente.';
			} else {
				$_SESSION['sweetAlert']['icon'] = 'error';
				$_SESSION['sweetAlert']['text'] = 'La categoría ingresada ya existe en el catálogo.';
			}
		}
		else
		{
			$_SESSION['sweetAlert']['icon'] = 'error';
			$_SESSION['sweetAlert']['text'] = 'Por favor ingrese datos válidos.';
		}

		header("Location: ".URL);
	}

	public function delVerify()
	{
		unset($_SESSION['dataVerificada']);

		header("Location: ".URL);
	}

	public function saveBill_compras()
	{
		$idCompra = parent::insertarCompra($_SESSION['factura'], $_SESSION['total-factura'], $_SESSION['proveedor'], $_SESSION['log']['id']);

		if ($idCompra)
		{
			$_SESSION['sweetAlert']['icon'] = 'success';
			$_SESSION['sweetAlert']['text'] = 'Factura ingresada exitosamente.';

			foreach ($_SESSION['productos']['id'] as $key => $value)
			{
				$idProducto = $_SESSION['productos']['producto'][$key];
				$cantidad = $_SESSION['productos']['cantidad'][$key];
				$punitario = $_SESSION['productos']['punitario'][$key];
				$total = $_SESSION['productos']['totalProd'][$key];

				if (!parent::insertarDetalleCompra($idProducto, $cantidad, $punitario, $total, $idCompra))
				{
					$_SESSION['sweetAlert']['icon'] = 'error';
					$_SESSION['sweetAlert']['text'] = 'No se pudo guardar el detalle de la factura.';

					break;
				}
			}

			if ($_SESSION['sweetAlert']['icon'] == 'success')
			{
				foreach ($_SESSION['productos']['id'] as $key => $value)
				{
					$idProducto = $_SESSION['productos']['producto'][$key];
					$cantidad = $_SESSION['productos']['cantidad'][$key];
					$punitario = $_SESSION['productos']['punitario'][$key];
					$numFactura = $_SESSION['factura'];

					if (!parent::insertarInventario($idProducto, $cantidad, $numFactura, $punitario))
					{
						$_SESSION['sweetAlert']['icon'] = 'error';
						$_SESSION['sweetAlert']['text'] = 'No se pudo actualzar el inventario.';

						break;
					}
				}

				if ($_SESSION['sweetAlert']['icon'] == 'success') { $this->cancelEntry(); }else { header("Location: ".URL); }
			}
			else
			{
				header("Location: ".URL);
			}
		}
		else
		{
			$_SESSION['sweetAlert']['icon'] = 'error';
			$_SESSION['sweetAlert']['text'] = 'No fue posible guardar el ingreso, por favor contactese con el administrador del sistema.';
		}
	}

	public function saveBill()
	{
		$_SESSION['nfact'][] = $_SESSION['factura'];
		$_SESSION['tfact'][] = $_SESSION['total-factura'];
		$_SESSION['idprov'][] = $_SESSION['proveedor'];
		$_SESSION['prods'][] = $_SESSION['productos'];

		unset($_SESSION['ingresoProductos']);
		unset($_SESSION['factura']);
		unset($_SESSION['total-factura']);
		unset($_SESSION['proveedor']);
		unset($_SESSION['productos']);
		unset($_SESSION['subtotal']);
		unset($_SESSION['dataVerificada']);

		$_SESSION['view'] = 'preordenProfile';
		$_SESSION['val'] = $_SESSION['preordenAbierta'];

		header("Location: ".URL);
	}

	public function delBill($index)
	{
		unset($_SESSION['nfact'][$index]);
		unset($_SESSION['tfact'][$index]);
		unset($_SESSION['idprov'][$index]);
		unset($_SESSION['prods'][$index]);

		if (empty($_SESSION['nfact']))
		{
			unset($_SESSION['nfact']);
			unset($_SESSION['tfact']);
			unset($_SESSION['idprov']);
			unset($_SESSION['prods']);
		}

		header("Location: ".URL);
	}

	public function editBill($index)
	{
		$_SESSION['factura'] = $_SESSION['nfact'][$index];
		$_SESSION['total-factura'] = $_SESSION['tfact'][$index];
		$_SESSION['proveedor'] = $_SESSION['idprov'][$index];
		$_SESSION['productos'] = $_SESSION['prods'][$index];
		$_SESSION['subtotal'] = $_SESSION['total-factura'][$index];
		$_SESSION['ingresoProductos'] = true;
		$_SESSION['dataVerificada'] = true;
		$_SESSION['view'] = 'ingresos';
		$_SESSION['val'] = $index;
		$_SESSION['editBill'] = true;

		header("Location: ".URL);
	}

	public function editBillEntry($index)
	{
		$_SESSION['nfact'][$index] = $_SESSION['factura'];
		$_SESSION['tfact'][$index] = $_SESSION['total-factura'];
		$_SESSION['idprov'][$index] = $_SESSION['proveedor'];
		$_SESSION['prods'][$index] = $_SESSION['productos'];

		unset($_SESSION['ingresoProductos']);
		unset($_SESSION['factura']);
		unset($_SESSION['total-factura']);
		unset($_SESSION['proveedor']);
		unset($_SESSION['productos']);
		unset($_SESSION['subtotal']);
		unset($_SESSION['dataVerificada']);
		unset($_SESSION['editBill']);

		$_SESSION['view'] = 'preordenProfile';
		$_SESSION['val'] = $_SESSION['preordenAbierta'];

		header("Location: ".URL);
	}

	public function cancelEntry()
	{
		unset($_SESSION['ingresoProductos']);
		unset($_SESSION['factura']);
		unset($_SESSION['total-factura']);
		unset($_SESSION['proveedor']);
		unset($_SESSION['productos']);
		unset($_SESSION['subtotal']);
		unset($_SESSION['dataVerificada']);

		$_SESSION['view'] = 'preordenProfile';
		$_SESSION['val'] = $_SESSION['preordenAbierta'];

		header("Location: ".URL);
	}

	public function saveProdEntry()
	{
		if (isset($_SESSION['nfact']) && in_array('0000', $_SESSION['nfact']))
		{
			foreach ($_SESSION['nfact'] as $key => $value) {
				if ($value == '0000') {
					$index = $key;
					break;
				}
			}

			if (empty($_SESSION['prods'][$index]['id']))
			{
				$_SESSION['prods'][$index]['id'] = $_SESSION['productos']['id'];
				$_SESSION['prods'][$index]['cantidad'] = $_SESSION['productos']['cantidad'];
				$_SESSION['prods'][$index]['producto'] = $_SESSION['productos']['producto'];
				$_SESSION['prods'][$index]['punitario'] = $_SESSION['productos']['punitario'];
				$_SESSION['prods'][$index]['totalProd'] = $_SESSION['productos']['totalProd'];
			}
			else
			{
				$_SESSION['prods'][$index]['id'] = array_merge($_SESSION['prods'][$index]['id'], $_SESSION['productos']['id']);
				$_SESSION['prods'][$index]['cantidad'] = array_merge($_SESSION['prods'][$index]['cantidad'], $_SESSION['productos']['cantidad']);
				$_SESSION['prods'][$index]['producto'] = array_merge($_SESSION['prods'][$index]['producto'], $_SESSION['productos']['producto']);
				$_SESSION['prods'][$index]['punitario'] = array_merge($_SESSION['prods'][$index]['punitario'], $_SESSION['productos']['punitario']);
				$_SESSION['prods'][$index]['totalProd'] = array_merge($_SESSION['prods'][$index]['totalProd'], $_SESSION['productos']['totalProd']);
			}
		}
		else
		{
			$_SESSION['nfact'][] = '0000';
			$_SESSION['tfact'][] = 0;
			$_SESSION['idprov'][] = 12;
			$_SESSION['prods'][] = $_SESSION['productos'];
		}

		unset($_SESSION['productos']);

		$_SESSION['view'] = 'preordenProfile';
		$_SESSION['val'] = $_SESSION['preordenAbierta'];

		header("Location: ".URL);
	}

	public function delProdEntry($vals)
	{
		$index = explode('_', $vals);
		$i = $index[0];
		$j = $index[1];
		unset($_SESSION['prods'][$i]['id'][$j]);
		unset($_SESSION['prods'][$i]['cantidad'][$j]);
		unset($_SESSION['prods'][$i]['producto'][$j]);
		unset($_SESSION['prods'][$i]['punitario'][$j]);
		unset($_SESSION['prods'][$i]['totalProd'][$j]);

		if (empty($_SESSION['prods'][$i]['id']))
		{
			unset($_SESSION['nfact'][$i]);
			unset($_SESSION['tfact'][$i]);
			unset($_SESSION['idprov'][$i]);
			unset($_SESSION['prods'][$i]);
		}
	}

	public function cancelprodEntry()
	{
		unset($_SESSION['productos']);

		$_SESSION['view'] = 'preordenProfile';
		$_SESSION['val'] = $_SESSION['preordenAbierta'];

		header("Location: ".URL);
	}

	public function newVoucher($voucher)
	{
		if (parent::insertVoucher($voucher))
		{
			$_SESSION['sweetAlert']['icon'] = 'success';
			$_SESSION['sweetAlert']['text'] = 'Vale registrado con éxito';
		}
		else
		{
			$_SESSION['sweetAlert']['icon'] = 'error';
			$_SESSION['sweetAlert']['text'] = 'Existen datos duplicados, por favor verifique.';
		}
	}

	public function autorizarPiloto($idUsuario)
	{
		if (parent::autorizarLicPiloto($idUsuario))
		{
			$_SESSION['sweetAlert']['icon'] = "success";
			$_SESSION['sweetAlert']['text'] = "Usuario autorizado!";
		}
		else
		{
			$_SESSION['sweetAlert']['icon'] = "error";
			$_SESSION['sweetAlert']['text'] = "No se pudo autorizar el usuario.";
		}
	}

	public function desautorizarPiloto($idUsuario)
	{
		if (parent::desautorizarLicPiloto($idUsuario))
		{
			$_SESSION['sweetAlert']['icon'] = "success";
			$_SESSION['sweetAlert']['text'] = "Usuario desautorizado!";
		}
		else
		{
			$_SESSION['sweetAlert']['icon'] = "error";
			$_SESSION['sweetAlert']['text'] = "No se pudo desautorizar el usuario.";
		}
	}

	public function newPreorder($datos)
	{
		$detallePreorden = $datos['detpre'];

		$flag = true;

		$flag = (empty($datos['tipoPreorden'])) ? false : true;
		$flag = (empty($datos['idVehiculo'])) ? false : true;
		$flag = (empty($datos['kilometraje'])) ? false : true;

		if ($flag)
		{
			$idPreorden = parent::insertarPreorden($datos);

			if ($idPreorden)
			{
				//INSERTANDO EL DETALLE DE LA PREORDEN

				$x = true;

				foreach ($detallePreorden as $value)
				{
					if (!parent::insertarDetallePreorden($idPreorden, $datos['tipoPreorden'], $value))
					{
						$x = false;
						parent::cleanPreorden($idPreorden);
						break;
					}
				}

				if ($x)
				{
					$this->enaNotify($datos['idVehiculo']);
					$_SESSION['sweetAlert']['icon'] = "success";
					$_SESSION['sweetAlert']['text'] = "La preorden fue ingresada con éxito.";
				}
				else
				{
					$_SESSION['sweetAlert']['icon'] = "error";
					$_SESSION['sweetAlert']['text'] = "No se pudo guardar la preorden.";
				}
			}
			else
			{
				$_SESSION['sweetAlert']['icon'] = "error";
				$_SESSION['sweetAlert']['text'] = "No se pudo guardar la preorden.";
			}
		}
		else
		{
			$_SESSION['sweetAlert']['icon'] = "error";
			$_SESSION['sweetAlert']['text'] = "Datos incompletos.";
		}
	}

	public function addDetalleMtto($idMtto, $prods, $nfact, $idprov)
	{
		$flag = true;

		foreach ($prods['id'] as $key => $value)
		{
			$idprod = $prods['producto'][$key];
			$cantidad = $prods['cantidad'][$key];
			$punitario = $prods['punitario'][$key];

			if (!parent::newDetalleMtto($idMtto, $idprod, $cantidad, $nfact, $punitario, $idprov))
			{
				$flag = false;
				break;
			}
		}

		return $flag;
	}

	public function enaNotify($idVehiculo)
	{
		parent::showRes("DELETE FROM tbl_notificaciones WHERE idVehiculo = {$idVehiculo}");
	}
}

$modelAdmin = new Administrador;

$objAdmin = new AdminController;

if (isset($_GET['event'])) {
	$objAdmin->events($_GET['event'], $_GET['val']);
}

if (isset($_POST['newProvider']))
{
	$pais = $model->getIdPais($user['pais']);

	$objAdmin->newProvider($_POST['nombre'], $_POST['tipo'], $_POST['contacto'], $_POST['telefono'], $pais);
}

if (isset($_POST['updateProvider']))
{
	$objAdmin->updateProvider($_POST['nombre'], $_POST['contacto'], $_POST['telefono'], $_POST['id']);
}

if (isset($_POST['updateAuto']))
{
	$kmetraje = (isset($_POST['kmetraje'])) ? $_POST['kmetraje'] : 0;

	$datos = [
		'placa' => $_POST['placa'],
		'color' => $_POST['color'],
		'marca' => $_POST['marca'],
		'modelo' => $_POST['modelo'],
		'anno' => $_POST['anno'],
		'motor' => $_POST['motor'],
		'chasis' => $_POST['chasis'],
		'vin' => $_POST['vin'],
		'proveedor' => $_POST['proveedor'],
		'region' => $_POST['region'],
		'km' => $_POST['km'],
		'tipo' => $_POST['tipo'],
		'id' => $_POST['id'],
		'kmetraje' => $kmetraje
	];

	$objAdmin->updateAuto($datos);
}

if (isset($_POST['delAutoProfile']))
{
	$objAdmin->delAutoProfile($_POST['delAutoProfile']);
}

if (isset($_POST['newProduct']))
{
	$objAdmin->newProduct($_POST['nombre'], $_POST['categoria']);
}

if (isset($_POST['newEntry']))
{
	$factura = trim($_POST['factura']);
	$total = trim($_POST['total']);
	$proveedor = trim($_POST['proveedor']);

	$datos = [$factura, $total, $proveedor];

	$x = true;

	foreach ($datos as $value) { if (empty($value)) { $x = false; break; } }

	if ($x)
	{
		$_SESSION['ingresoProductos'] = true;

		$_SESSION['factura'] = $factura;
		$_SESSION['total-factura'] = $total;
		$_SESSION['proveedor'] = $proveedor;
	}
	else
	{
		$_SESSION['alertResultOne'] = false;
	}

	if (isset($_SESSION['editBill']))
		unset($_SESSION['editBill']);

	header("Location: ".URL);
}

if (isset($_POST['updateEntry']))
{
	$factura = trim($_POST['factura']);
	$total = trim($_POST['total']);
	$proveedor = trim($_POST['proveedor']);

	$datos = [$factura, $total, $proveedor];

	$x = true;

	foreach ($datos as $value) { if (empty($value)) { $x = false; break; } }

	if ($x)
	{
		$_SESSION['factura'] = $factura;
		$_SESSION['total-factura'] = $total;
		$_SESSION['proveedor'] = $proveedor;
	}
	else
	{
		$_SESSION['alertResultOne'] = false;
	}

	header("Location: ".URL);
}

if (isset($_POST['addProd']))
{
	$cantidad = trim($_POST['cantidad']);
	$producto = trim($_POST['producto']);
	$punitario = trim($_POST['punitario']);

	$datos = [$cantidad, $producto, $punitario];

	$x = true;

	foreach ($datos as $value) { if (empty($value)) { $x = false; break; } }

	if ($x)
	{
		$_SESSION['productos']['id'][] = count($_SESSION['productos']['id']);
		$_SESSION['productos']['cantidad'][] = $cantidad;
		$_SESSION['productos']['producto'][] = $producto;
		$_SESSION['productos']['punitario'][] = $punitario;
		$_SESSION['productos']['totalProd'][] = $punitario * $cantidad;
	}
	else
	{
		$_SESSION['sweetAlert']['icon'] = 'error';
		$_SESSION['sweetAlert']['text'] = 'Ingrese datos válidos.';
	}

	$suma = 0;

	foreach ($_SESSION['productos']['totalProd'] as $Key => $value) { $suma += $value; }

	$_SESSION['subtotal'] = $suma;

	if ($_SESSION['subtotal'] > $_SESSION['total-factura'])
	{
		$_SESSION['sweetAlert']['icon'] = 'error';
		$_SESSION['sweetAlert']['text'] = 'El subtotal de productos no coincide con la totalidad de la factura.';
	}
	elseif ($_SESSION['subtotal'] == $_SESSION['total-factura'])
	{
		$_SESSION['sweetAlert']['icon'] = 'success';
		$_SESSION['sweetAlert']['text'] = 'Datos verificados exitosamente.';

		$_SESSION['dataVerificada'] = true;
	}


	header("Location: ".URL);
}

if (isset($_POST['addProdEntry']))
{
	$cantidad = trim($_POST['cantidad']);
	$producto = trim($_POST['producto']);
	$punitario = trim($_POST['punitario']);

	$datos = [$cantidad, $producto, $punitario];

	$x = true;

	foreach ($datos as $value) { if (empty($value)) { $x = false; break; } }

	if ($x)
	{
		$_SESSION['productos']['id'][] = count($_SESSION['productos']['id']);
		$_SESSION['productos']['cantidad'][] = $cantidad;
		$_SESSION['productos']['producto'][] = $producto;
		$_SESSION['productos']['punitario'][] = $punitario;
		$_SESSION['productos']['totalProd'][] = $punitario * $cantidad;
	}
	else
	{
		$_SESSION['sweetAlert']['icon'] = 'error';
		$_SESSION['sweetAlert']['text'] = 'Ingrese datos válidos.';
	}

	header("Location: ".URL);
}

if (isset($_GET['editProd']))
{
	$_SESSION['editProd'] = $_GET['editProd'];

	header("Location: ".URL);
}

if (isset($_GET['editCategory']))
{
	$_SESSION['editCategory'] = $_GET['editCategory'];

	header("Location: ".URL);
}

if (isset($_POST['updateProd']))
{
	$objAdmin->updateProd($_POST['producto'], $_POST['categoria'], $_POST['updateProd']);
}

if (isset($_POST['updateCategory']))
{
	$objAdmin->updateCategory($_POST['categoria'], $_POST['updateCategory']);
}

if (isset($_GET['startGas']))
{
	$val = strtolower(preg_replace('([^A-Za-z0-9 ])', '', trim($_GET['startGas'])));

	if ($val == 'vouchers' || $val == 'invoices') { $_SESSION['startGas']['tipo'] = $val; }

	header("Location: ".URL);
}

if (isset($_POST['gasProvider']))
{
	$_SESSION['startGas']['proveedor'] = preg_replace('([^A-Za-z0-9 ])', '', trim($_POST['provider']));

	header("Location: ".URL);
}

if (isset($_GET['endGas'])) { unset($_SESSION['startGas']); header("Location: ".URL); }

if (isset($_POST['newVoucher']))
{
	$voucher = [
		'fecha' => preg_replace('([^A-Za-z0-9- ])', '', trim($_POST['fecha'])),
		'nIngreso' => preg_replace('([^A-Za-z0-9 ])', '', trim($_POST['nIngreso'])),
		'auto' => preg_replace('([^A-Za-z0-9 ])', '', trim($_POST['auto'])),
		'valor' => preg_replace('([^A-Za-z0-9. ])', '', trim($_POST['valor'])),
		'kmSalida' => preg_replace('([^A-Za-z0-9 ])', '', trim($_POST['kmSalida'])),
		'kmEntrada' => preg_replace('([^A-Za-z0-9 ])', '', trim($_POST['kmEntrada'])),
		'idUsuario' => preg_replace('([^A-Za-z0-9 ])', '', trim($_SESSION['idUsuario']))
	];

	$val = true;

	foreach ($voucher as $value)
	{
		if (empty($voucher)) { $val = false; break; }
	}

	if ($val)
	{
		$objAdmin->newVoucher($voucher);
	}
	else
	{
		$_SESSION['sweetAlert']['icon'] = 'error';
		$_SESSION['sweetAlert']['text'] = 'Datos insuficientes, verifica los datos ingresados.';

		header("Location: ".URL);
	}
}

if (isset($_GET['autorizarPiloto']))
{
	$objAdmin->autorizarPiloto($_GET['autorizarPiloto']);
}

if (isset($_GET['desautorizarPiloto']))
{
	$objAdmin->desautorizarPiloto($_GET['desautorizarPiloto']);
}

if (isset($_POST['iniciarPreorden']))
{
	$_SESSION['inicioPreorden'] = true;

	$_SESSION['tipoPreorden'] = preg_replace('([^A-Za-z0-9 ])', '', trim($_POST['tipo']));

	header("Location: ".URL);
}

if (isset($_POST['addAction']))
{
	$acciones = $model->listarAccionesMant();

	$i = preg_replace('([^0-9])', '', trim($_POST['accion']));

	if (isset($_SESSION['mmtos']))
	{
		$x = true;

		foreach ($_SESSION['mmtos']['id'] as $value)
		{
			if ($value == $acciones['idAccion'][$i])
			{
				$x = false;
				break;
			}
		}

		if ($x)
		{
			$_SESSION['mmtos']['id'][] = $acciones['idAccion'][$i];
			$_SESSION['mmtos']['accion'][] = $acciones['accion'][$i];
		}
	}
	else
	{
		$_SESSION['mmtos']['id'][] = $acciones['idAccion'][$i];
		$_SESSION['mmtos']['accion'][] = $acciones['accion'][$i];
	}

	header("Location: ".URL);
}

if (isset($_GET['delAction']))
{
	$i = $_GET['val'];
	unset($_SESSION['mmtos']['id'][$i]);
	unset($_SESSION['mmtos']['accion'][$i]);

	header("Location: ".URL);
}

if (isset($_GET['cancelPreorden']))
{
	unset($_SESSION['inicioPreorden']);
	unset($_SESSION['tipoPreorden']);
	unset($_SESSION['mmtos']);
	unset($_SESSION['comentarios']);
	unset($_SESSION['placa']);
	unset($_SESSION['kmActual']);

	if (isset($_SESSION['val'])) { unset($_SESSION['val']); }

	header("Location: ".URL);
}

if (isset($_GET['newPreorder']))
{
	if (isset($_SESSION['mmtos']))
	{
		$comentarios = isset($_SESSION['comentarios']) ? trim($_SESSION['comentarios']) : '';

		$datos =[
			'tipoPreorden' => preg_replace('([^A-Za-z0-9 ])', '', trim($_SESSION['tipoPreorden'])),
			'idVehiculo' => preg_replace('([^A-Za-z0-9- ])', '', $model->getIdCar(trim($_SESSION['placa']))),
			'kilometraje' => preg_replace('([^A-Za-z0-9 ])', '', trim($_SESSION['kmActual'])),
			'comentarios' => preg_replace('([^A-Za-zÁ-ź0-9 ])', '', $comentarios),
			'detpre' => $_SESSION['mmtos']['accion']
		];

		unset($_SESSION['inicioPreorden']);
		unset($_SESSION['tipoPreorden']);
		unset($_SESSION['placa']);
		unset($_SESSION['kmActual']);
		unset($_SESSION['comentarios']);
		unset($_SESSION['mmtos']);
		if (isset($_SESSION['val'])) { unset($_SESSION['val']); }

		$objAdmin->newPreorder($datos);
	}
	else
	{
		$_SESSION['sweetAlert']['icon'] = "error";
		$_SESSION['sweetAlert']['text'] = "No hay acciones registradas.";
	}
}

if (isset($_GET['openPreorder']))
{
	$_SESSION['preordenAbierta'] = $_GET['openPreorder'];
	header("Location: ".URL);
}

if (isset($_GET['cancelarPreorden']))
{
	unset($_SESSION['preordenAbierta']);

	unset($_SESSION['comentPreorden']);

	// Datos de la factura

	unset($_SESSION['nfact']);
	unset($_SESSION['tfact']);
	unset($_SESSION['idprov']);
	unset($_SESSION['prods']);

	unset($_SESSION['ingresoProductos']);
	unset($_SESSION['factura']);
	unset($_SESSION['total-factura']);
	unset($_SESSION['proveedor']);
	unset($_SESSION['productos']);
	unset($_SESSION['subtotal']);
	unset($_SESSION['dataVerificada']);

	header("Location: ".URL."/?request=preordenes");
}

if (isset($_GET['endPreorder']))
{
	$preorden = $modelAdmin->infoPreorden($_SESSION['preordenAbierta']);

	$comentGeneral = (isset($_SESSION['comentPreorden'])) ? $_SESSION['comentPreorden'] : '';

	$comentGeneral = preg_replace('([^A-Za-zÁ-ź0-9 ])', '', $comentGeneral);

	$proxKm = $preorden['kilometraje'] + 3000;

	$idMtto = $modelAdmin->newMtto($preorden['idPreorden'], $_SESSION['log']['id'], $comentGeneral, $preorden['tipoPreorden'], $proxKm, $preorden['idVehiculo']);

	if ($idMtto)
	{
		$flag = true;

		foreach ($_SESSION['nfact'] as $key => $value)
		{
			$prods = $_SESSION['prods'][$key];
			$idprov = $_SESSION['idprov'][$key];
			$nfact = preg_replace('([^0-9 ])', '', $value);
			if (!$objAdmin->addDetalleMtto($idMtto, $prods, $nfact, $idprov)) {
				$flag = false;
				break;
			}
		}

		if ($flag)
		{
			unset($_SESSION['preordenAbierta']);
			unset($_SESSION['totalAcciones']);
			unset($_SESSION['comentPreorden']);

			unset($_SESSION['nfact']);
			unset($_SESSION['tfact']);
			unset($_SESSION['idprov']);
			unset($_SESSION['prods']);

			$_SESSION['view'] = 'preordenes';

			header("Location: ".URL);
		}
		else
		{
			$_SESSION['sweetAlert']['icon'] = "error";
			$_SESSION['sweetAlert']['text'] = "Datos incompletos.";
		}
	}
	else
	{
		$_SESSION['sweetAlert']['icon'] = 'error';
		$_SESSION['sweetAlert']['text'] = 'No fue posible actualizar la preorden.';
	}
}

if (isset($_POST['sendAlert']))
{
	$rango = preg_replace('([^0-9 ])', '', trim($_POST['rango']));
	$alerta = preg_replace('([^0-9 ])', '', trim($_POST['alerta']));

	if ($modelAdmin->updateAlert($rango, $alerta))
	{
		$_SESSION['sweetAlert']['icon'] = "success";
		$_SESSION['sweetAlert']['text'] = "Datos actualizados exitosamente.";
	}
	else
	{
		$_SESSION['sweetAlert']['icon'] = "error";
		$_SESSION['sweetAlert']['text'] = "No se pudo actualizar la alerta.";
	}
	header("Location: ".URL);
}

if (isset($_GET['dntshwntf']))
{
	$_SESSION['notificaciones'] = false;
	header("Location: ".URL);
}

if (isset($_GET['preordenes']))
{
	if (isset($_SESSION['preordenAbierta'])){
		header("Location: ".URL."?req=preordenProfile&val=".$_SESSION['preordenAbierta']);
	}
	else{
		header("Location: ".URL."?request=preordenes");
	}
}

if (isset($_GET['editAction']))
{
	$_SESSION['editAction'] = $_GET['editAction'];

	header("Location: ".URL);
}

if (isset($_POST['newAction']))
{
	$accion = preg_replace('([^A-Za-zÁ-ź0-9 ])', '', $_POST['action']);

	if ($modelAdmin->addAction($accion))
    {
        $_SESSION['sweetAlert']['icon'] = 'success';
        $_SESSION['sweetAlert']['text'] = 'Acción ingresada exitosamente.';
    }
    else
    {
        $_SESSION['sweetAlert']['icon'] = 'error';
        $_SESSION['sweetAlert']['text'] = 'No se pudo ingresar la acción.';
    }

    header("Location: ".URL);
}

if (isset($_POST['actionUpdate']))
{
	$accion = preg_replace('([^A-Za-zÁ-ź0-9 ])', '', $_POST['action']);

	if ($modelAdmin->updateAction($accion, $_POST['actionUpdate']))
    {
        $_SESSION['sweetAlert']['icon'] = 'success';
        $_SESSION['sweetAlert']['text'] = 'Acción actualizada exitosamente.';
    }
    else
    {
        $_SESSION['sweetAlert']['icon'] = 'error';
        $_SESSION['sweetAlert']['text'] = 'No se pudo actualizar la acción.';
    }

    header("Location: ".URL);
}

if (isset($_POST['delActionMant']))
{
	if ($modelAdmin->delActionMant($_POST['delActionMant']))
    {
    	unset($_SESSION['editAction']);

        $_SESSION['sweetAlert']['icon'] = 'success';
        $_SESSION['sweetAlert']['text'] = 'Acción eliminada exitosamente.';
    }
    else
    {
        $_SESSION['sweetAlert']['icon'] = 'error';
        $_SESSION['sweetAlert']['text'] = 'No se pudo eliminar la acción.';
    }

    header("Location: ".URL);
}