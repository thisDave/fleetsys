/*
----------------------------------------
|         CREACIÓN Y SELECCION         |
----------------------------------------
*/

DROP DATABASE IF EXISTS `db_fleetsys`;

CREATE DATABASE IF NOT EXISTS `db_fleetsys` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

USE `db_fleetsys`;

/*
----------------------------------------
|          TABLAS PRINCIPALES          |
----------------------------------------
*/


/*TABLA Paises*/

CREATE TABLE IF NOT EXISTS tbl_Paises(
	idPais	INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	Pais 	VARCHAR(45) NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*
TABLA Regiones
La tabla regiones incluye la foránea a pais, por lo que no es necesario que se mande a llamar
el pais de un usuario desde la tabla anterior
*/


CREATE TABLE IF NOT EXISTS tbl_Regiones(
	idRegion	INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	idPais 		INT NOT NULL,
	region		VARCHAR(45) NOT NULL,

	CONSTRAINT FK_regiones_idPais FOREIGN KEY (idPais) REFERENCES tbl_Paises (idPais)

)ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*
TABLA Tipo Proveedores

Hay proveedores que serán:
  Proveedor del vehículo
  Proveedor de producto
  Proveedor de mantenimiento en caso sea externo
*/

CREATE TABLE IF NOT EXISTS tbl_TipoProveedores(
	idTipoProveedor	INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	tipoProveedor	VARCHAR(35) NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*TABLA Proveedores*/

CREATE TABLE IF NOT EXISTS tbl_Proveedores(
	idProveedor			INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	nombre 				VARCHAR(45) NOT NULL,
	contacto 			VARCHAR(45) NULL,
	telefono			VARCHAR(45) NULL,
	idtipoProveedor 	INT NULL,
    idPais				INT NULL,

	CONSTRAINT FK_proveedores_idTipoProveedores FOREIGN KEY (idtipoProveedor) REFERENCES tbl_TipoProveedores (idTipoProveedor),
    CONSTRAINT FK_proveedores_idPais FOREIGN KEY (idPais) REFERENCES tbl_Paises (idPais)

)ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*TABLA Niveles de Usuario*/

CREATE TABLE IF NOT EXISTS tbl_nivelesUsuarios(
  idNivelUsuario	INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  nivelUsuario		VARCHAR(20) NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*
TABLA Cargos
Los cargos serán agregados por cada usuarioo, ya que los nombres de los cargos se prestan mucho a variaciones dentro de la fundación.

Nota se quitará la tabla temporalmente hasta tener una base de cargos oficial establecida por rrhh

*/

/*
CREATE TABLE IF NOT EXISTS tbl_Cargos(
	idCargo	INT NOT NULL PRIMARY KEY,
	cargo	VARCHAR(45) NOT NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8;
*/

/* TABLA Estados */

CREATE TABLE IF NOT EXISTS tbl_Estados(
	idEstado	INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	Estado		VARCHAR(25) NOT NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*TABLA Fotos de Perfil*/

CREATE TABLE IF NOT EXISTS tbl_fotoPerfiles(
	idFoto		INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	nombre		VARCHAR(15) NOT NULL,
	formato		VARCHAR(12) NOT NULL,
	foto 		BLOB NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*TABLA Usuarios*/

CREATE TABLE IF NOT EXISTS tbl_usuarios(
	idUsuario		INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	firstName 		VARCHAR(45) NOT NULL,
    secndName		VARCHAR(45) NOT NULL,
	firstApe 		VARCHAR(45) NOT NULL,
    secndApe		VARCHAR(45) NOT NULL,
	idNivelUsuario 	INT NOT NULL,
	idRegion 		INT NOT NULL,
	email 			VARCHAR(70) NOT NULL,
	pass			VARCHAR(100) NOT NULL,
	cargo			VARCHAR(70) NOT NULL,
	token 			VARCHAR(70) NULL,
	fechaToken 		DATETIME NULL,
    idFoto			INT NOT NULL DEFAULT 1,
    idEstado		INT NOT NULL,

	CONSTRAINT FK_usuarios_idRegion FOREIGN KEY (idRegion) REFERENCES tbl_Regiones (idRegion),

	CONSTRAINT FK_usuarios_idNivelUsuario FOREIGN KEY (idNivelUsuario) REFERENCES tbl_nivelesUsuarios (idNivelUsuario),

    CONSTRAINT FK_usuarios_idFoto FOREIGN KEY (idFoto) REFERENCES tbl_fotoPerfiles (idFoto),

    CONSTRAINT FK_usuarios_idEstado FOREIGN KEY (idEstado) REFERENCES tbl_Estados (idEstado)

)ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* TABLA licencias */

CREATE TABLE IF NOT EXISTS tbl_licencias(
	idLicencia		INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	idUsuario		INT NOT NULL,
    nit				VARCHAR(17) NOT NULL,
    dui				VARCHAR(10) NOT NULL,
    fechaExpd		DATE NOT NULL,
    fechaVenc		DATE NOT NULL,
    tipoLicencia	VARCHAR(30) NOT NULL,
    idEstado		INT NOT NULL,
    aprobacion		INT(1),

	CONSTRAINT FK_licencias_idUsuario FOREIGN KEY (idUsuario) REFERENCES tbl_usuarios (idUsuario),

    CONSTRAINT FK_licencias_idEstado FOREIGN KEY (idEstado) REFERENCES tbl_Estados (idEstado)

)ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*
TABLA Categorías
Categorías de los productos del inventario
*/

CREATE TABLE IF NOT EXISTS tbl_Categorias(
	idCategoria	INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	nombre		VARCHAR(50) NOT NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*TABLA Productos*/

CREATE TABLE IF NOT EXISTS tbl_Productos(
	idProducto	INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	idCategoria INT NOT NULL,
	nombre 		VARCHAR(60) NOT NULL,

    CONSTRAINT FK_productos_idCategoria FOREIGN KEY (idCategoria) REFERENCES tbl_Categorias (idCategoria)

)ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*TABLA Compra*/

CREATE TABLE IF NOT EXISTS tbl_compraProductos(
	idCompra		INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	fecha			DATE NOT NULL,
	numFactura		VARCHAR(10) NOT NULL,
    totalCompra		FLOAT NOT NULL,
    idProveedor		INT NOT NULL,
    idUsuario		INT NOT NULL,

    CONSTRAINT FK_compraProductos_idProveedor FOREIGN KEY (idProveedor) REFERENCES tbl_Proveedores (idProveedor),
    CONSTRAINT FK_compraProductos_idUsuario FOREIGN KEY (idUsuario) REFERENCES tbl_usuarios (idUsuario)

)ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*TABLA Detalle Compra*/

CREATE TABLE IF NOT EXISTS tbl_detalleCompras(
	idProducto		INT NOT NULL,
	cantidad		INT NOT NULL,
	precioUnitario	FLOAT NOT NULL,
    total			FLOAT NOT NULL,
    idCompra		INT NOT NULL,

    CONSTRAINT FK_detalleCompra_idProducto FOREIGN KEY (idProducto) REFERENCES tbl_Productos (idProducto),
    CONSTRAINT FK_detalleCompra_idCompra FOREIGN KEY (idCompra) REFERENCES tbl_compraProductos (idCompra)

)ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* TABLA Inventario */

CREATE TABLE IF NOT EXISTS tbl_inventarios(
	idInventario	INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	idProducto 		INT NOT NULL,
	cantidad 		INT NOT NULL,
    numFactura		VARCHAR(10) NOT NULL,
    precioUnitario	FLOAT NOT NULL,

    CONSTRAINT FK_inventarios_idProducto FOREIGN KEY (idProducto) REFERENCES tbl_Productos (idProducto)

)ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* TABLA Tipo de Vehiculos */

CREATE TABLE IF NOT EXISTS tbl_tipoVehiculos(
	idTipoVehiculo	INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	vehiculo		VARCHAR(45) NOT NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*TABLA Marcas*/

CREATE TABLE IF NOT EXISTS tbl_marcas(
	idMarca	INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	marca	VARCHAR(20) NOT NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*TABLA Vehiculos*/

CREATE TABLE IF NOT EXISTS tbl_Vehiculos(
	idVehiculo		INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
/*	fotoVehiculo 	BLOB NOT NULL, */
	numeroPlaca 	VARCHAR(45) NOT NULL,
	color 			VARCHAR(45) NOT NULL,
    idMarca 		INT NOT NULL,
    modelo			VARCHAR(20) NOT NULL,
    anno			INT NOT NULL,
    motor			VARCHAR(15) NOT NULL,
    chasis			VARCHAR(25) NOT NULL,
    vin				VARCHAR(30) NOT NULL,
	idProveedor 	INT NOT NULL,
	idRegion 		INT NOT NULL,
	proxMtto 		INT NULL,
    kilometraje		INT NOT NULL,
	idTipoVehiculo 	INT NOT NULL,
    idEstado		INT NOT NULL,

	CONSTRAINT FK_vehiculos_idTipoVehiculo FOREIGN KEY (idTipoVehiculo) REFERENCES tbl_tipoVehiculos (idTipoVehiculo),

	CONSTRAINT FK_vehiculos_idRegion FOREIGN KEY (idRegion) REFERENCES tbl_Regiones (idRegion),

	CONSTRAINT FK_vehiculos_idProveedor FOREIGN KEY (idProveedor) REFERENCES tbl_Proveedores (idProveedor),

    CONSTRAINT FK_vehiculos_idMarca FOREIGN KEY (idMarca) REFERENCES tbl_marcas (idMarca),

    CONSTRAINT FK_vehiculos_idEstado FOREIGN KEY (idEstado) REFERENCES tbl_Estados (idEstado)

)ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*TABLA Bitacoras*/

CREATE TABLE IF NOT EXISTS tbl_Bitacoras(
	idBitacora		INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	idVehiculo 		INT NOT NULL,
	kmInicial 		INT NOT NULL,
	kmFinal 		INT NULL,
	fechaSalida 	DATE NOT NULL,
	fechaEntrada 	DATE NULL,
    HoraSalida 		TIME NOT NULL,
	HoraEntrada 	TIME NULL,
	idUsuario 		INT NOT NULL,
	destino 		VARCHAR(50) NOT NULL,
	motivo 			VARCHAR(60) NOT NULL,
    observaciones	VARCHAR(200) NULL,
    idEstado		INT NOT NULL,

	CONSTRAINT FK_bitacoras_idVehiculo FOREIGN KEY (idVehiculo) REFERENCES tbl_Vehiculos (idVehiculo) ON DELETE CASCADE,

	CONSTRAINT FK_bitacoras_idUsuario FOREIGN KEY (idUsuario) REFERENCES tbl_usuarios (idUsuario),

    CONSTRAINT FK_bitacoras_idEstado FOREIGN KEY (idEstado) REFERENCES tbl_Estados (idEstado)

)ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* TABLA hojaSalida */

CREATE TABLE IF NOT EXISTS tbl_hojaSalidas(
	idHoja				INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    idBitacora			INT NOT NULL,
    nivelGas			VARCHAR(15) NOT NULL,
    observacionInicial	VARCHAR(200),
    observacionFinal	VARCHAR(200),

    CONSTRAINT FK_hojaSalidas_idBitacora FOREIGN KEY (idBitacora) REFERENCES tbl_Bitacoras (idBitacora)

)ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* TABLA autoItems */

CREATE TABLE IF NOT EXISTS tbl_autoItems(
	idItem		INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    item		VARCHAR(50) NOT NULL,
    idEstado	INT NOT NULL,

    CONSTRAINT FK_autoItems_idEstado FOREIGN KEY (idEstado) REFERENCES tbl_Estados (idEstado)

)ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* TABLA detalle de hojaSalidas */

CREATE TABLE IF NOT EXISTS tbl_detalleHojas(
	idHoja			INT NOT NULL,
    idItem			INT NOT NULL,
    idEstado		INT NOT NULL,
    CONSTRAINT FK_detalleHojas_idHoja FOREIGN KEY (idHoja) REFERENCES tbl_hojaSalidas (idHoja),
    CONSTRAINT FK_detalleHojas_idItem FOREIGN KEY (idItem) REFERENCES tbl_autoItems (idItem),
    CONSTRAINT FK_detalleHojas_idEstado FOREIGN KEY (idEstado) REFERENCES tbl_Estados (idEstado)

)ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* TABLA tipo de golpes */

CREATE TABLE IF NOT EXISTS tbl_tipoGolpes(
	idTipo			INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    tipo			VARCHAR(15) NOT NULL,
    descripcion		VARCHAR(200)

)ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* TABLA Golpes */

CREATE TABLE IF NOT EXISTS tbl_detalleGolpes(
	idHoja			INT NOT NULL,
    costado			VARCHAR(30) NOT NULL,
    foto			VARCHAR(100) NOT NULL,
    idTipo			INT NOT NULL,
    CONSTRAINT FK_detalleGolpes_idHoja FOREIGN KEY (idHoja) REFERENCES tbl_hojaSalidas (idHoja),
    CONSTRAINT FK_detalleGolpes_idTipo FOREIGN KEY (idTipo) REFERENCES tbl_tipoGolpes (idTipo)

)ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*TABLA Combustibles*/

CREATE TABLE IF NOT EXISTS tbl_Combustibles(
	idCombustible	INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    tipoIngreso		VARCHAR(15) NOT NULL,
    nIngreso		INT NOT NULL,
	valor 			FLOAT NOT NULL,
    fechaIngreso	DATE NOT NULL,
    fecha			DATE NOT NULL,
    hora			TIME NOT NULL,
    kmSalida		INT NOT NULL,
    kmEntrada		INT NOT NULL,
    idProveedor		INT NOT NULL,
	idUsuario 		INT NOT NULL,
	idVehiculo		INT NOT NULL,

	CONSTRAINT FK_combustibles_idUsuario FOREIGN KEY (idUsuario) REFERENCES tbl_usuarios (idUsuario),

	CONSTRAINT FK_combustibles_idVehiculo FOREIGN KEY (idVehiculo) REFERENCES tbl_Vehiculos (idVehiculo)  ON DELETE CASCADE

)ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*TABLA acciones mantenimiento*/

CREATE TABLE IF NOT EXISTS tbl_accionesMant(
	idAccion	INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    accion		VARCHAR(300) NOT NULL

)ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*TABLA Preordenes*/

CREATE TABLE IF NOT EXISTS tbl_preordenes(
	idPreorden		INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    tipoPreorden	VARCHAR(20) NOT NULL,
    idVehiculo 		INT NOT NULL,
    kilometraje		INT NOT NULL,
	fechaEntrada 	DATE NOT NULL,
    fechaSalida		DATE NULL,
    comentarios 	VARCHAR(100) NOT NULL,
	idUsuario 		INT NOT NULL,
    idEstado		INT NOT NULL DEFAULT 3,

	CONSTRAINT FK_preorden_idUsuario FOREIGN KEY (idUsuario) REFERENCES tbl_usuarios (idUsuario),

	CONSTRAINT FK_preorden_idVehiculo FOREIGN KEY (idVehiculo) REFERENCES tbl_Vehiculos (idVehiculo)  ON DELETE CASCADE,

    CONSTRAINT FK_preorden_idEstado FOREIGN KEY (idEstado) REFERENCES tbl_Estados (idEstado)

)ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*TABLA Detalle Preordenes externas*/

CREATE TABLE IF NOT EXISTS tbl_detallePrexternas(
	idPreorden	INT NOT NULL,
    accion		VARCHAR(300) NOT NULL,
    idEstado	INT NOT NULL,

	CONSTRAINT FK_dprexterna_idpreorden FOREIGN KEY (idpreorden) REFERENCES tbl_preordenes (idpreorden)

)ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*TABLA Detalle Preordenes externas*/

CREATE TABLE IF NOT EXISTS tbl_detallePrinternas(
	idPreorden	INT NOT NULL,
    accion		VARCHAR(300) NOT NULL,
    idEstado	INT NOT NULL,

	CONSTRAINT FK_dprinterna_idpreorden FOREIGN KEY (idpreorden) REFERENCES tbl_preordenes (idpreorden)

)ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*TABLA mantenimientos*/

CREATE TABLE IF NOT EXISTS tbl_mttos(
	idMtto 		INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	idPreorden	INT NOT NULL,
    idUsuario	INT NOT NULL,
	tipoMtto	VARCHAR(10) NOT NULL,
    comentario	VARCHAR(100) NOT NULL,

	CONSTRAINT FK_mantenimientos_idpreorden FOREIGN KEY (idpreorden) REFERENCES tbl_preordenes (idpreorden),
	CONSTRAINT FK_mantenimientos_idUsuario FOREIGN KEY (idUsuario) REFERENCES tbl_usuarios (idUsuario)

)ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*TABLA Detalle mantenimientos*/

CREATE TABLE IF NOT EXISTS tbl_detalleMttos(
	idMtto			INT NOT NULL,
    idProducto 		INT NOT NULL,
	cantidad 		INT NOT NULL,
    numFactura		VARCHAR(10) NOT NULL,
    precioUnitario	FLOAT NOT NULL,
    idProveedor 	INT NOT NULL,

	CONSTRAINT FK_detallemttos_idMtto FOREIGN KEY (idMtto) REFERENCES tbl_mttos (idMtto),
    CONSTRAINT FK_detallemttos_idProducto FOREIGN KEY (idProducto) REFERENCES tbl_Productos (idProducto),
    CONSTRAINT FK_detallemttos_idProveedor FOREIGN KEY (idProveedor) REFERENCES tbl_Proveedores (idProveedor)

)ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*TABLA Histories*/

CREATE TABLE IF NOT EXISTS tbl_Histories(
	idHistory		INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	fechaIngreso 	DATE NULL,
	horaInicio 		TIME NULL,
	horaFin 		TIME NULL,
	idUsuario 		INT NULL,

	CONSTRAINT FK_histories_idUsuario FOREIGN KEY (idUsuario) REFERENCES tbl_usuarios (idUsuario)

)ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*TABLA Cookies*/

CREATE TABLE IF NOT EXISTS tbl_cookies(
	email			VARCHAR(60) NOT NULL,
	pass			VARCHAR(60) NOT NULL,
    sessionToken	VARCHAR(125) NOT NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*TABLA Comentarios*/

CREATE TABLE IF NOT EXISTS tbl_comentarioSistema(
	idComentario	INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    idUsuario		INT NULL,
    comentario		VARCHAR(200),
    fecha			DATE,
    hora			TIME
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*TABLA Alertas*/

CREATE TABLE IF NOT EXISTS tbl_alertas(
	idAlerta	INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    info		VARCHAR(60) NOT NULL,
    valor		INT NOT NULL,
    alerta		INT NOT NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*TABLA Notificaciones*/

CREATE TABLE IF NOT EXISTS tbl_notificaciones(
	idAlerta	INT NOT NULL AUTO_INCREMENT,
    mensaje		VARCHAR(60) NOT NULL,
    detalles	VARCHAR(100) NOT NULL,
    idVehiculo 	INT NOT NULL,

    CONSTRAINT FK_notificaciones_idAlerta FOREIGN KEY (idAlerta) REFERENCES tbl_alertas (idAlerta),
    CONSTRAINT FK_notificaciones_idVehiculo FOREIGN KEY (idVehiculo) REFERENCES tbl_Vehiculos (idVehiculo)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*

--  TABLAS EXTRA OFICIALES --

*/

CREATE TABLE IF NOT EXISTS tbl_entradas(
	idEntrada	INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    idUsuario	INT NOT NULL,
    fecha		DATETIME NOT NULL DEFAULT NOW(),

    CONSTRAINT FK_entradas_idUsuario FOREIGN KEY (idUsuario) REFERENCES tbl_usuarios (idUsuario)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS tbl_salidas(
	idEntrada	INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    idUsuario	INT NOT NULL,
    fecha		DATETIME NOT NULL DEFAULT NOW(),

    CONSTRAINT FK_salidas_idUsuario FOREIGN KEY (idUsuario) REFERENCES tbl_usuarios (idUsuario)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;





