/*
-----------------------------------------
|       PROCEDIMIENTOS ALMACENADOS      |
-----------------------------------------
*/

USE `db_fleetsys`;

DELIMITER //
CREATE PROCEDURE SP_login( _id INT )
BEGIN
	SELECT
		u.idUsuario,
		n.nivelUsuario AS 'nivel'
	FROM
		tbl_usuarios u
	INNER JOIN
		tbl_nivelesUsuarios n ON u.idNivelUsuario = n.idNivelUsuario
	WHERE
		u.idUsuario = _id;
END //


DELIMITER //
CREATE PROCEDURE SP_infoUsuario( _id INT )
BEGIN
	SELECT
		u.idUsuario,
		u.firstName,
        u.secndName,
        u.firstApe,
        u.secndApe,
        u.email,
		n.nivelUsuario AS 'Nivel',
        u.Cargo,
        r.Region,
        p.Pais,
        f.foto,
        e.estado
	FROM
		tbl_usuarios u
	INNER JOIN
		tbl_nivelesUsuarios n ON u.idNivelUsuario = n.idNivelUsuario
	INNER JOIN
		tbl_Regiones r ON u.idRegion = r.idRegion
	INNER JOIN
		tbl_Paises p ON r.idPais = p.idPais
	INNER JOIN
		tbl_fotoPerfiles f ON u.idFoto = f.idFoto
	INNER JOIN
		tbl_Estados e ON u.idEstado = e.idEstado
	WHERE
		u.idUsuario = _id;
END //


DELIMITER //
CREATE PROCEDURE SP_actualizarUsuario(
    _firstName 	VARCHAR(45),
    _secndName 	VARCHAR(45),
    _firstApe 	VARCHAR(45),
    _secndApe 	VARCHAR(45),
    _cargo		VARCHAR(70),
    _idRegion	INT,
    _idUsuario	INT
)
BEGIN
	UPDATE
		tbl_usuarios
	SET
		firstName = _firstName,
        secndName = _secndName,
        firstApe = _firstApe,
        secndApe = _secndApe,
        Cargo = _cargo,
        idRegion = _idRegion
	WHERE
		idUsuario = _idUsuario;
END //


DELIMITER //
CREATE PROCEDURE SP_listaVehiculos( _idPais INT )
BEGIN
	SELECT
		v.idVehiculo,
        v.numeroPlaca,
        v.color,
        m.marca,
        v.modelo,
        v.anno,
        v.motor,
        v.chasis,
        v.vin,
        p.nombre,
        r.region,
        v.proxMtto,
        v.kilometraje,
        t.vehiculo AS 'tipo',
        e.estado
	FROM
		tbl_Vehiculos v
	INNER JOIN
		tbl_marcas m ON v.idMarca = m.idMarca
	INNER JOIN
		tbl_Proveedores p ON v.idProveedor = p.idProveedor
	INNER JOIN
		tbl_tipoVehiculos t ON v.idTipoVehiculo = t.idTipoVehiculo
	INNER JOIN
		tbl_Regiones r ON v.idRegion = r.idRegion
	INNER JOIN
		tbl_Estados e ON v.idEstado = e.idEstado
	WHERE
		r.idPais = _idPais AND v.idTipoVehiculo <> 3;
END //


DELIMITER //
CREATE PROCEDURE SP_allCars( _idPais INT )
BEGIN
	SELECT
		v.idVehiculo,
        v.numeroPlaca,
        v.color,
        m.marca,
        v.modelo,
        v.anno,
        v.motor,
        v.chasis,
        v.vin,
        p.nombre,
        r.region,
        v.proxMtto,
        v.kilometraje,
        t.vehiculo AS 'tipo',
        e.estado
	FROM
		tbl_Vehiculos v
	INNER JOIN
		tbl_marcas m ON v.idMarca = m.idMarca
	INNER JOIN
		tbl_Proveedores p ON v.idProveedor = p.idProveedor
	INNER JOIN
		tbl_tipoVehiculos t ON v.idTipoVehiculo = t.idTipoVehiculo
	INNER JOIN
		tbl_Regiones r ON v.idRegion = r.idRegion
	INNER JOIN
		tbl_Estados e ON v.idEstado = e.idEstado
	WHERE
		r.idPais = _idPais;
END //


DELIMITER //
CREATE PROCEDURE SP_listaMotos( _idPais INT )
BEGIN
	SELECT
		v.idVehiculo,
        v.numeroPlaca,
        v.color,
        m.marca,
        v.modelo,
        v.anno,
        v.motor,
        v.chasis,
        v.vin,
        p.nombre,
        r.region,
        v.proxMtto,
        v.kilometraje,
        t.vehiculo AS 'tipo',
        e.estado
	FROM
		tbl_Vehiculos v
	INNER JOIN
		tbl_marcas m ON v.idMarca = m.idMarca
	INNER JOIN
		tbl_Proveedores p ON v.idProveedor = p.idProveedor
	INNER JOIN
		tbl_tipoVehiculos t ON v.idTipoVehiculo = t.idTipoVehiculo
	INNER JOIN
		tbl_Regiones r ON v.idRegion = r.idRegion
	INNER JOIN
		tbl_Estados e ON v.idEstado = e.idEstado
	WHERE
		r.idPais = 1 AND v.idTipoVehiculo = 3;
END //


DELIMITER //
CREATE PROCEDURE SP_infoVehiculo( _id INT )
BEGIN
	SELECT
		v.idVehiculo,
        v.numeroPlaca,
        v.color,
        m.marca,
        v.modelo,
        v.anno,
        v.motor,
        v.chasis,
        v.vin,
        p.nombre,
        r.region,
        v.proxMtto,
        v.kilometraje,
        t.vehiculo AS 'tipo',
        e.estado
	FROM
		tbl_Vehiculos v
	INNER JOIN
		tbl_marcas m ON v.idMarca = m.idMarca
	INNER JOIN
		tbl_Proveedores p ON v.idProveedor = p.idProveedor
	INNER JOIN
		tbl_tipoVehiculos t ON v.idTipoVehiculo = t.idTipoVehiculo
	INNER JOIN
		tbl_Regiones r ON v.idRegion = r.idRegion
	INNER JOIN
		tbl_Estados e ON v.idEstado = e.idEstado
	WHERE
		v.idVehiculo = _id;
END //


DELIMITER //
CREATE PROCEDURE SP_listaProveedores( _idPais INT )
BEGIN
	SELECT
		p.idProveedor,
        p.nombre,
        p.contacto,
        p.telefono,
        t.tipoProveedor,
        c.pais
	FROM
		tbl_Proveedores p
	INNER JOIN
		tbl_TipoProveedores t ON p.idTipoProveedor = t.idTipoProveedor
	INNER JOIN
		tbl_Paises c ON p.idPais = c.idPais
	WHERE
		p.idPais = _idPais;
END //


DELIMITER //
CREATE PROCEDURE SP_infoProveedor( _id INT )
BEGIN
	SELECT
		p.idProveedor,
        p.nombre,
        p.contacto,
        p.telefono,
        t.tipoProveedor,
        c.pais
	FROM
		tbl_Proveedores p
	INNER JOIN
		tbl_TipoProveedores t ON p.idTipoProveedor = t.idTipoProveedor
	INNER JOIN
		tbl_Paises c ON p.idPais = c.idPais
	WHERE
		p.idProveedor = _id;
END //


DELIMITER //
CREATE PROCEDURE SP_insertProvider(
	_nombre			VARCHAR(45),
    _contacto		VARCHAR(45),
    _telefono		VARCHAR(45),
    _idTipo			INT,
    _idPais			INT
)
BEGIN
	IF EXISTS (SELECT * FROM tbl_Proveedores WHERE nombre = _nombre) THEN
		SELECT 'error' AS 'resultado';
    ELSE
		INSERT INTO tbl_Proveedores VALUES (NULL, _nombre, _contacto, _telefono, _idTipo, _idPais);
		SELECT 'success' AS 'resultado';
    END IF;
END //


DELIMITER //
CREATE PROCEDURE SP_updateProvider(
	_nombre			VARCHAR(45),
    _contacto		VARCHAR(45),
    _telefono		VARCHAR(45),
    _idProveedor	INT
)
BEGIN
	UPDATE
		tbl_Proveedores
	SET
		nombre = _nombre,
        contacto = _contacto,
        telefono = _telefono
	WHERE
		idProveedor = _idProveedor;
END //

DELIMITER //
CREATE PROCEDURE SP_autosProveedores( _idProveedor INT )
BEGIN
	SELECT
		v.idVehiculo,
        v.numeroPlaca,
        v.color,
        m.marca,
        p.nombre,
        r.region,
        v.proxMtto,
        v.kilometraje,
        t.vehiculo,
        e.estado
	FROM
		tbl_Vehiculos v
	INNER JOIN
		tbl_marcas m ON v.idMarca = m.idMarca
	INNER JOIN
		tbl_Proveedores p ON v.idProveedor = p.idProveedor
	INNER JOIN
		tbl_tipoVehiculos t ON v.idTipoVehiculo = t.idTipoVehiculo
	INNER JOIN
		tbl_Regiones r ON v.idRegion = r.idRegion
	INNER JOIN
		tbl_Estados e ON v.idEstado = e.idEstado
	WHERE
		p.idProveedor = _idProveedor;
END //


DELIMITER //
CREATE PROCEDURE SP_productosProveedores( _idProveedor INT )
BEGIN
	SELECT
		p.idProducto,
        p.nombre,
        c.nombre AS 'categoria',
        i.cantidad
	FROM
		tbl_inventarios i
	INNER JOIN
		tbl_Productos p ON i.idProducto = p.idProducto
	INNER JOIN
		tbl_Categorias c ON p.idCategoria = c.idCategoria
	WHERE
		p.idProveedor = _idProveedor;
END //


DELIMITER //
CREATE PROCEDURE SP_ingresarAuto(
	_numeroPlaca 		VARCHAR(45),
	_color 				VARCHAR(45),
    _idMarca 			INT,
    _modelo				VARCHAR(20),
    _anno				INT,
    _motor				VARCHAR(15),
    _chasis				VARCHAR(25),
    _vin				VARCHAR(30),
	_idProveedor 		INT,
	_idRegion 			INT,
	_mtto			 	INT,
    _kilometraje		INT,
	_idTipoVehiculo		INT,
    _idUsuario			INT
)
BEGIN
	DECLARE _idVehiculo INT;

	INSERT INTO
		tbl_Vehiculos
	VALUES(
		NULL,
		_numeroPlaca,
		_color,
		_idMarca,
		_modelo,
		_anno,
		_motor,
		_chasis,
		_vin,
		_idProveedor,
		_idRegion,
		_mtto,
        _kilometraje,
		_idTipoVehiculo,
        1
	);

    SET _idVehiculo = (SELECT idVehiculo FROM tbl_Vehiculos ORDER BY idVehiculo DESC LIMIT 1);

    CALL SP_primerBitacora(_idVehiculo, _kilometraje, _idUsuario);
END //


DELIMITER //
CREATE PROCEDURE SP_primerBitacora(
	_idVehiculo		INT,
    _kilometraje	INT,
    _idUsuario		INT
)
BEGIN
	INSERT INTO
		`tbl_Bitacoras`(`idBitacora`, `idVehiculo`, `kmInicial`, `kmFinal`, `fechaSalida`, `fechaEntrada`, `HoraSalida`, `HoraEntrada`, `idUsuario`, `destino`, `motivo`, `observaciones`, `idEstado`)
	VALUES
		(NULL, _idVehiculo, _kilometraje, _kilometraje, CURDATE(), CURDATE(), TIME_FORMAT(NOW(), "%H:%i"), TIME_FORMAT(NOW(), "%H:%i"), _idUsuario, 'Fundación Educo', 'Primer registro de bitácora', '', 6);	
END //


DELIMITER //
CREATE PROCEDURE SP_actualizarAuto(
	_numeroPlaca 	VARCHAR(45),
	_color 			VARCHAR(45),
    _idMarca 		INT,
    _modelo			VARCHAR(20),
    _anno			INT,
    _motor			VARCHAR(15),
    _chasis			VARCHAR(25),
    _vin			VARCHAR(30),
	_idProveedor 	INT,
	_idRegion 		INT,
	_mtto	 		INT,
    _kilometraje	INT,
	_idTipoVehiculo INT,
    _idVehiculo		INT
)
BEGIN
	DECLARE _countBit INT;

	SET _countBit = (SELECT COUNT(*) FROM tbl_Bitacoras WHERE idVehiculo = _idVehiculo);

	IF _countBit <= 1 THEN
		UPDATE tbl_Bitacoras SET kmInicial = _kilometraje, kmFinal = _kilometraje WHERE idVehiculo = _idVehiculo;
	END IF;

	UPDATE
		tbl_Vehiculos
	SET
		numeroPlaca = _numeroPlaca,
		color = _color,
		idMarca = _idMarca,
		modelo = _modelo,
		anno = _anno,
		motor = _motor,
		chasis = _chasis,
		vin = _vin,
		idProveedor = _idProveedor,
		idRegion = _idRegion,
		proxMtto = _mtto,
        kilometraje = _kilometraje,
		idTipoVehiculo = _idTipoVehiculo
	WHERE
		idVehiculo = _idVehiculo;
END //


DELIMITER //
CREATE PROCEDURE SP_eliminarAuto( _id INT )
BEGIN
	DECLARE _total INT;
    DECLARE _idBitacora INT;
    DECLARE _idHoja INT;

	IF EXISTS (SELECT * FROM tbl_Vehiculos WHERE idVehiculo = _id) THEN

        SET _total = (SELECT count(*) FROM `tbl_Bitacoras` WHERE idVehiculo = _id);

        IF _total <= 1 THEN

            SET _idBitacora = (SELECT idBitacora FROM `tbl_Bitacoras` WHERE idVehiculo = _id);

            SET _idHoja = (SELECT idHoja FROM `tbl_hojasalidas` WHERE idBitacora = _idBitacora);

            DELETE FROM tbl_detallehojas WHERE idHoja = _idHoja;

            DELETE FROM tbl_detalleGolpes WHERE idHoja = _idHoja;

            DELETE FROM tbl_hojasalidas WHERE idBitacora = _idBitacora;

			DELETE FROM tbl_Bitacoras WHERE idVehiculo = _id;

            DELETE FROM tbl_Vehiculos WHERE idVehiculo = _id;

			SELECT 'success' AS 'resultado';
		ELSE
			SELECT 'error' AS 'resultado';
        END IF;
	ELSE
		SELECT 'error' AS 'resultado';
    END IF;
END //

DELIMITER //
CREATE PROCEDURE SP_listarProductCatalog()
BEGIN
	SELECT
		p.idProducto,
        p.nombre,
        c.idCategoria,
        c.nombre AS 'categoria'
	FROM
		tbl_Productos p
	INNER JOIN
		tbl_Categorias c ON p.idCategoria = c.idCategoria;
END //


DELIMITER //
CREATE PROCEDURE SP_insertProduct(
	_categoria	INT,
    _nombre		VARCHAR(60)
)
BEGIN
	DECLARE _val INT;
	IF NOT EXISTS (SELECT * FROM tbl_Productos WHERE nombre = _nombre) THEN
		INSERT INTO tbl_Productos VALUES (NULL, _categoria, _nombre);
        SET _vaL = 1;
	ELSE
		SET _val = 0;
    END IF;

    SELECT _val AS 'val';
END //


DELIMITER //
CREATE PROCEDURE SP_insertCategory(
	_categoria	VARCHAR(50)
)
BEGIN
    IF EXISTS (SELECT * FROM tbl_Categorias WHERE nombre = _categoria) THEN
		SELECT 'error' AS 'resultado';
	ELSE
		INSERT INTO `tbl_Categorias`(`idCategoria`, `nombre`) VALUES (NULL, _categoria);
        SELECT 'success' AS 'resultado';
    END IF;
END //


DELIMITER //
CREATE PROCEDURE SP_insertarCompra(
	_numFactura		VARCHAR(10),
	_totalCompra	FLOAT,
	_idProveedor 	INT(11),
    _idUsuario		INT(11)
)
BEGIN
	INSERT INTO tbl_compraProductos(`idCompra`, `fecha`, `numFactura`, `totalCompra`, `idProveedor`, `idUsuario`) VALUES
    (NULL, NOW(), _numFactura, _totalCompra, _idProveedor, _idUsuario);

    SELECT idCompra FROM tbl_compraProductos ORDER BY idCompra DESC LIMIT 1;
END //


DELIMITER //
CREATE PROCEDURE SP_insertarDetalleCompra(
	_idProducto		INT,
	_cantidad		INT,
	_precioUnitario FLOAT,
    _total			FLOAT,
    _idCompra		INT
)
BEGIN
	INSERT INTO tbl_detalleCompras(`idProducto`, `cantidad`, `precioUnitario`, `total`, `idCompra`) VALUES
    (_idProducto, _cantidad, _precioUnitario, _total, _idCompra);
END //


DELIMITER //
CREATE PROCEDURE SP_insertarInventario(
	_idProducto		INT,
	_cantidad		INT,
    _numFactura		VARCHAR(10),
    _precioUnitario	FLOAT
)
BEGIN
	INSERT INTO tbl_inventarios(`idInventario`, `idProducto`, `cantidad`, `numFactura`, `precioUnitario`) VALUES
    (NULL, _idProducto, _cantidad, _numFactura, _precioUnitario);
END //


DELIMITER //
CREATE PROCEDURE SP_listaInventario()
BEGIN
	SELECT
		i.idProducto,
        p.nombre,
        c.nombre AS 'categoria',
        SUM(i.cantidad) AS 'cantidad',
        i.numFactura,
        i.precioUnitario
	FROM
		tbl_inventarios i
	INNER JOIN
		tbl_Productos p ON i.idProducto = p.idProducto
	INNER JOIN
		tbl_Categorias c ON p.idCategoria = c.idCategoria
	GROUP BY
		idProducto;
END //


DELIMITER //
CREATE PROCEDURE SP_listaProductoInventario( _idProducto INT )
BEGIN
	SELECT
		i.idProducto,
        p.nombre,
        c.nombre AS 'categoria',
        i.cantidad,
        i.numFactura,
        CONCAT('$',i.precioUnitario) AS 'precioUnitario'
	FROM
		tbl_inventarios i
	INNER JOIN
		tbl_Productos p ON i.idProducto = p.idProducto
	INNER JOIN
		tbl_Categorias c ON p.idCategoria = c.idCategoria
	WHERE
		i.idProducto = _idProducto;
END //


DELIMITER //
CREATE PROCEDURE SP_updateProd(
    _nombre			VARCHAR(60),
    _idCategoria	INT,
    _idProducto 	INT
)
BEGIN
	IF EXISTS (SELECT * FROM tbl_Productos WHERE nombre = _nombre AND idProducto <> _idProducto) THEN
		SELECT 'error' AS 'resultado';
    ELSE
		UPDATE tbl_Productos SET idCategoria = _idCategoria, nombre = _nombre WHERE idProducto = _idProducto;
        SELECT 'success' AS 'resultado';
    END IF;
END //


DELIMITER //
CREATE PROCEDURE SP_updateCategory(
    _nombre			VARCHAR(60),
    _idCategoria	INT
)
BEGIN
	IF EXISTS (SELECT * FROM tbl_Categorias WHERE nombre = _nombre AND idCategoria <> _idCategoria) THEN
		SELECT 'error' AS 'resultado';
    ELSE
		UPDATE tbl_Categorias SET nombre = _nombre WHERE idCategoria = _idCategoria;
        SELECT 'success' AS 'resultado';
    END IF;
END //


DELIMITER //
CREATE PROCEDURE SP_delCategory(
    _idCategoria	INT
)
BEGIN
	IF EXISTS (SELECT * FROM tbl_Categorias WHERE idCategoria = _idCategoria) THEN
		DELETE FROM tbl_Categorias WHERE idCategoria = _idCategoria;
		SELECT 'success' AS 'resultado';
    ELSE
        SELECT 'error' AS 'resultado';
    END IF;
END //


DELIMITER //
CREATE PROCEDURE SP_delProd(
    _idProducto	INT
)
BEGIN
	IF EXISTS (SELECT * FROM tbl_Productos WHERE idProducto = _idProducto) THEN
		DELETE FROM tbl_Productos WHERE idProducto = _idProducto;
		SELECT 'success' AS 'resultado';
    ELSE
        SELECT 'error' AS 'resultado';
    END IF;
END //


DELIMITER //
CREATE PROCEDURE SP_bitacorasVehiculo(
    _idVehiculo	INT
)
BEGIN
	SELECT
		b.idBitacora,
		CONCAT(u.firstName, ' ', u.firstApe) AS 'piloto',
		b.fechaSalida,
		b.horaSalida,
		b.fechaEntrada,
		b.horaEntrada,
		b.kmInicial,
		b.kmFinal,
		b.destino,
		b.motivo,
		b.observaciones,
        e.estado
	FROM
		tbl_Bitacoras b
	INNER JOIN
		tbl_usuarios u ON b.idUsuario = u.idUsuario
	INNER JOIN
		tbl_Estados e ON b.idEstado = e.idEstado
	WHERE
		b.idVehiculo = _idVehiculo;
END //


DELIMITER //
CREATE PROCEDURE SP_nuevaBitacora(
	_idVehiculo 	INT,
	_kmInicial 		INT,
	_fechaSalida 	DATE,
    _HoraSalida 	TIME,
	_idUsuario 		INT,
	_destino 		VARCHAR(50),
	_motivo 		VARCHAR(60),
    _observaciones	VARCHAR(200),
    _hojaObserv		VARCHAR(200),
    _nivelGas		VARCHAR(15)
)
BEGIN
	DECLARE _idBitacora INT;
    DECLARE _idHoja INT;

	INSERT INTO `tbl_Bitacoras` (`idBitacora`, `idVehiculo`, `kmInicial`, `fechaSalida`, `HoraSalida`, `idUsuario`, `destino`, `motivo`, `observaciones`, `idEstado`) VALUES
    (NULL, _idVehiculo, _kmInicial, _fechaSalida, _HoraSalida, _idUsuario, _destino, _motivo, _observaciones, 7);

    SET _idBitacora = (SELECT idBitacora FROM tbl_Bitacoras ORDER BY idBitacora DESC LIMIT 1);

    INSERT INTO `tbl_hojaSalidas`(`idHoja`, `idBitacora`, `nivelGas`, `observacionInicial`) VALUES (NULL, _idBitacora, _nivelGas, _hojaObserv);

    SET _idHoja = (SELECT idBitacora FROM tbl_hojaSalidas ORDER BY idHoja DESC LIMIT 1);

    SELECT _idHoja AS 'idHoja';

END //


DELIMITER //
CREATE PROCEDURE SP_updateBitacora(
    _kmFinal	INT,
    _idBitacora	INT,
    _horaEntrada TIME
)
BEGIN
	DECLARE _idVehiculo INT;
	DECLARE _diferencia INT;
	DECLARE _valor INT;
	DECLARE _proxMtto INT;
	DECLARE _alerta INT;

	UPDATE
		tbl_Bitacoras
	SET
		kmFinal = _kmFinal,
		fechaEntrada = CURDATE(),
		horaEntrada = _horaEntrada,
		idEstado = 6 WHERE idBitacora = _idBitacora;

	SET _idVehiculo = (SELECT idVehiculo FROM tbl_Bitacoras WHERE idBitacora = _idBitacora);

	UPDATE tbl_Vehiculos SET kilometraje = _kmFinal WHERE idVehiculo = _idVehiculo;

	SET _valor = (SELECT valor FROM tbl_alertas WHERE idAlerta = 1);

	SET _proxMtto = (SELECT proxMtto FROM tbl_Vehiculos WHERE idVehiculo = _idVehiculo);

	SET _alerta = (SELECT alerta FROM tbl_alertas WHERE idAlerta = 1);

	SET _diferencia = _proxMtto - _kmFinal;

	IF (_diferencia <= _alerta) THEN
		INSERT INTO
			tbl_notificaciones
		VALUES
			(1, "Realizar preorden para:", "Crear preorden de mantenimiento", _idVehiculo);
	END IF;
END //


DELIMITER //
CREATE PROCEDURE SP_primerHojaSalida( _idBitacora INT )
BEGIN
	INSERT INTO `tbl_hojaSalidas`(`idHoja`, `idBitacora`, `nivelGas`, `observacionInicial`, `observacionFinal`) VALUES (NULL, _idBitacora, 'Tanque Lleno', 'Creación inicial de hoja de salida.', 'Creación inicial de hoja de final.');

    SELECT idHoja FROM tbl_hojaSalidas ORDER BY idHoja DESC LIMIT 1;
END //


DELIMITER //
CREATE PROCEDURE SP_ultimaHoja( _idVehiculo INT )
BEGIN

	SELECT hs.* FROM tbl_hojaSalidas hs INNER JOIN tbl_Bitacoras bt ON hs.idBitacora = bt.idBitacora WHERE bt.idVehiculo = _idVehiculo ORDER BY hs.idHoja DESC LIMIT 1;

END //

DELIMITER //
CREATE PROCEDURE SP_updateHoja(
    _observ		VARCHAR(200),
    _nivelGas	VARCHAR(15),
    _idBitacora	INT
)
BEGIN
	UPDATE tbl_hojaSalidas SET observacionFinal = _observ, nivelGas = _nivelGas WHERE idBitacora = _idBitacora;
END //


DELIMITER //
CREATE PROCEDURE SP_ultimoDetalleHoja( _idVehiculo INT )
BEGIN

	DECLARE _idHoja INT;

    SET _idHoja = (SELECT idHoja FROM tbl_hojaSalidas h INNER JOIN tbl_Bitacoras b ON h.idBitacora = b.idBitacora WHERE b.idVehiculo = _idVehiculo ORDER BY h.idHoja DESC LIMIT 1);

	SELECT
		dh.idItem,
        ai.item,
        dh.idEstado,
        e.estado
	FROM
		tbl_detalleHojas dh
    INNER JOIN
		tbl_autoItems ai ON dh.idItem = ai.idItem
	INNER JOIN
		tbl_Estados e ON dh.idEstado = e.idEstado
	WHERE
		dh.idHoja = _idHoja;

END //


DELIMITER //
CREATE PROCEDURE SP_ultimoDetalleGolpes( _idHoja INT )
BEGIN
	SELECT
		dg.idHoja,
		dg.costado,
		dg.foto,
		tg.*
	FROM
		tbl_detalleGolpes dg
	INNER JOIN
		tbl_tipoGolpes tg ON dg.idTipo = tg.idTipo
	INNER JOIN
		tbl_hojaSalidas hs ON dg.idHoja = hs.idHoja
	INNER JOIN
		tbl_Bitacoras bt ON hs.idBitacora = bt.idBitacora
	WHERE
		dg.idHoja = _idHoja;
END //


DELIMITER //
CREATE PROCEDURE SP_buscarPiloto( _kmSalida INT, _kmEntrada INT, _idVehiculo INT)
BEGIN
	SELECT
		u.idUsuario,
		CONCAT(u.firstName, ' ', u.firstApe) AS 'nombre'
	FROM
		tbl_Bitacoras b
	INNER JOIN
		tbl_usuarios u ON b.idUsuario = u.idUsuario
	WHERE
		b.kmInicial = _kmSalida AND b.kmFinal = _kmEntrada AND b.idVehiculo = _idVehiculo;
END //


DELIMITER //
CREATE PROCEDURE SP_insertarVale(
	_tipoIngreso	VARCHAR(15),
	_nIngreso		INT,
	_valor 			FLOAT,
    _fechaIngreso	DATE,
    _idProveedor	INT,
	_idUsuario 		INT,
	_idVehiculo		INT,
    _kmSalida		INT,
    _kmEntrada		INT
)
BEGIN
	IF EXISTS (SELECT * FROM tbl_Combustibles WHERE nIngreso = _nIngreso) THEN
		SELECT 'error' AS 'resultado';
	ELSE
		IF EXISTS (SELECT * FROM tbl_Combustibles WHERE kmSalida = _kmSalida AND kmEntrada = _kmEntrada) THEN
			SELECT 'error' AS 'resultado';
		ELSE
			INSERT INTO `tbl_Combustibles`(`idCombustible`, `tipoIngreso`, `nIngreso`, `valor`, `fechaIngreso`, `fecha`, `hora`, `kmSalida`, `kmEntrada`, `idProveedor`, `idUsuario`, `idVehiculo`) VALUES 
			(NULL, _tipoIngreso, _nIngreso, _valor, _fechaIngreso, CURDATE(), TIME_FORMAT(NOW(), "%H:%i"), _kmSalida, _kmEntrada, _idProveedor, _idUsuario, _idVehiculo);
			SELECT 'success' AS 'resultado';
		END IF;
    END IF;
END //


DELIMITER //
CREATE PROCEDURE SP_listaVales()
BEGIN
	SELECT
		v.idVehiculo,
		c.tipoIngreso,
        c.nIngreso,
		CONCAT('$',valor) AS 'valor',
        c.fechaIngreso,
		c.fecha,
		c.hora,
		v.numeroPlaca,
		t.vehiculo AS 'tipo',
		CONCAT(u.firstName, ' ', u.firstApe) AS 'piloto',
		c.kmSalida,
		c.kmEntrada
	FROM
		tbl_Combustibles c
	INNER JOIN
		tbl_usuarios u ON c.idUsuario = u.idUsuario
	INNER JOIN
		tbl_Vehiculos v ON c.idVehiculo = v.idVehiculo
	INNER JOIN
		tbl_tipoVehiculos t ON v.idTipoVehiculo = t.idTipoVehiculo;
END //


DELIMITER //
CREATE PROCEDURE SP_verificaLicencia( _idUsuario INT )
BEGIN
	IF EXISTS (SELECT * FROM tbl_licencias WHERE idUsuario = _idUsuario) THEN
		SELECT 'success' AS 'resultado';
    ELSE
		SELECT 'error' AS 'resultado';
    END IF;
END //


DELIMITER //
CREATE PROCEDURE SP_infoLicencia( _idUsuario INT )
BEGIN
	SELECT
		l.idLicencia,
		l.idUsuario,
        u.firstName,
		u.secndName,
		u.firstApe,
		u.secndApe,
        CONCAT(u.firstName, ' ', u.secndName, ' ', u.firstApe, ' ', u.secndApe) AS 'nombre',
		l.nit,
		l.dui,
		l.fechaExpd,
		l.fechaVenc,
		l.tipoLicencia,
		l.idEstado,
		l.aprobacion
	FROM
		tbl_licencias l
	INNER JOIN
		tbl_usuarios u ON l.idUsuario = u.idUsuario
	WHERE
		u.idUsuario = _idUsuario;
END //


DELIMITER //
CREATE PROCEDURE SP_insertarLicencia(
	_idUsuario		INT,
    _nit			VARCHAR(17),
    _dui			VARCHAR(10),
    _fechaExpd		DATE,
    _fechaVenc		DATE,
    _tipoLicencia	VARCHAR(30)
)
BEGIN
	IF EXISTS (SELECT * FROM tbl_licencias WHERE idUsuario = _idUsuario AND tipoLicencia = _tipoLicencia) THEN
		SELECT 'error' AS 'resultado';
    ELSE
		INSERT INTO `tbl_licencias`(`idLicencia`, `idUsuario`, `nit`, `dui`, `fechaExpd`, `fechaVenc`, `tipoLicencia`, `idEstado`, `aprobacion`) VALUES
		(NULL, _idUsuario, _nit, _dui, _fechaExpd, _fechaVenc, _tipoLicencia, 11, 0);

		SELECT 'success' AS 'resultado';
    END IF;
END //


DELIMITER //
CREATE PROCEDURE SP_actualizarLicencia(
	_idLicencia		INT,
    _nit			VARCHAR(17),
    _dui			VARCHAR(10),
    _fechaExpd		DATE,
    _fechaVenc		DATE,
    _tipoLicencia	VARCHAR(30)
)
BEGIN
	UPDATE
		tbl_licencias
	SET
		dui = _dui, nit = _nit, fechaExpd = _fechaExpd, fechaVenc = _fechaVenc, tipoLicencia = _tipoLicencia
	WHERE
		idLicencia = _idLicencia;
END //


DELIMITER //
CREATE PROCEDURE SP_listaUsuarios()
BEGIN
	SELECT
		u.idUsuario,
		u.firstName AS 'nombre1',
		u.secndName  AS 'nombre2',
		u.firstApe  AS 'apellido1',
		u.secndApe  AS 'apellido2',
        CONCAT(u.firstName, ' ', u.secndName, ' ', u.firstApe, ' ', u.secndApe) AS 'nombreCompleto',
		r.region,
		u.email,
		u.cargo,
		u.idEstado,
        e.estado
	FROM
		tbl_usuarios u
	INNER JOIN
		tbl_Regiones r ON u.idRegion = r.idRegion
	INNER JOIN
		tbl_Estados e ON u.idEstado = e.idEstado;
END //


DELIMITER //
CREATE PROCEDURE SP_listaPilotos()
BEGIN
	SELECT
		u.idUsuario,
		u.firstName AS 'nombre1',
		u.secndName  AS 'nombre2',
		u.firstApe  AS 'apellido1',
		u.secndApe  AS 'apellido2',
        CONCAT(u.firstName, ' ', u.secndName, ' ', u.firstApe, ' ', u.secndApe) AS 'nombreCompleto',
		r.region,
		u.email,
		u.cargo,
        l.tipoLicencia,
        e.estado,
        l.aprobacion
	FROM
		tbl_licencias l
	INNER JOIN
		tbl_usuarios u ON l.idUsuario = u.idUsuario
	INNER JOIN
		tbl_Regiones r ON u.idRegion = r.idRegion
	INNER JOIN
		tbl_Estados e ON l.idEstado = e.idEstado
	WHERE
		u.idEstado = 1;
END //


DELIMITER //
CREATE PROCEDURE SP_bitacorasUsuarios( _idUsuario INT )
BEGIN
	SELECT
		v.numeroPlaca,
		v.color,
		m.marca,
		b.*
	FROM
		tbl_Bitacoras b
	INNER JOIN
		tbl_Vehiculos v ON b.idVehiculo = v.idVehiculo
	INNER JOIN
		tbl_marcas m ON v.idMarca = m.idMarca
	WHERE
		b.idUsuario = _idUsuario;
END //


DELIMITER //
CREATE PROCEDURE SP_insertarPreorden(
    _tipoPreorden	VARCHAR(20),
    _idVehiculo 	INT,
    _kilometraje	INT,
    _comentarios	VARCHAR(100),
	_idUsuario		INT
)
BEGIN
	INSERT INTO tbl_preordenes (tipoPreorden, idVehiculo, kilometraje, fechaEntrada, comentarios, idUsuario)
	VALUES
	(_tipoPreorden, _idVehiculo, _kilometraje, NOW(), _comentarios, _idUsuario);

	SELECT idPreorden FROM tbl_preordenes ORDER BY idPreorden DESC LIMIT 1;
END //


DELIMITER //
CREATE PROCEDURE SP_listaPreordenes()
BEGIN
	SELECT
		p.idPreorden,
	    v.numeroPlaca,
	    v.modelo,
	    v.color,
	    p.tipoPreorden,
	    p.fechaEntrada,
	    p.fechaSalida,
	    u.email AS 'usuario',
	    e.estado
	FROM
		tbl_preordenes p
	INNER JOIN
		tbl_usuarios u ON p.idUsuario = u.idUsuario
	INNER JOIN
		tbl_Vehiculos v ON p.idVehiculo = v.idVehiculo
	INNER JOIN
		tbl_Regiones r ON v.idRegion = r.idRegion
	INNER JOIN
		tbl_Estados e ON p.idEstado = e.idEstado;
END //


DELIMITER //
CREATE PROCEDURE SP_valDatCarCreate(
	_numeroPlaca 	VARCHAR(45),
	_motor			VARCHAR(15),
    _chasis			VARCHAR(25)
)
BEGIN
	IF NOT EXISTS (SELECT * FROM tbl_Vehiculos WHERE numeroPlaca = _numeroPlaca) THEN
		IF NOT EXISTS (SELECT * FROM tbl_Vehiculos WHERE motor = _motor) THEN
			IF NOT EXISTS (SELECT * FROM tbl_Vehiculos WHERE chasis = _chasis) THEN
				SELECT 'success' AS 'resultado';
			ELSE
				SELECT 'error' AS 'resultado';
            END IF;
		ELSE
			SELECT 'error' AS 'resultado';
        END IF;
	ELSE
		SELECT 'error' AS 'resultado';
    END IF;
END //


DELIMITER //
CREATE PROCEDURE SP_valDatCarUpdate(
	_numeroPlaca 	VARCHAR(45),
	_motor			VARCHAR(15),
    _chasis			VARCHAR(25),
    _id				INT
)
BEGIN
	IF NOT EXISTS (SELECT * FROM tbl_Vehiculos WHERE numeroPlaca = _numeroPlaca AND idVehiculo <> _id) THEN
		IF NOT EXISTS (SELECT * FROM tbl_Vehiculos WHERE motor = _motor  AND idVehiculo <> _id) THEN
			IF NOT EXISTS (SELECT * FROM tbl_Vehiculos WHERE chasis = _chasis  AND idVehiculo <> _id) THEN
				SELECT 'success' AS 'resultado';
			ELSE
				SELECT 'error' AS 'resultado';
            END IF;
		ELSE
			SELECT 'error' AS 'resultado';
        END IF;
	ELSE
		SELECT 'error' AS 'resultado';
    END IF;
END //


DELIMITER //
CREATE PROCEDURE SP_verificaDetalleGolpe(_id INT)
BEGIN
	IF EXISTS (SELECT idHoja FROM tbl_detalleGolpes WHERE idHoja = _id) THEN
		SELECT 'success' AS 'resultado';
	ELSE
		SELECT 'error' AS 'resultado';
    END IF;
END //


DELIMITER //
CREATE PROCEDURE SP_newMtto(
	_idPreorden INT,
	_idUsuario INT,
	_comentario VARCHAR(100),
	_tipo VARCHAR(10),
	_proxKm INT,
	_idVehiculo INT
)
BEGIN
	INSERT INTO tbl_mttos VALUES (NULL, _idPreorden, _idUsuario, _tipo, _comentario);
	UPDATE tbl_preordenes SET fechaSalida = NOW(), idEstado = 5 WHERE idPreorden = _idPreorden;
	UPDATE tbl_Vehiculos SET proxMtto = _proxKm WHERE idVehiculo = _idVehiculo;
    SELECT idMtto FROM tbl_mttos ORDER BY idMtto DESC LIMIT 1;
END //


DELIMITER //
CREATE PROCEDURE SP_detalleFactura(_idPreorden INT)
BEGIN
	DECLARE _idMtto INT;
	SET _idMtto = (SELECT idMtto FROM tbl_mttos WHERE idPreorden = _idPreorden);

	SELECT
		dm.idMtto,
		p.nombre AS 'producto',
		dm.cantidad,
		dm.numFactura,
		pr.nombre AS 'proveedor',
		dm.precioUnitario,
		(dm.cantidad * dm.precioUnitario) AS 'total'
	FROM
		tbl_detalleMttos dm
	INNER JOIN
		tbl_Productos p ON dm.idProducto = p.idProducto
	INNER JOIN
		tbl_Proveedores pr ON dm.idProveedor = pr.idProveedor
	WHERE
		dm.idMtto = _idMtto;
END //


DELIMITER //
CREATE PROCEDURE SP_infoFactura(_idMtto INT, _nfact VARCHAR(10))
BEGIN
	SELECT
		p.nombre AS 'producto',
		dm.cantidad,
		dm.numFactura,
		pr.nombre AS 'proveedor',
		dm.precioUnitario,
		(dm.cantidad * dm.precioUnitario) AS 'total'
	FROM
		tbl_detalleMttos dm
	INNER JOIN
		tbl_Productos p ON dm.idProducto = p.idProducto
	INNER JOIN
		tbl_Proveedores pr ON dm.idProveedor = pr.idProveedor
	WHERE
		dm.idMtto = _idMtto AND dm._numFactura = _nfact;
END //