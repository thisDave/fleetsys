<?php

class Conexion
{
	    public $host;
	    public $user;
	    public $pwd;
	    public $bd;
	    public $con;

	    public function __construct()
	    {
	    	$this->host = "192.168.71.239";
	    	$this->user = "myadmin";
	    	$this->pwd = "Admin_32";
	    	$this->db = "db_fleetsys";
	    }

	    public function conectar()
	    {
	    	try
	    	{
	    		$this->con = mysqli_connect($this->host, $this->user, $this->pwd, $this->db);

	    		if ($this->con)
	    			return $this->con;
	    		else
	    			throw new Exception('No fue posible conectarse a la base de datos');
	    	}
	    	catch (Exception $e)
	    	{
	    		header("Location: ".URL."db_err");
	    	}
	    }

	    public function desconectar()
	    {
	    	try
	    	{
	    		if (!mysqli_close($this->con))
	    			throw new Exception('No fue posible conectarse a la base de datos');

	    	}
	    	catch (Exception $e)
	    	{
	    		header("Location: ".URL."db_err");
	    	}
	    }
	}
