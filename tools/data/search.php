<?php

session_start();

require_once "Conexion.php";

class Datos extends Conexion
{
    public function showRes($query)
	{
		$c = parent::conectar();
		$c->set_charset('utf8');
		$res = $c->query($query);
		parent::desconectar();
		return $res;
	}
}

$datos = new Datos;

if (isset($_POST['buscarPiloto']))
{
	$kmSalida = $_POST['kmOutput'];
	$kmEntrada = $_POST['kmInput'];
	$auto = $_POST['auto'];

	$query = "CALL SP_buscarPiloto(".$kmSalida.", ".$kmEntrada.", ".$auto.")";

	$mysql = $datos->showRes($query);

	$_SESSION['idUsuario'] = '';

	if ($mysql)
	{
		if ($mysql->num_rows > 0)
		{
			$datos = $mysql->fetch_assoc();

			$_SESSION['idUsuario'] = $datos['idUsuario'];
			$piloto = $datos['nombre'];

			$res = $piloto;
		}
		else
		{
			$res = 'Datos incorrectos, verificar Bitácora';
		}
	}
	else
	{
		$res = 'Error en consulta a BD, contacte con el administrador.';
	}

	echo $res;
}

if (isset($_POST['buscarKm']))
{
	$placa = preg_replace('([^A-Za-z0-9 ])', '', trim(strtoupper($_POST['placa'])));

	$_SESSION['placa'] = $placa;

	$query = "SELECT kilometraje FROM tbl_Vehiculos WHERE numeroPlaca = '{$placa}'";

	$mysql = $datos->showRes($query);

	if ($mysql)
	{
		if ($mysql->num_rows > 0)
		{
			$datos = $mysql->fetch_assoc();

			$_SESSION['kmActual'] = $datos['kilometraje'];

			$res = $datos['kilometraje'];
		}
		else
		{
			$res = 'Datos incorrectos.';
		}
	}
	else
	{
		$res = 'Error en la base de datos.';
	}

	echo $res;
}

if (isset($_POST['comments']))
{
	$_SESSION['comentarios'] = $_POST['comments'];
}

if (isset($_POST['commentPreorden']))
{
	$_SESSION['comentPreorden'] = $_POST['commentPreorden'];
}